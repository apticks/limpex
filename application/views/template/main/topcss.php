<!--Start Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/public/css/bootstrap.min.css?<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/public/css/font.css?<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/public/css/font-awesome.min.css?<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/public/css/swiper.min.css?<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/public/css/magnific-popup.css?<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/public/css/layers.css?<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/public/css/navigation.css?<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/public/css/settings.css?<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/public/css/range.css?<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/public/css/nice-select.css?<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/public/css/style.css?<?php echo time();?>">

<!-- Product desc source -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/public/css/product_desc.css?<?php echo time();?>">