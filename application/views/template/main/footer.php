<!--Partners-->
	<div class="clv_partner_wrapper clv_section">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-lg-6 col-md-6">
					<div class="clv_heading">
						<h3>Our Associates</h3>
						<div class="clv_underline"><img src="<?php echo base_url();?>assets/public/images/agri_underline2.png" alt="image" /></div>
						</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="partner_slider">
						<div class="swiper-container">
							<div class="swiper-wrapper">
								<div class="swiper-slide">
									<div class="partner_slide">
										<div class="partner_image">
											<span>
												<img alt="basix" src="<?php echo base_url()?>assets/img/partners/vikas.png" width="170" height="150"/>
											</span>
										</div>
									</div>
								</div>
							
								<div class="swiper-slide">
									<div class="partner_slide">
										<div class="partner_image">
											<span>
												<img alt="basix" src="<?php echo base_url()?>assets/img/partners/girijan_vikas.jpg" width="170" height="150"/>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- Add Arrows -->
						<span class="slider_arrow partner_left left_arrow">
							<svg 
							 xmlns="http://www.w3.org/2000/svg"
							 xmlns:xlink="http://www.w3.org/1999/xlink"
							 width="10px" height="15px">
							<path fill-rule="evenodd"  fill="rgb(226, 226, 226)"
							 d="M0.324,8.222 L7.117,14.685 C7.549,15.097 8.249,15.097 8.681,14.685 C9.113,14.273 9.113,13.608 8.681,13.197 L2.670,7.478 L8.681,1.760 C9.113,1.348 9.113,0.682 8.681,0.270 C8.249,-0.139 7.548,-0.139 7.116,0.270 L0.323,6.735 C0.107,6.940 -0.000,7.209 -0.000,7.478 C-0.000,7.747 0.108,8.017 0.324,8.222 Z"/>
							</svg>
						</span>
						<span class="slider_arrow partner_right right_arrow">
							<svg 
							 xmlns="http://www.w3.org/2000/svg"
							 xmlns:xlink="http://www.w3.org/1999/xlink"
							 width="19px" height="25px">
							<path fill-rule="evenodd" fill="rgb(226, 226, 226)"
							 d="M13.676,13.222 L6.883,19.685 C6.451,20.097 5.751,20.097 5.319,19.685 C4.887,19.273 4.887,18.608 5.319,18.197 L11.329,12.478 L5.319,6.760 C4.887,6.348 4.887,5.682 5.319,5.270 C5.751,4.861 6.451,4.861 6.884,5.270 L13.676,11.735 C13.892,11.940 14.000,12.209 14.000,12.478 C14.000,12.747 13.892,13.017 13.676,13.222 Z"/>
							</svg>
						</span>
					</div> 
				</div>
			</div>
			<!--Newsletter-->
			<div class="clv_newsletter_wrapper">
				<div class="newsletter_text">
					<h2>get update from <br/>anywhere</h2>
					<h4>subscribe us to get more info</h4>
				</div>
				<div class="newsletter_field">
					<h3>don't miss out on the good news!</h3>
					<div class="newsletter_field_block">
						<form action="<?php echo base_url('subscribe');?>"  method="post" enctype="multipart/form-data">
						<input type="text" placeholder="Enter Your Email Here" id="input-news-field" style="width: 100% !important;height: 50px !important;padding: 0px 20px;padding-right: 180px;border-radius: 30px;border: none;outline: none;" name="email" required="" />
						
						 <button type="submit" class="clv_btn submitForm" data-type="contact" value="apply">subscribe now</button> 
					</form>
					</div>
				</div>
			</div>
		</div>
	</div>

<!--Footer-->
<div class="clv_footer_wrapper clv_section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3 col-lg-3 col-xs-12">
				<div class="footer_block" id="footer-about">
					<div class="footer_logo">
						<a href="<?php echo base_url('home');?>"><img src="<?php echo base_url();?>assets/img/limpex_logo.png" alt="Cultivation" style="height: 100px;width: 180px;"></a>
						<marquee direction="left" width="200" style="color: #008c5c">Sarve Jana Sukhino Bhavantu!!</marquee>

					</div>
					<p>We are very excited to provide taste of LIMPEX Global Pvt Ltd a difference of approach on quality of agriculture products directly from Farmers (Farmers Producers Organizations) to foreign buyers. But before you do, we wanted to share OUR STORY with you. <a href="<?php echo base_url('about');?>"> read more </a></p>
				</div>
			</div>
<!--			<div class="col-md-4 col-lg-4 col-xs-12">
 				<div class="footer_block"> -->
<!-- 					<div class="footer_heading"> -->
<!-- 						<h4>recent post</h4> 
						<img src="<?php echo base_url()?>assets/public/images/footer_underline.png" alt="image" />
						<div class="footer_slider_arrows"> -->
<!-- 							<span class="footer_arrow footer_left"> <svg -->
<!-- 									xmlns="http://www.w3.org/2000/svg" -->
<!-- 									xmlns:xlink="http://www.w3.org/1999/xlink" width="6px" -->
<!-- 									height="10px"> -->
<!-- 									<path fill-rule="evenodd" fill="rgb(182, 182, 182)" -->
<!-- 										d="M0.215,5.448 L4.736,9.733 C5.023,10.007 5.489,10.007 5.777,9.733 C6.064,9.462 6.064,9.020 5.777,8.747 L1.777,4.954 L5.777,1.161 C6.064,0.888 6.064,0.447 5.777,0.174 C5.489,-0.098 5.023,-0.098 4.735,0.174 L0.215,4.461 C0.071,4.598 -0.000,4.776 -0.000,4.954 C-0.000,5.133 0.072,5.312 0.215,5.448 Z" /> -->
<!-- 									</svg> -->
<!-- 							</span> <span class="footer_arrow footer_right"> <svg -->
<!-- 									xmlns="http://www.w3.org/2000/svg" -->
<!-- 									xmlns:xlink="http://www.w3.org/1999/xlink" width="7px" -->
<!-- 									height="10px"> -->
<!-- 									<path fill-rule="evenodd" fill="rgb(182, 182, 182)" -->
<!-- 										d="M6.409,5.448 L1.885,9.733 C1.598,10.007 1.131,10.007 0.844,9.733 C0.556,9.462 0.556,9.020 0.844,8.747 L4.846,4.954 L0.844,1.161 C0.556,0.888 0.556,0.447 0.844,0.174 C1.131,-0.098 1.598,-0.098 1.886,0.174 L6.410,4.461 C6.553,4.598 6.625,4.776 6.625,4.954 C6.625,5.133 6.553,5.312 6.409,5.448 Z" /> -->
<!-- 									</svg> -->
<!-- 							</span> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 					<div class="footer_post_slider"> -->
<!-- 						<div class="swiper-container"> -->
<!-- 							<div class="swiper-wrapper"> -->
								
								<?php $sno = 1; if(! empty($blogs)){ foreach($blogs as $cat):?>
<!-- 								<div class="swiper-slide"> -->
<!-- 									<div class="footer_post_slide"> -->
<!-- 										<div class="footer_post_image"> 
											 <img src="<?php echo base_url()?>assets/public/images/footer_post2.jpg?<?php echo time();?>" alt="image" /> -->
<!--											<img src="<?php echo base_url();?>uploads/blog_image/blog_<?php echo $cat['id'];?>.jpg?<?php echo time();?>" alt="image" style="width: 69px;height: 50px;">
 										</div> -->
<!-- 										<div class="footer_post_content"> 
											<span><?php echo date('M d, Y', strtotime($cat['created_at']))?></span>
 											<p> 
												<a href="<?php echo base_url();?>blog/desc?id=<?php echo $cat['id'];?>"><?php echo $cat['title']?></a>
											</p> -->
<!-- 										</div> -->
<!-- 									</div> -->
<!-- 								</div> -->
							<?php endforeach;}?>
								
<!-- 							</div> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 				</div> 
			
			</div>-->
			<div class="col-md-3 col-lg-3 col-xs-12 ">
				<div class="footer_block">
					<div class="footer_heading">
						<h4>Catalogue</h4>
						<img src="<?php echo base_url()?>assets/public/images/footer_underline.png" alt="image" />
					</div>
					<ul class="useful_links">
    					<?php $categories = $this->category_model->get_all();
    					if(! empty($categories)){foreach ($categories as $category ):?>
    						<li><a href="<?php echo base_url();?>product/list?cat_id=<?php echo $category['id'];?>&sub_cat_id=0"><span><i class="fa fa-angle-right"
									aria-hidden="true"></i></span><?php echo $category['name'];?></a></li>
    					<?php endforeach;}?>
					</ul>
				</div>
				<!-- <div class="counter">
                	<a href='https://www.symptoma.ro/'></a> <script type='text/javascript' src='https://www.freevisitorcounters.com/auth.php?id=a58d6c65cf3ddf0f7b114b37b570c22d16d9db82'></script>
                	<script type="text/javascript" src="https://www.freevisitorcounters.com/en/home/counter/712935/t/0"></script>
                </div> -->
			</div>
			<div class="col-md-3 col-lg-3 col-xs-12 ">
				<div class="footer_block">
					<div class="footer_heading">
						<h4>contact</h4>
						<img src="<?php echo base_url()?>assets/public/images/footer_underline.png" alt="image" />
					</div>
					<p>
						<span><svg xmlns="http://www.w3.org/2000/svg"
								xmlns:xlink="http://www.w3.org/1999/xlink" width="16px"
								height="16px">
								<defs>
								<filter id="Filter_1">
									<feFlood flood-color="rgb(31, 161, 46)" flood-opacity="1"
									result="floodOut" />
									<feComposite operator="atop" in="floodOut" in2="SourceGraphic"
									result="compOut" />
									<feBlend mode="normal" in="compOut" in2="SourceGraphic" />
								</filter>

								</defs>
								<g filter="url(#Filter_1)">
								<path fill-rule="evenodd" fill="rgb(81, 176, 30)"
									d="M14.873,0.856 C14.815,0.856 14.700,0.856 14.643,0.913 L0.850,6.660 C0.620,6.776 0.505,6.948 0.505,7.176 C0.505,7.465 0.677,7.695 0.965,7.752 L6.942,9.189 C7.057,9.189 7.114,9.305 7.172,9.419 L8.608,15.396 C8.666,15.626 8.896,15.855 9.183,15.855 C9.413,15.855 9.643,15.683 9.700,15.511 L15.447,1.718 C15.447,1.660 15.505,1.603 15.505,1.488 C15.447,1.085 15.217,0.856 14.873,0.856 ZM9.355,8.902 L9.068,7.695 C9.011,7.465 8.838,7.350 8.666,7.292 L7.459,7.005 C7.172,6.948 7.172,6.545 7.401,6.487 L11.022,4.993 C11.252,4.878 11.482,5.109 11.424,5.395 L9.930,9.017 C9.758,9.189 9.413,9.131 9.355,8.902 Z" />
								</g>
								</svg></span>
								 <!--  Rajas Courtyard, 179 pillar, <br>Near ICBM college,<br>Upparapllay, Rajendranagar mandal, <br>Hyderabad,500048 -->
								 Nagaratna Residency, <br>Aruna cooperative society,<br> Bhagyanagar Colony, Kukatpally,<br> Hyderabad Telangana 500072.
								 </p>
								<p>
								<!-- <span><svg xmlns="http://www.w3.org/2000/svg"
								xmlns:xlink="http://www.w3.org/1999/xlink" width="16px"
								height="16px">
								<defs>
								<filter id="Filter_1">
									<feFlood flood-color="rgb(31, 161, 46)" flood-opacity="1"
									result="floodOut" />
									<feComposite operator="atop" in="floodOut" in2="SourceGraphic"
									result="compOut" />
									<feBlend mode="normal" in="compOut" in2="SourceGraphic" />
								</filter>

								</defs>
								<g filter="url(#Filter_1)">
								<path fill-rule="evenodd" fill="rgb(81, 176, 30)"
									d="M14.873,0.856 C14.815,0.856 14.700,0.856 14.643,0.913 L0.850,6.660 C0.620,6.776 0.505,6.948 0.505,7.176 C0.505,7.465 0.677,7.695 0.965,7.752 L6.942,9.189 C7.057,9.189 7.114,9.305 7.172,9.419 L8.608,15.396 C8.666,15.626 8.896,15.855 9.183,15.855 C9.413,15.855 9.643,15.683 9.700,15.511 L15.447,1.718 C15.447,1.660 15.505,1.603 15.505,1.488 C15.447,1.085 15.217,0.856 14.873,0.856 ZM9.355,8.902 L9.068,7.695 C9.011,7.465 8.838,7.350 8.666,7.292 L7.459,7.005 C7.172,6.948 7.172,6.545 7.401,6.487 L11.022,4.993 C11.252,4.878 11.482,5.109 11.424,5.395 L9.930,9.017 C9.758,9.189 9.413,9.131 9.355,8.902 Z" />
								</g>
								</svg></span> Branch Office:<br>Regional Office (Kerala),
											Near Sasthamkulangara Temple,
											Keezcherimel,
											PO: Chengannur,
											Alappuzha - 689121
											Kerala.<br>Contact No: +91 9422093021</p> -->
					</p>
					
					<p>
						<span><svg xmlns="http://www.w3.org/2000/svg"
								xmlns:xlink="http://www.w3.org/1999/xlink" width="16px"
								height="15px">
								
								<g filter="url(#Filter_1)">
								<path fill-rule="evenodd" fill="rgb(81, 176, 30)"
									d="M13.866,7.235 C13.607,5.721 12.892,4.344 11.802,3.254 C10.653,2.108 9.197,1.381 7.592,1.156 L7.755,-0.002 C9.613,0.257 11.296,1.096 12.626,2.427 C13.888,3.692 14.716,5.284 15.019,7.039 L13.866,7.235 ZM10.537,4.459 C11.296,5.222 11.796,6.181 11.977,7.238 L10.824,7.436 C10.684,6.617 10.300,5.874 9.713,5.287 C9.091,4.666 8.304,4.276 7.439,4.155 L7.601,2.996 C8.719,3.151 9.734,3.657 10.537,4.459 ZM4.909,8.182 C5.709,9.162 6.611,10.033 7.689,10.711 C7.920,10.854 8.176,10.960 8.417,11.092 C8.538,11.160 8.623,11.139 8.723,11.035 C9.088,10.661 9.460,10.293 9.831,9.924 C10.318,9.440 10.931,9.440 11.421,9.924 C12.017,10.516 12.614,11.110 13.207,11.707 C13.704,12.207 13.701,12.818 13.201,13.324 C12.864,13.665 12.505,13.989 12.186,14.345 C11.721,14.866 11.140,15.035 10.472,14.997 C9.500,14.944 8.607,14.623 7.745,14.205 C5.831,13.275 4.194,11.985 2.823,10.355 C1.808,9.150 0.971,7.834 0.422,6.355 C0.153,5.639 -0.038,4.906 0.022,4.129 C0.059,3.651 0.237,3.242 0.590,2.907 C0.971,2.546 1.330,2.168 1.705,1.800 C2.192,1.319 2.804,1.319 3.295,1.797 C3.598,2.093 3.894,2.396 4.194,2.696 C4.485,2.988 4.775,3.277 5.065,3.570 C5.578,4.085 5.578,4.684 5.069,5.197 C4.703,5.565 4.341,5.933 3.969,6.293 C3.873,6.390 3.863,6.468 3.913,6.586 C4.160,7.173 4.513,7.694 4.909,8.182 Z" />
								</g>
								</svg></span> +91 9422839891
					</p>
					<p>
						<span><svg xmlns="http://www.w3.org/2000/svg"
								xmlns:xlink="http://www.w3.org/1999/xlink" width="16px"
								height="16px">
							
							<g filter="url(#Filter_1)">
							<path fill-rule="evenodd" fill="rgb(81, 176, 30)"
									d="M16.000,5.535 C16.000,4.982 15.680,4.507 15.280,4.191 L8.000,-0.002 L0.720,4.191 C0.320,4.507 0.000,4.982 0.000,5.535 L0.000,13.447 C0.000,14.317 0.720,15.028 1.600,15.028 L14.400,15.028 C15.280,15.028 16.000,14.317 16.000,13.447 L16.000,5.535 ZM8.000,9.491 L1.360,5.376 L8.000,1.579 L14.640,5.376 L8.000,9.491 Z" />
							</g>
							</svg></span> limpexglobal@gmail.com
					</p>
						<p>
						<!-- <span><svg xmlns="http://www.w3.org/2000/svg"
								xmlns:xlink="http://www.w3.org/1999/xlink" width="16px"
								height="16px">
							
							<g filter="url(#Filter_1)">
							<path fill-rule="evenodd" fill="rgb(81, 176, 30)"
									d="M16.000,5.535 C16.000,4.982 15.680,4.507 15.280,4.191 L8.000,-0.002 L0.720,4.191 C0.320,4.507 0.000,4.982 0.000,5.535 L0.000,13.447 C0.000,14.317 0.720,15.028 1.600,15.028 L14.400,15.028 C15.280,15.028 16.000,14.317 16.000,13.447 L16.000,5.535 ZM8.000,9.491 L1.360,5.376 L8.000,1.579 L14.640,5.376 L8.000,9.491 Z" />
							</g>
							</svg></span> girish@limpex.in
					</p> -->
					
				</div>
			</div>
			
			<div class="col-md-3 col-lg-3 col-xs-12">
				<div class="footer_block">
					<div class="footer_heading">
						<h4>Social</h4>
						<img src="<?php echo base_url()?>assets/public/images/footer_underline.png" alt="image" />
						<ul class="agri_social_links">
						<li><a href="<?php echo $this->setting_model->where('key','facebook')->get()['value'];?>"><span><i class="fa fa-facebook"
									aria-hidden="true"></i></span></a></li>
						<li><a href="<?php echo $this->setting_model->where('key','twiter')->get()['value'];?>"><span><i class="fa fa-twitter"
									aria-hidden="true"></i></span></a></li>
							<li><a href="<?php echo $this->setting_model->where('key','linkdin')->get()['value'];?>"><span><i class="fa fa-linkedin"
									aria-hidden="true"></i></span></a></li>
						<!-- <li><a href="<?php //echo $this->setting_model->where('key','youtube')->get()['value'];?>"><span><i class="fa fa-youtube-play"
									aria-hidden="true"></i></span></a></li> -->
					</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--Copyright-->
<div class="clv_copyright_wrapper">
	<p>
		copyright &copy; 2020. Designed by <a href="http://apticks.com/">Apticks Solutions</a> 
	</p>
</div>

<style>
.counter{
   padding: 0px 10px;
}
.counter img{
    width : 200px;
    display: none;
}
div#error_ {
    display: none;
}
</style>
