<!--Main js file Style-->
<script src="<?php echo base_url();?>assets/public/js/jquery.js?<?php echo time();?>"></script>
<script src="<?php echo base_url();?>assets/public/js/bootstrap.min.js?<?php echo time();?>"></script>
<script src="<?php echo base_url();?>assets/public/js/swiper.min.js?<?php echo time();?>"></script>
<script src="<?php echo base_url();?>assets/public/js/magnific-popup.min.js?<?php echo time();?>"></script>
<script src="<?php echo base_url();?>assets/public/js/jquery.themepunch.tools.min.js?<?php echo time();?>"></script>
<script src="<?php echo base_url();?>assets/public/js/jquery.themepunch.revolution.min.js?<?php echo time();?>"></script>
<script src="<?php echo base_url();?>assets/public/js/jquery.appear.js?<?php echo time();?>"></script>
<script src="<?php echo base_url();?>assets/public/js/jquery.countTo.js?<?php echo time();?>"></script>
<script src="<?php echo base_url();?>assets/public/js/isotope.min.js?<?php echo time();?>"></script>
<script src="<?php echo base_url();?>assets/public/js/nice-select.min.js?<?php echo time();?>"></script>
<script src="<?php echo base_url();?>assets/public/js/range.js?<?php echo time();?>"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->	
<script  src="<?php echo base_url();?>assets/public/js/revolution.extension.actions.min.js?<?php echo time();?>"></script>
<script  src="<?php echo base_url();?>assets/public/js/revolution.extension.kenburn.min.js?<?php echo time();?>"></script>
<script  src="<?php echo base_url();?>assets/public/js/revolution.extension.layeranimation.min.js?<?php echo time();?>"></script>
<script  src="<?php echo base_url();?>assets/public/js/revolution.extension.migration.min.js?<?php echo time();?>"></script>
<script  src="<?php echo base_url();?>assets/public/js/revolution.extension.parallax.min.js?<?php echo time();?>"></script>
<script  src="<?php echo base_url();?>assets/public/js/revolution.extension.slideanims.min.js?<?php echo time();?>"></script>
<script  src="<?php echo base_url();?>assets/public/js/revolution.extension.video.min.js?<?php echo time();?>"></script>
<script src="<?php echo base_url();?>assets/public/js/custom.js?<?php echo time();?>"></script>



<!-- Product details page source -->
<script src='https://unpkg.com/xzoom/dist/xzoom.min.js'></script>
<script src='https://hammerjs.github.io/dist/hammer.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/foundation/6.3.1/js/foundation.min.js'></script>
<script src="<?php echo base_url();?>assets/public/js/product_desc.js?<?php echo time();?>"></script>

<!-- Google translate -->
<script type="text/javascript">
  function googleTranslateElementInit() {
    new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
  }
</script>
<script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit" type="text/javascript"></script>

<!-- Flag click handler -->
<script type="text/javascript">
    $('.translation-links a').click(function() {
      var lang = $(this).data('lang');
      var $frame = $('.goog-te-menu-frame:first');
      if (!$frame.size()) {
        alert("Error: Could not find Google translate frame.");
        return false;
      }
      $frame.contents().find('.goog-te-menu2-item span.text:contains('+lang+')').get(0).click();
      return false;
    });
</script>
<!-- Lazy loading images -->
<script type="text/javascript">
    const images = document.querySelectorAll('[data-src]');
    const config = {};
    
    function preloadImage(img){
    	const src = img.getAttribute("data-src");
    	if (! src){
    		return;
    	}
    	img.src = src;
    }
    
    let observer = new IntersectionObserver(function (entries, self) {
      entries.forEach(entry => {
          if (! entry.isIntersecting) { 
             return;       	
          }else{
          	preloadImage(entry.target);
          	self.unobserve(entry.target);
          }
      });
    }, config);
    images.forEach(image => { observer.observe(image); });
</script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5ee5ded49e5f69442290857b/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>

