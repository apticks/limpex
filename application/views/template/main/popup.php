<!-- Export and Import Modal -->
<div class="modal fade" id="export-import" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Export and Import</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h3>IMPORTS:</h3>
       	<p> We are committed to Import high yielding varieties of Seeds in bulk quantity and agriculture equipment suitable to domestic Farmers for the betterment of their lives and society at Large. 
       	</p>
       	<h3>EXPORTS:</h3>
       	<p>We are committed to Export all fresh agriculture products in bulk quantity directly from Farmers Producers Organizations to the foreign customers to the maximum extent by ensuring 100% quality,
quantity and timeline. </p>
      </div>
    </div>
  </div>
</div>

<!-- Domestic Modal -->
<div class="modal fade" id="domestic" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Domestic Supply Change</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h3>DOMESTIC SUPPLY CHAIN:</h3>
       	<p> We are planning to provide a platform to the growers of organic products to market their fresh
products directly to the domestic bulk consumers.  	</p>
      </div>
    </div>
  </div>
</div>

<!-- Marketing Modal -->
<div class="modal fade" id="marketing" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Marketing & Facilitation Sevices</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       	<h3>MARKETING & FACILITATION SERVICES:</h3>
       	<p>Apart from agriculture products we undertake marketing services to the third party and
facilitate their services to the domestic suppliers / buyers / consumers to Import or Export any
material or commodity as per the requirement and standards allowed and prescribed. 
 </p>
      </div>
    </div>
  </div>
</div>

<!-- Training and Education Modal -->
<div class="modal fade" id="training" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Training & Education</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h3>TRAINING & EDUCATION:</h3>
       	<p>Give training to the domestic farmers on organic farming, pest control management, marketing
through well qualified agriculture professionals in the field and educate them in harvesting,
storage, transportation and marketing. </p>
      </div>
    </div>
  </div>
</div>