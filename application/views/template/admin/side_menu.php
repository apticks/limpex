<div class="main-sidebar sidebar-style-2">
				<aside id="sidebar-wrapper">
					<div class="sidebar-brand">
						<a href="index.html"> <img alt="image" src="<?php echo base_url();?>assets/img/limpex_logo.png" alt="Cultivation"  class="header-logo" /> 
                            <!--<span class="logo-name">Aegis</span>-->
						</a>
					</div>
					<div class="sidebar-user">
						<div class="sidebar-user-picture">
							<img alt="image" src="<?php echo base_url()?>assets/img/userbig.png">
						</div>
						<div class="sidebar-user-details">
							<div class="user-name"><?php echo $user->email;?></div>
							<div class="user-role"><?php echo $user->first_name.''.$user->last_name;?></div>
						</div>
					</div>
					<ul class="sidebar-menu">
						<li class="menu-header">Main</li>
						<li class="dropdown active"><a href="<?php echo base_url('dashboard');?>" class="nav-link "><i data-feather="airplay"></i><span>Dashboard</span> </a>
							
						</li>
						
    					<li class="dropdown"><a href="#" class="nav-link has-dropdown"><i data-feather="grid"></i><span>Catalogue</span></a>
    							<ul class="dropdown-menu">
    									<li><a class="nav-link" href="<?php echo base_url('ecom_category/r');?>">Categories</a></li>
    									<li><a class="nav-link" href="<?php echo base_url('ecom_sub_category/r');?>">Sub_Categories</a></li>
    									<li><a class="nav-link" href="<?php echo base_url('ecom_product/r');?>">Products</a></li>
    							</ul>
    					</li>
    					<li class="dropdown"><a href="<?php echo base_url('blog_admin/r');?>" class="nav-link ">
    						<i data-feather="book-open"></i><span>Blog</span> </a>
						</li>
						<li class="dropdown"><a href="<?php echo base_url('admin/contact/r');?>" class="nav-link "><i data-feather="phone-incoming"></i><span>Contact Us</span>
							</a>
						</li>
						<li class="dropdown"><a href="<?php echo base_url('subscribers');?>" class="nav-link "><i data-feather="users"></i><span>Subscribers</span>
							</a>
						</li>
						<li class="dropdown"><a href="<?php echo base_url('bulk_email/r');?>" class="nav-link "><i data-feather="message-square"></i><span>Mail</span>
							</a>
						</li>
    					<li class="dropdown"><a href="<?php echo base_url('orders');?>" class="nav-link "><i
									data-feather="truck"></i><span>Orders</span>
							</a>
						</li>
						<li class="dropdown"><a href="<?php echo base_url('admin/certificates/r');?>" class="nav-link "><i
									data-feather="truck"></i><span>Certificates</span>
							</a>
						</li>
						
						<li class="dropdown"><a href="#" class="nav-link has-dropdown"><i
									data-feather="settings"></i><span>Settings</span></a>
							<ul class="dropdown-menu">
    								<li><a class="nav-link" href="<?php echo base_url('settings/r');?>">Site Settings</a></li>
									
				            </ul>
						</li>
					</ul>
				</aside>
			</div>