<?php

class Home extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->template = 'template/main/main';
        $this->load->model('contact_model');
        $this->load->model('category_model');
        $this->load->model('sub_category_model');
        $this->load->model('product_model');
        $this->load->model('blog_model');
        $this->load->model('Subscribe_model');
        $this->load->model('Enquiry_model');
        $this->load->model('setting_model');
        $this->load->model('certificate_model');
        $this->data['blogs'] = $this->blog_model->limit(5)
            ->order_by('id', 'DESC')
            ->get_all();
    }

    public function index()
    {
        $this->data['title'] = 'Home';
        $this->data['content'] = 'home/home';
        $this->data['page'] = 'home';
        $this->data['categories'] = $this->category_model->get_all();
        $this->_render_page($this->template, $this->data);
    }

    public function about()
    {
        $this->data['title'] = 'About us';
        $this->data['content'] = 'home/about';
        $this->data['page'] = 'about';
        $this->_render_page($this->template, $this->data);
    }
    
    public function spice_growing()
    {
        $this->data['title'] = 'spice growing';
        $this->data['content'] = 'home/spice_growing';
        $this->data['page'] = 'about';
        $this->_render_page($this->template, $this->data);
    }
    
    public function medical_properties()
    {
        $this->data['title'] = 'Medical Properties of spices';
        $this->data['content'] = 'home/medical_properties';
        $this->data['page'] = 'about';
        $this->_render_page($this->template, $this->data);
    }
    public function our_team()
    {
        $this->data['title'] = 'Our Team';
        $this->data['content'] = 'home/our_team';
        $this->data['page'] = 'our_team';
        $this->_render_page($this->template, $this->data);
    }
    public function products($type = 'list')
    {
        if ($type == 'list') {
            $this->data['title'] = 'Products';
            $this->data['content'] = 'home/products';
            $this->data['page'] = 'catalogue';
            $this->data['categories'] = $this->category_model->get_all();
            $this->data['sub_categories'] = $this->db->query("SELECT sub_categories.id as id, sub_categories.name, sub_categories.desc, (SELECT COUNT(id) FROM products WHERE products.deleted_at IS null AND products.sub_cat_id = sub_categories.id) as count FROM `sub_categories` WHERE cat_id = ".$this->input->get('cat_id')." AND deleted_at IS NULL")
            ->result_array();//SELECT sub_categories.id as id, sub_categories.name, sub_categories.desc, COUNT(products.id) as count FROM `sub_categories`  LEFT JOIN products ON sub_categories.id = products.sub_cat_id WHERE products.cat_id=" . $this->input->get('cat_id') . " OR sub_categories.cat_id = ". $this->input->get('cat_id')." AND products.deleted_at IS NULL AND sub_categories.deleted_at IS NULL GROUP BY products.sub_cat_id
                $this->data['products'] = $this->product_model->where('cat_id', $this->input->get('cat_id'))
                ->where('sub_cat_id', (empty($this->input->get('sub_cat_id')) && ! empty($this->data['sub_categories'])) ? $this->data['sub_categories'][0]['id'] : $this->input->get('sub_cat_id'))
                ->get_all();
            $this->_render_page($this->template, $this->data);
        } elseif ($type == 'desc') {
            $this->data['title'] = 'Product';
            $this->data['content'] = 'home/product_desc';
            $this->data['page'] = 'catalogue';
            $this->data['categories'] = $this->category_model->get_all();
            $this->data['product'] = $this->product_model->where('id', $this->input->get('id'))
                ->get();
            $this->data['images'] = $this->db->where('product_id', $this->input->get('id'))
                ->get('product_images')
                ->result_array();
            $this->data['blog'] = $this->db->where('product_id', $this->input->get('id'))
                ->get('blog')
                ->result_array();
            $this->_render_page($this->template, $this->data);
        }
    }

    public function category()
    {
        $this->data['title'] = 'Categories';
        $this->data['content'] = 'home/categories';
        $this->data['page'] = 'catalogue';
        $this->_render_page($this->template, $this->data);
    }

    public function blog($type = 'list')
    {
        if ($type == 'list') {
            $this->data['title'] = 'Blog';
            $this->data['content'] = 'home/blog';
            $this->data['page'] = 'blog';
            $this->data['ecom_categories'] = $this->category_model->get_all();
            $this->data['ecom_sub_categories'] = $this->sub_category_model->get_all();
            $this->data['ecom_product'] = $this->product_model->get_all();
            $this->data['blogs'] = $this->blog_model->limit(5)
                ->order_by('id', 'DESC')
                ->get_all();
            $this->data['blogs_all'] = $this->blog_model->order_by('id', 'DESC')->get_all();
            $this->_render_page($this->template, $this->data);
        } elseif ($type == 'all_list') {
            $this->data['title'] = 'Blog';
            $this->data['content'] = 'home/blog_list';
            $this->data['page'] = 'blog_list';
            $this->data['ecom_categories'] = $this->category_model->get_all();
            $this->data['ecom_sub_categories'] = $this->sub_category_model->get_all();
            $this->data['ecom_product'] = $this->product_model->get_all();
            $this->data['blogs_all'] = $this->blog_model->order_by('id', 'DESC')->get_all();
            $this->_render_page($this->template, $this->data);
        } elseif ($type == 'desc') {
            $this->data['title'] = 'Blog';
            $this->data['content'] = 'home/blog_desc';
            $this->data['page'] = 'blog';
            $this->data['ecom_categories'] = $this->category_model->get_all();
            $this->data['ecom_sub_categories'] = $this->sub_category_model->get_all();
            $this->data['ecom_product'] = $this->product_model->get_all();
            $this->data['blogs_desc'] = $this->blog_model->order_by('id', 'DESC')
                ->where('id', $this->input->get('id'))
                ->get();
            $this->_render_page($this->template, $this->data);
        } elseif ($type == 'cat_desc') {
            $this->data['title'] = 'Blog';
            $this->data['content'] = 'home/cat_blog';
            $this->data['page'] = 'blog';
            $this->data['ecom_categories'] = $this->category_model->get_all();
            $this->data['cat_blogs'] = $this->blog_model->where('cat_id', $this->input->get('cat_id'))
                ->get_all();
            $this->data['blogs'] = $this->blog_model->get_all();

            // print_r($this->data['cat_blogs']);exit();
            $this->_render_page($this->template, $this->data);
        }
    }

    public function gallery()
    {
        $this->data['title'] = 'Gallery';
        $this->data['content'] = 'home/gallery';
        $this->data['page'] = 'gallery';
        $this->_render_page($this->template, $this->data);
    }

    public function contact()
    {
        $this->form_validation->set_rules($this->contact_model->rules);
        if ($this->form_validation->run() == false) {
            $this->data['title'] = 'Contact Us';
            $this->data['content'] = 'home/contact';
            $this->data['page'] = 'contact';
            $this->_render_page($this->template, $this->data);
        } else {
            $id = $this->contact_model->insert([
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'email' => $this->input->post('email'),
                'subject' => $this->input->post('subject'),
                'message' => $this->input->post('message')
            ]);
            if (! empty($id))
                $this->session->set_flashdata('success', 'Contact Details Submitted Successfully..!');
            else
                $this->session->set_flashdata('error', 'Something went wrong..!');

            redirect('contact', 'refresh');
        }
    }

    public function subscribe()
    {
        $id = $this->Subscribe_model->insert([
            'email' => $this->input->post('email')
        ]);
        if (! empty($id))
            $this->session->set_flashdata('subscribed', 'Successfully Subscribed');
        else
            $this->session->set_flashdata('error', 'Something went wrong..!');
        redirect('home', 'refresh');
    }

    public function enquiry()
    {
        $this->data['product'] = $this->product_model->where()->get();
        $id = $this->Enquiry_model->insert([
            'product_id' => $this->input->post('id'),
            'name' => $this->input->post('name'),
            'qty' => $this->input->post('qty'),
            'email' => $this->input->post('email'),
            'contact' => $this->input->post('contact'),
            'address' => $this->input->post('address')
        ]);

        redirect('home', 'refresh');
    }

    public function certificate_list()
    {
        $this->data['title'] = 'Certificate';
        $this->data['content'] = 'home/certificate';
        $this->data['page'] = 'certificate';
        $this->data['certificate'] = $this->certificate_model->get_all();
        $this->_render_page($this->template, $this->data);
    }
}