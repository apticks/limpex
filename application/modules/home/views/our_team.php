<!--Breadcrumb-->
<div class="breadcrumb_wrapper">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-md-4">
				<div class="breadcrumb_inner">
					<h3>Our Team</h3>
				</div>
			</div>
		</div>
	</div>
	<div class="breadcrumb_block">
		<ul>
			<li><a href="<?php echo base_url();?>home">home</a>
			</li>
			<li>Our Team</li>
		</ul>
	</div>
</div>
<div class="agri_team_wrapper clv_section">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-9 col-lg-9">
				<div class="agri_team_section">
					<div class="row">
						<!-- <div class="col-md-4" style="margin-left: 174px">
							<div class="agri_team_block">
								<div class="agri_team_image">
									<img data-src="<?php echo base_url();?>assets/img/team/team1.jpeg" width="250" alt="image" />
								</div>
								<div class="agri_team_overlay">
									<h4>Dr. NBT Raju</h4>
									<span>MSc(Agr), Phd In Entomology / Chairman Rtd Professor From Dr.NG Ranga Agriculture University, Hyderabad Served 35 Years Covering The Areas Of Teaching And Research.</span>
									<p>you can join us</p>
									<ul>
										<li><a href="https://www.facebook.com/Limpex-Global-100270095077286/?modal=admin_todo_tour"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a>
										</li>
										<li><a href="https://twitter.com/home?lang=en"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a>
										</li>
									</ul>
								</div>
							</div>
						</div> -->
						<div class="col-md-4"></div>
						<div class="col-md-4 justify-content-center">
							<div class="agri_team_block">
								<div class="agri_team_image">
									<img data-src="<?php echo base_url();?>assets/img/team/team2.jpeg" width="250" alt="image" />
								</div>
								<div class="agri_team_overlay">
									<h4>D.Ravi Teja Varma</h4>
									<span>Executive Director B.Com, MBA Finance</span>
									<p>you can join us</p>
									<ul>
										<li><a href="https://www.facebook.com/Limpex-Global-100270095077286/?modal=admin_todo_tour"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a>
										</li>
										<li><a href="https://twitter.com/home?lang=en"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-4"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row justify-content-center">
	<div class="col-lg-6 col-md-6">
		<div class="clv_heading">
			<h3>Our Team</h3>
			<div class="clv_underline">
				<img data-src="<?php echo base_url();?>assets/public/images/agri_underline2.png" alt="image" />
			</div>
			<p>Our team consists of Versatile members who come from various age groups with the same zeal and passion towards the purpose of Limpex. It’s a combination of enthusiastic young & well experienced members who work hard every single day to improve our efficiency of business & lives of farmers.</p>
		</div>
	</div>
</div>
<div class="container">
	<div class="col-lg-12 col-md-12">
		<div class="agri_team_section">
			<div class="row">
				<!-- <div class="col-md-3">
					<div class="agri_team_block">
						<div class="agri_team_image">
							<img data-src="<?php echo base_url();?>assets/img/team/team3.jpeg" height="289px" width="250" alt="image" />
						</div>
						<div class="agri_team_overlay">
							<h4>K A Gopalan Achary</h4>
							<span>Chief General Manager(Kerala),<br>BCOM, MBA<br> Worked in Coal india more than 34 years retired as a senior manager and joined Limpex Team</span>
							<p>you can join us</p>
							<ul>
								<li><a href="https://www.facebook.com/Limpex-Global-100270095077286/?modal=admin_todo_tour"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a>
								</li>
								<li><a href="https://twitter.com/home?lang=en"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="agri_team_block">
						<div class="agri_team_image">
							<img data-src="<?php echo base_url();?>assets/img/team/team4.jpeg" height="289px" width="250" alt="image" />
						</div>
						<div class="agri_team_overlay">
							<h4>Girish Kumar G</h4>
							<span>BBA, Regional Manager,Kerala  </span>
							<p>you can join us</p>
							<ul>
								<li><a href="https://www.facebook.com/Limpex-Global-100270095077286/?modal=admin_todo_tour"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a>
								</li>
								<li><a href="https://twitter.com/home?lang=en"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a>
								</li>
							</ul>
						</div>
					</div>
				</div> -->
				<div class="col-md-3">
					<div class="agri_team_block">
						<div class="agri_team_image">
							<img data-src="<?php echo base_url();?>assets/img/team/ceo.jpg" height="289px" width="250" alt="image" />
						</div>
						<div class="agri_team_overlay">
							<h4>Datla Seetha Rama Raju</h4>
							<span>CEO</span>
							<p>you can join us</p>
							<ul>
								<li><a href="https://www.facebook.com/Limpex-Global-100270095077286/?modal=admin_todo_tour"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a>
								</li>
								<li><a href="https://twitter.com/home?lang=en"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="agri_team_block">
						<div class="agri_team_image">
							<img data-src="<?php echo base_url();?>assets/img/team/marketing_director.jpg" height="289px" width="250" alt="image" />
						</div>
						<div class="agri_team_overlay">
							<h4>Mohammed Sanaullah</h4>
							<span>Director marketing</span>
							<p>you can join us</p>
							<ul>
								<li><a href="https://www.facebook.com/Limpex-Global-100270095077286/?modal=admin_todo_tour"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a>
								</li>
								<li><a href="https://twitter.com/home?lang=en"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="agri_team_block">
						<div class="agri_team_image">
							<img data-src="<?php echo base_url();?>assets/img/icons/HappyCustomers.png" height="289px" width="250" alt="image" />
						</div>
						<div class="agri_team_overlay">
							<h4>Coming Soon</h4>
							<span></span>
							<p>you can join us</p>
							<ul>
								<li><a href="https://www.facebook.com/Limpex-Global-100270095077286/?modal=admin_todo_tour"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a>
								</li>
								<li><a href="https://twitter.com/home?lang=en"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="agri_team_block">
						<div class="agri_team_image">
							<img data-src="<?php echo base_url();?>assets/img/icons/HappyCustomers.png" height="289px" width="250" alt="image" />
						</div>
						<div class="agri_team_overlay">
							<h4>Coming Soon</h4>
							<span></span>
							<p>you can join us</p>
							<ul>
								<li><a href="https://www.facebook.com/Limpex-Global-100270095077286/?modal=admin_todo_tour"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a>
								</li>
								<li><a href="https://twitter.com/home?lang=en"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>