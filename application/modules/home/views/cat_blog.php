<!--Breadcrumb-->
    <div class="breadcrumb_wrapper">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-4">
                    <div class="breadcrumb_inner">
                        <h3><?php echo $ecom_categories[array_search($_GET['cat_id'], array_column($ecom_categories, 'id'))]['name'];?></h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="breadcrumb_block">
            <ul>
                <li><a href="<?php echo base_url()?>">home</a></li>
                <li><?php echo $ecom_categories[array_search($_GET['cat_id'], array_column($ecom_categories, 'id'))]['name'];?></li>
            </ul>
        </div>
    </div>
    <div class="blog_sidebar_wrapper clv_section">
    <div class="container-fluid">
        <div class="row">
        <div class="col-lg-9 col-md-9">
    <div class="blog_section">
                        
                        <?php if(! empty($cat_blogs)){ $sno =1; foreach ($cat_blogs as $cat_blog):?>
                             <div class="product_item_block">
                        <div class="agri_blog_image">
                             <img data-src="<?php echo base_url();?>uploads/blog_image/blog_<?php echo $cat_blog['id'];?>.jpg" ><span class="agri_blog_date" ><?php echo date('M d, Y', strtotime($cat_blog['created_at'])); ?></span>
                        </div>
                        <div class="agri_blog_content">
                            <h3>
                                <a href="blog_single.html"><?php echo $cat_blog['title']; ?></a>
                            </h3>
                            <div class="blog_user">
                                <div class="user_name">
                                    <img data-src="<?php echo base_url()?>assets/public/images/user.png" alt="image"> <a href="javascript:;"><span><?php echo $cat_blog['author']; ?></span></a>
                                </div>
                            </div>
                            <p><?php if(strlen($blogs[0]['desc']) > 200){$pos=strpos($blogs[0]['desc'], ' ', 200);echo substr($blogs[0]['desc'],0,$pos ); }else{echo $blogs[0]['desc'];}?></p>
                                <a href="<?php echo base_url();?>blog/desc?id=<?php echo $cat_blog['id'];?>">read more <span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span></a>
                            <!-- <p><?php echo $cat_blog['desc'];?></p> -->

                            <!-- <a href="<?php echo base_url();?>blog/desc?id=<?php echo $cat_blog['id'];?>">read more <span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span></a> -->
                        </div>
                          
                    </div>
                    <?php endforeach;}else{echo "<h1 style='text-align:center'>No Blog for ".$categories[array_search($_GET['cat_id'], array_column($cat_blog, 'id'))]['name']." </h1>";}?>
                         
                </div>
                </div>
                <div class="col-lg-3 col-md-3">
                <div class="blog_sidebar">
                    <div class="sidebar_block">
                        <div class="sidebar_heading">
                            <h3>category</h3>
                            <img data-src="<?php echo base_url()?>assets/public/images/footer_underline.png" alt="image">
                        </div>
                        <div class="sidebar_category">
                            
                            <ul><?php $sno = 1; foreach($ecom_categories as $cat):?>
                                <li>
                                    <!-- <a href="<?php echo base_url();?>blog/cat_desc?id=<?php echo $mostpopular['cat_id'];?>" ><?php echo $cat['name']?></a> -->
                                    <a href="<?php echo base_url();?>blog/cat_desc?cat_id=<?php echo $cat['id'];?>"><?php echo $cat['name'];?></a>
                                <?php endforeach;?>
                            </ul>
                        </div>
                    </div>
                    <div class="sidebar_block">
                        <div class="sidebar_heading">
                            <h3>recent post</h3>

                            <img data-src="<?php echo base_url()?>assets/public/images/footer_underline.png" alt="image">
                        </div>
                        <div class="sidebar_post">
                            <ul><?php $sno = 1; foreach($blogs as $cat):?>
                                <li>
                                    <div class="post_image">
                                        <!-- <img data-src="<?php echo base_url()?>assets/public/images/sidebar_post1.jpg" alt="image"> -->
                                        <img data-src="<?php echo base_url();?>uploads/blog_image/blog_<?php echo $cat['id'];?>.jpg" alt="image" style="width: 69px;height: 50px;">
                                    </div>
                                    <div class="post_content">
                                        <p><?php echo date('M d, Y', strtotime($cat['created_at']))?></p>
                                        <a href="<?php echo base_url();?>blog/desc?id=<?php echo $cat['id'];?>"><?php echo $cat['title']?></a>
                                    </div>
                                </li>
                                
                                <?php endforeach;?>
                            </ul>
                            <a href="<?php echo base_url('blog/all_list');?>">View All <span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span></a>
                        </div>
                    </div>
                </div>
            </div>
            </div>
</div>    
</div>