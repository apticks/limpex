<!--Breadcrumb-->
<div class="breadcrumb_wrapper">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-md-4">
				<div class="breadcrumb_inner">
					<h3>CERTIFICATE</h3>
				</div>
			</div>
		</div>
	</div>
	<div class="breadcrumb_block">
		<ul>
			<li><a href="<?php echo base_url('home')?>">home</a></li>
			<li>CERTIFICATE</li>
		</ul>
	</div>
</div>
 
		<div class="clv_section">
			<div class="container-fluid">
				<div class="row justify-content-center">
					<div class="col-lg-6 col-md-6">
						<div class="clv_heading">
							<h3>CERTIFICATE</h3>
							<div class="clv_underline"><img loading="lazy" src="<?php echo base_url();?>assets/public/images/footer_underline.png" alt="image"></div>
							<!-- <p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dole magna aliqua. Ut enim ad minim veniam quis nostrud exercitation.</p> -->
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="gallery_slide">
								<div class="gallery_grid">
									<?php foreach ($certificate as $certificates):?>
									<div class="gallery_grid_item">
										<div class="gallery_image">
											<h1><?php echo $certificates['title'];?></h1><hr/>
											<img loading="lazy" src="<?php echo base_url()?>uploads/certificates_image/certificates_<?php echo $certificates['id'];?>.jpg?<?php echo time();?>">
											<div class="gallery_overlay">
												<a href="<?php echo base_url()?>uploads/certificates_image/certificates_<?php echo $certificates['id'];?>.jpg?<?php echo time();?>" class="view_image"><span><i class="fa fa-search" aria-hidden="true"></i></span></a>
											</div>
										</div>
									</div>
									<?php endforeach;?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end Mixpanel -->
	<script type="text/javascript">
    	mixpanel.track("<?php echo $title;?>");
	</script>
<!-- end Mixpanel -->