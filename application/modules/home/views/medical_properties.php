 <!--Breadcrumb-->
		<div class="breadcrumb_wrapper">
			<div class="container-fluid" style="background-image: url(<?php echo base_url();?>assets/img/icons/common back.png);">
				<div class="row justify-content-center">
					<div class="col-md-4">
						<div class="breadcrumb_inner">
							<h3>Medicinal & other Properties of Spices</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="breadcrumb_block">
				<ul>
					<li><a href="<?php echo base_url('home');?>">home</a></li>
					<li>Medicinal & other Properties of Spices</li>
				</ul>
			</div>
		</div>
		<!--About Section-->
		<div class="clv_about_wrapper">
			<div class="container-fluid">
				<div class="col-md-12 col-lg-12 col-xs-12">
					<embed src="https://www.indianspices.com/sites/default/files/medicinal%20_other_values_spices.pdf" class="pdf-embd"style="padding: 50px; margin: 10px 120px" width="1000px" height="1000px" 
 type="application/pdf">
				</div>
			</div>
		</div>
		<!-- end Mixpanel -->
	<script type="text/javascript">
    	mixpanel.track("<?php echo $title;?>");
	</script>
<!-- end Mixpanel -->