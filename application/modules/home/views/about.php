<!--Breadcrumb-->
		<div class="breadcrumb_wrapper">
			<div class="container-fluid" style="background-image: url(<?php echo base_url();?>assets/img/icons/common back.png);">
				<div class="row justify-content-center">
					<div class="col-md-4">
						<div class="breadcrumb_inner">
							<h3>about us</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="breadcrumb_block mt-3">
				<ul>
					<li><a href="<?php echo base_url('home');?>">home</a></li>
					<li>about us</li>
				</ul>
			</div>
		</div>
		<!--About Section-->
		<div class="clv_about_wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="about_content">
							<div class="about_heading">
								<h2>Welcome to Our <span>Limpex</span></h2>
								<div class="clv_underline"><img data-src="<?php echo base_url()?>assets/public/images/underline.png" alt="image" /></div>
							</div>
							<p>We are very excited to provide taste of LIMPEX Global Private Limited , a different approach on quality of agricultural products sourced directly from Farmers (Farmers Producers Organizations)to foreign buyers . But before you do,we wanted to share OUR STORY with you .</p>
							<p>We come with known line age of FOUR generations over 80 years in farmers' family covering most of the spices and pulses in south India and now ventured in export business, being a member of FPO and also Liais on with more than 128 FPOs covering over 143K farmers around the country directly/indirectly with a motive to provide reasonable consideration/price to the farmers and quality oriented agri-products to foreign buyers.</p>
							<p>We noticed that most of the agricultural products are being sold across the world pass through multiple middlemen before reaching buyers abroad like you.</p>
							<p>By then, most of the <strong>PRIME Flavor, Freshness, and fragrance of the spices reduce to considerable extent.</strong> Additionally, even though there is a huge demand for spices globally every year, millions of spice growers back here in India are adversely affected with problems , like <strong>low prices to their farm products and an uncertain future,</strong> inspite of support from local state governments to protect their interests.   India grows some of the finest spices in the world (50% of the world's production), the absence of front line support to Farmers has made the spice industry directly dependent on bulk exports in addition to local consumers. Further such middlemen/dealers/brands do not hesitate to compromise on quality in order to compete on price points and earn higher profit margins; on the other hand farmers back home in India are getting a significantly lower price.</p>
							<p>By looking in to the total scenario , we initiated a journey to covert hese aspects, and decided to start LIMPEX Global Private Limited,<strong>in the name of our beloved family pillar Smt Lalitha Kumari ."Limpex stands for Lalitha imports and exorts".</strong> </p>
							<p>We are unique in the field of exports by having time to time contacts right from the days of sowing, pest control, crop management, harvesting,and final procurement at our state-of the-art consultancy facility being provided through agricultural professionals and after processing commodity would be shipped directly to our foreign customers based on their requirements.</p>
							<p>Our personal touch and attention to each transaction adds true value that brings a positive impact on our customer's growt hand prosperity. We understand that each customer has a unique requirement.</p>
							<p>Apart from eliminating middlemen at maximum extent, we are able to make available <strong>garden fresh high-quality spices and agri-products</strong> to consumers across the globe, and reasonable earnings to the farmers who pays immense love,care, passion and livelihood .</p>
							<div class="garden_green_box">
    							<div class="green_box_content">
    								<h4>We always respect our VEDIC THEME OF</h4>
    								<p><strong>&quot;</strong>Om Sarve Bhavantu Sukhinah</p>  <p>Sarve Santu Niraamayaah |</p> <p>Sarve Bhadraanni Pashyantu</p> <p>Maa Kashcid-Duhkha-Bhaag-Bhavet |</p> <p>Om Shaantih Shaantih Shaantih<strong>&quot;</strong></p>
    								<hr style="border-top: 1px solid rgb(255, 192, 37);"/>
    								<h4>Meaning:</h4>
    								<ol style="color: #ffff;margin: 0px 22px;">
                                      <li>Om, May All be Happy, </li>
                                      <li>May All be Free from Illness. </li>
                                      <li>May All See what is Auspicious, </li>
                                      <li>May no one Suffer.  </li>
                                      <li>Om Peace, Peace, Peace. </li>
                                    </ol>
    							</div>
    						</div><br><br>
    						<p>We are also confident that once our basic fabric of trust is established with honest buyers, people will keep coming back to us. We believe that with the right combination of ingredients, quality, quantity, value for money and timely delivery offers immense advantages to our consumers.</p>
						</div>
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
		</div>
		<!--Team-->
		<div class="clv_team_wrapper">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-6 col-md-6">
						<div class="clv_heading">
							<h3>our team</h3>
							<div class="clv_underline"><img data-src="<?php echo base_url()?>assets/public/images/underline3.png" alt="image" /></div>
							<p>Our team consists of Versatile members who come from various age groups with the same zeal and passion towards the purpose of Limpex. It’s a combination of enthusiastic young & well experienced members who work hard every single day to improve our efficiency of business & lives of farmers. </p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="team_section">
							<div class="row justify-content-center">
								<!-- <div class="col-md-3">
									<div class="team_block">
										<div class="team_image">
											<img data-src="<?php echo base_url();?>assets/img/team/team1.jpeg" width="210" alt="image" />
											<div class="social_overlay">
												<p>you can join us</p>
												<ul>
													<li><a href="javascript:;"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a></li>
													<li><a href="javascript:;"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a></li>
												</ul>
											</div>
										</div>
										<div class="team_details">
											<div class="team_name">
												<h3>Dr. NBT Raju</h3>
												<p> MSc(Agr), Phd in Entomology / Chairman Rtd Professor from Dr.NG Ranga Agriculture University, Hyderabad served 35 years covering the areas of teaching and research. </p>
												<span class="divider"></span>
												<a href="javascript:;">raju@limpex.in</a>
											</div>
										</div>
									</div>
								</div> -->
								<div class="col-md-4"></div>
								<div class="col-md-3">
									<div class="team_block">
										<div class="team_image">
											<img data-src="<?php echo base_url();?>assets/img/team/team2.jpeg"  width="210"  alt="image" />
											<div class="social_overlay">
												<p>you can join us</p>
												<ul>
													<li><a href="javascript:;"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a></li>
													<li><a href="javascript:;"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a></li>
												</ul>
											</div>
										</div>
										<div class="team_details">
											<div class="team_name">
												<h3>D.Ravi Teja Varma</h3>
												<p> Executive Director B.Com, MBA Finance </p><br><br><br>
												<span class="divider"></span>
												<a href="javascript:;">ravi@limpex.in</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end Mixpanel -->
	<script type="text/javascript">
    	mixpanel.track("<?php echo $title;?>");
	</script>
<!-- end Mixpanel -->
