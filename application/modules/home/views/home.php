<?php if(!empty($this->session->flashdata('subscribed'))){ ?>

    <script type='text/javascript'>
        window.alert('Successfully Subscribed!!!')
    </script>

    <?php }?>
    <!--Revolution Slider-->
	<div class="clv_rev_slider">
		<div id="rev_slider_1164_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="exploration-header" data-source="gallery" style="background-color:transparent;padding:0px;">
			<!-- START REVOLUTION SLIDER 5.4.1 fullscreen mode -->
				<div id="rev_slider_1164_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.1">
			<ul>	<!-- SLIDE  -->
				<li data-index="rs-3204" data-transition="slideoververtical" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="../../assets/<?php echo base_url();?>assets/public/images/news1-1-100x50.jpg"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="2000" data-fsslotamount="7" data-saveperformance="off"  data-title="" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
					<!-- MAIN IMAGE -->
					<img src="<?php echo base_url();?>assets/img/home_slider/2_1.jpg"  alt="image"  data-lazyload="" data-bgposition="center bottom" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
					<!-- LAYERS -->
			
					<!-- LAYER NR. 1 -->
					<div class="tp-caption  " 
						 id="slide-3204-layer-1" 
						 data-x="['left','left','left','left']" data-hoffset="['364','114','76','26']" 
						 data-y="['top','top','top','top']" data-voffset="['262','252','195','185']" 
									data-fontsize="['20','20','20','20']"
						data-lineheight="['22','22','22','22']"
						data-width="['700','700','700','700']"
						data-height="none"
						data-whitespace="normal"
			     
						data-type="text" 
						data-basealign="slide" 
						data-responsive_offset="on" 
						data-responsive="on"
						data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
						data-textAlign="['left','left','left','left']"
						data-paddingtop="[0,0,0,0]"
						data-paddingright="[0,0,0,0]"
						data-paddingbottom="[0,0,0,0]"
						data-paddingleft="[0,0,0,0]"
			
						style="z-index: 5; white-space: nowrap; font-size: 20px; font-weight: 700; color: #1fa12e; display: inline-block;font-family:'Source Sans Pro', sans-serif;letter-spacing:3px;">A New Way To Invest In Agriculture</div>
			
						<div class="tp-caption tp-shape tp-shapewrapper " 
						id="slide-3204-layer-2" 
						data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
								data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
										   data-width="full"
							   data-height="full"
							   data-whitespace="normal"
					
							   data-type="shape" 
							   data-basealign="slide" 
							   data-responsive_offset="off" 
							   data-responsive="off"
							   data-frames='[{"delay":10,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
							   data-textAlign="['inherit','inherit','inherit','inherit']"
							   data-paddingtop="[0,0,0,0]"
							   data-paddingright="[0,0,0,0]"
							   data-paddingbottom="[0,0,0,0]"
							   data-paddingleft="[0,0,0,0]"

							   style="z-index: 1;background-color:rgba(0,0,0,0.70);"> </div>
			
					<!-- LAYER NR. 3 -->
					<div class="tp-caption  " 
						 id="slide-3204-layer-3" 
						 data-x="['left','left','left','left']" data-hoffset="['364','114','76','26']" 
						 data-y="['top','top','top','top']" data-voffset="['302','352','235','225']" 
									data-width="['100%','100%','100%','100%']"
									data-fontsize="['82','82','40','40']"
								data-lineheight="['82','82','40','40']"
						data-height="none"
						data-whitespace="normal"
			 
						data-type="text" 
						data-basealign="slide" 
						data-responsive_offset="on" 
						data-responsive="off"
						data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":650,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
						data-textAlign="['left','left','left','left']"
						data-paddingtop="[0,0,0,0]"
						data-paddingright="[0,0,0,0]"
						data-paddingbottom="[0,0,0,0]"
						data-paddingleft="[0,0,0,0]"
			
						style="z-index: 7; font-size: 82px; line-height: 81px; font-weight: 300; color: rgba(255, 255, 255, 1); display: block;font-family:'Source Sans Pro', sans-serif;">Experience The Real</div>

					<!-- LAYER NR. 4 -->
					<div class="tp-caption  " 
						 id="slide-3204-layer-4" 
						 data-x="['left','left','left','left']" data-hoffset="['364','118','76','26']" 
						 data-y="['top','top','top','top']" data-voffset="['382','272','305','295']" 
									data-width="['100%','100%','100%','100%']"
									data-fontsize="['82','82','40','40']"
								data-lineheight="['82','82','40','40']"
						data-height="none"
						data-whitespace="normal"
			 
						data-type="text" 
						data-basealign="slide" 
						data-responsive_offset="on" 
						data-responsive="off"
						data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":650,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
						data-textAlign="['left','left','left','left']"
						data-paddingtop="[0,0,0,0]"
						data-paddingright="[0,0,0,0]"
						data-paddingbottom="[0,0,0,0]"
						data-paddingleft="[0,0,0,0]"
			
						style="z-index: 7; font-size: 82px; line-height: 82px; font-weight: 700; color: rgba(255, 255, 255, 1); display: block;font-family:'Source Sans Pro', sans-serif;">Agriculture</div>

					</li>
				<!-- SLIDE  -->
				<li data-index="rs-3205" data-transition="slideoververtical" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="../../assets/<?php echo base_url();?>assets/public/images/news2-1-100x50.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
					<!-- MAIN IMAGE -->
					<img src="<?php echo base_url();?>assets/img/home_slider/3_1.jpg"  alt="image"  data-lazyload="" data-bgposition="center bottom" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
					<!-- LAYERS -->
			
					<!-- LAYER NR. 5 -->
					<div class="tp-caption  " 
						 id="slide-3205-layer-1" 
						 data-x="['left','left','left','left']" data-hoffset="['364','114','76','26']" 
						 data-y="['top','top','top','top']" data-voffset="['262','252','195','185']" 
									data-fontsize="['20','20','20','20']"
						data-lineheight="['22','22','22','22']"
						data-width="['700','700','700','700']"
						data-height="none"
						data-whitespace="normal"
			     
						data-type="text" 
						data-basealign="slide" 
						data-responsive_offset="on" 
						data-responsive="on"
						data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
						data-textAlign="['left','left','left','left']"
						data-paddingtop="[0,0,0,0]"
						data-paddingright="[0,0,0,0]"
						data-paddingbottom="[0,0,0,0]"
						data-paddingleft="[0,0,0,0]"
			
						style="z-index: 5; white-space: nowrap; font-size: 20px; font-weight: 700; color: #1fa12e; display: inline-block;font-family:'Source Sans Pro', sans-serif;letter-spacing:3px;"></div>
			
					<!-- LAYER NR. 6 -->
					<div class="tp-caption tp-shape tp-shapewrapper " 
						 id="slide-3205-layer-2" 
						 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
								 data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
											data-width="full"
								data-height="full"
								data-whitespace="normal"
					 
								data-type="shape" 
								data-basealign="slide" 
								data-responsive_offset="off" 
								data-responsive="off"
								data-frames='[{"delay":10,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
								data-textAlign="['inherit','inherit','inherit','inherit']"
								data-paddingtop="[0,0,0,0]"
								data-paddingright="[0,0,0,0]"
								data-paddingbottom="[0,0,0,0]"
								data-paddingleft="[0,0,0,0]"

								style="z-index: 1;background-color:rgba(0,0,0,0.70);"> </div>
			
					<!-- LAYER NR. 7 -->
					<div class="tp-caption  " 
						 id="slide-3205-layer-3" 
						 data-x="['left','left','left','left']" data-hoffset="['364','114','76','26']" 
						 data-y="['top','top','top','top']" data-voffset="['302','352','235','225']" 
									data-width="['100%','100%','100%','100%']"
									data-fontsize="['82','82','40','40']"
								data-lineheight="['82','82','40','40']"
								
						data-height="none"
						data-whitespace="normal"
			 
						data-type="text" 
						data-basealign="slide" 
						data-responsive_offset="on" 
						data-responsive="off"
						data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":650,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
						data-textAlign="['left','left','left','left']"
						data-paddingtop="[0,0,0,0]"
						data-paddingright="[0,0,0,0]"
						data-paddingbottom="[0,0,0,0]"
						data-paddingleft="[0,0,0,0]"
			
						style="z-index: 7; font-size: 82px; line-height: 81px; font-weight: 300; color: rgba(255, 255, 255, 1); display: block;font-family:'Source Sans Pro', sans-serif;"><!-- Experience The Real --></div>
					
						<!-- LAYER NR. 8 -->
					<div class="tp-caption  " 
						 id="slide-3205-layer-4" 
						 data-x="['left','left','left','left']" data-hoffset="['364','114','76','26']" 
						 data-y="['top','top','top','top']" data-voffset="['382','272','305','295']" 
									data-width="['100%','100%','100%','100%']"
									data-fontsize="['82','82','40','40']"
									data-lineheight="['82','82','40','40']"
						data-height="none"
						data-whitespace="normal"
			 
						data-type="text" 
						data-basealign="slide" 
						data-responsive_offset="on" 
						data-responsive="off"
						data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":650,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
						data-textAlign="['left','left','left','left']"
						data-paddingtop="[0,0,0,0]"
						data-paddingright="[0,0,0,0]"
						data-paddingbottom="[0,0,0,0]"
						data-paddingleft="[0,0,0,0]"
			
						style="font-size: 82px; line-height: 82px; font-weight: 700; margin-right: 10px; color: rgba(255, 255, 255, 1); display: block;font-family:'Source Sans Pro', sans-serif; text-align: right"><p style="text-align: center; margin-top: -169px;">Save the<br> Planet...<br>Buy <br>Organic!!!</p></div>
					<!-- LAYER NR. 9 -->
					<!-- <a class="tp-caption rev-btn clv_btn" 
						href="javascript:;"
						id="slide-3205-layer-5" 
						data-x="['left','left','left','left']" data-hoffset="['364','114','76','26']" 
						data-y="['top','top','top','top']" data-voffset="['489','550','385','376']" 
						data-fontweight="['500','500','500','500']"
						data-width="none"
						data-height="none"
						data-whitespace="nowrap"
						data-type="button" 
						data-basealign="slide" 
						data-responsive_offset="on" 
						data-responsive="off"
						data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":800,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
						data-textAlign="['left','left','left','left']"
						data-paddingtop="[0,0,0,0]"
						data-paddingright="[45,45,45,45]"
						data-paddingbottom="[0,0,0,0]"
						data-paddingleft="[45,45,45,45]"
						style="z-index: 8; white-space: nowrap;">read more</a> -->
				</li>
				
			</ul>
			<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>	</div>
			</div><!-- END REVOLUTION SLIDER -->
	</div>
	<!--Revolution Silder-->
	
	<!--Categories-->
	<div class="org_service_wrapper clv_section">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="org_left_service" style="background-image: linear-gradient(#008c5c, #ffc025);">
						<div class="service_description" >
							<h3>awesome organic products</h3>
							<!-- <img src="<?php echo base_url();?>assets/img/home_slider/2.jpg"  alt="image" style="background-color:transparent;padding:0px;"> -->
							
							<!-- <img src="<?php echo base_url();?>assets/public/images/org_underline.png" alt="image"> -->
							<p>India is the land of farmers and they are backbone of Indian economy. Our efforts to export agri products is a bit of support for sustainable development.

</p>
						</div>
						<div class="service_contact">
							<span>
								
								<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 480.56 480.56" style="enable-background:new 0 0 480.56 480.56;" xml:space="preserve" width="32px" height="32px">
								<g>
									<g>
										<path style="fill: #fec007;" d="M365.354,317.9c-15.7-15.5-35.3-15.5-50.9,0c-11.9,11.8-23.8,23.6-35.5,35.6c-3.2,3.3-5.9,4-9.8,1.8
											c-7.7-4.2-15.9-7.6-23.3-12.2c-34.5-21.7-63.4-49.6-89-81c-12.7-15.6-24-32.3-31.9-51.1c-1.6-3.8-1.3-6.3,1.8-9.4
											c11.9-11.5,23.5-23.3,35.2-35.1c16.3-16.4,16.3-35.6-0.1-52.1c-9.3-9.4-18.6-18.6-27.9-28c-9.6-9.6-19.1-19.3-28.8-28.8
											c-15.7-15.3-35.3-15.3-50.9,0.1c-12,11.8-23.5,23.9-35.7,35.5c-11.3,10.7-17,23.8-18.2,39.1c-1.9,24.9,4.2,48.4,12.8,71.3
											c17.6,47.4,44.4,89.5,76.9,128.1c43.9,52.2,96.3,93.5,157.6,123.3c27.6,13.4,56.2,23.7,87.3,25.4c21.4,1.2,40-4.2,54.9-20.9
											c10.2-11.4,21.7-21.8,32.5-32.7c16-16.2,16.1-35.8,0.2-51.8C403.554,355.9,384.454,336.9,365.354,317.9z"></path>
										<path style="fill: #fec007;" d="M346.254,238.2l36.9-6.3c-5.8-33.9-21.8-64.6-46.1-89c-25.7-25.7-58.2-41.9-94-46.9l-5.2,37.1
											c27.7,3.9,52.9,16.4,72.8,36.3C329.454,188.2,341.754,212,346.254,238.2z"></path>
										<path style="fill: #fec007;" d="M403.954,77.8c-42.6-42.6-96.5-69.5-156-77.8l-5.2,37.1c51.4,7.2,98,30.5,134.8,67.2c34.9,34.9,57.8,79,66.1,127.5
											l36.9-6.3C470.854,169.3,444.354,118.3,403.954,77.8z"></path>
									</g>
								</g>
								</svg>
							</span>
							<h4>( +40 ) 48-55-0189</h4>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="org_right_service">
						<div class="row">
							<?php foreach ($categories as $category):?>
							<div class="col-md-3">
								<div class="service_block">
									<a href="<?php echo base_url()?>product/list?cat_id=<?php echo $category['id'];?>&sub_cat_id=0">
    									<img data-src="<?php echo base_url();?>uploads/ecom_category_image/ecom_category_<?php echo $category['id']?>.jpg" alt="image">
    									<h3><?php echo $category['name']?></h3>
									</a>
								</div>
							</div>
							<?php endforeach;?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

	<!--Agriculture Services-->
	<div class="agri_service_wrapper clv_section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-6 col-md-6">
					<div class="clv_heading">
						<h3>top services</h3>
						<div class="clv_underline"><img data-src="<?php echo base_url();?>assets/public/images/agri_underline2.png" alt="image" /></div>
<!-- 						<p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dole magna aliqua. Ut enim ad minim veniam quis nostrud exercitation.</p> -->
					</div>
				</div>
			</div>
			<div class="row justify-content-end">
				<div class="col-lg-6 col-md-6 agri_boy_image">
			<img data-src="<?php echo base_url();?>assets/img/icons/top_services_Main.png" alt="image"/>
		</div>
				<div class="col-lg-6 col-md-6">
					<div class="agri_service_inner">
						<div class="row">
							<div class="col-md-6">
								<div class="agri_service_section spacer">
									<a href="javascript:;" data-toggle="modal" data-target="#export-import">
									<div class="agri_service_block">
										<div class="agrice_service_image">
											<img data-src="<?php echo base_url();?>assets/img/icons/Import-Export1.png" alt="image" style="height: 100px;"/>

										</div>
										<h4>Export/Import</h4>
									</div>
									</a>
									<a href="javascript:;" data-toggle="modal" data-target="#marketing">
									<div class="agri_service_block">
										<div class="agrice_service_image">
											<img data-src="<?php echo base_url();?>assets/img/icons/Market.png" alt="image" style="height: 121px;"/>
										</div>
										<h4>Marketing & Facilitation Sevices</h4>
									</div>
									</a>
								</div>
							</div>
							<div class="col-md-6">
								<div class="agri_service_section">
									<a href="javascript:;" data-toggle="modal" data-target="#domestic">
									<div class="agri_service_block">
										<div class="agrice_service_image">
											<img data-src="<?php echo base_url();?>assets/img/icons/Domestic_Chain.png" alt="image" style="height: 121px;"/>
										</div>
										<h4>Domestic Supply Chain</h4>
									</div>
									</a>
									<a href="javascript:;" data-toggle="modal" data-target="#training">
									<div class="agri_service_block">
										<div class="agrice_service_image">
											<img data-src="<?php echo base_url();?>assets/img/icons/educatin_event.png" alt="image" style="height: 121px;" />
										</div>
										<h4>Training & Education</h4>
									</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<!--Counter Section-->
	<div class="clv_counter_wrapper clv_section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-6 col-md-6">
					<div class="clv_heading">
						<h3>we are an expert in this field</h3>
						<div class="clv_underline"><img data-src="<?php echo base_url();?>assets/public/images/agri_underline2.png" alt="image" /></div>
						<p>Agriculture is our wisest pursuit, because it will in the end contribute most to real wealth, good morals, and happiness. To make agriculture sustainable, the grower has got to be able to make a profit.</p>
					</div>
				</div>
			</div>
			<div class="counter_section">
				<div class="row">
					<div class="col-lg-3 col-md-3">
						<div class="counter_block">
							<div class="counter_img">
								<span><img data-src="<?php echo base_url();?>assets/img/icons/fpo.png" alt="image" class="img-fluid" /></span>
							</div>
							<div class="counter_text">
								<h4 class="blue_color"><span class="count_no" data-to="1" data-speed="3000">1</span><span>28</span></h4>
								<h5>FPO's/NGO's</h5>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-3">
						<div class="counter_block">
							<div class="counter_img">
								<span><img data-src="<?php echo base_url();?>assets/img/icons/HappyCustomers.png" alt="image" class="img-fluid" /></span>
							</div>
							<div class="counter_text">
								<h4 class="red_color"><span class="count_no" data-to="140" data-speed="3000">140</span><span>k+</span></h4>
								<h5>FARMERS </h5>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-3">
						<div class="counter_block">
							<div class="counter_img">
								<span><img data-src="<?php echo base_url();?>assets/img/icons/OrdersCompleted.png" alt="image" class="img-fluid" /></span>
							</div>
							<div class="counter_text">
								<h4 class="yellow_color"><span class="count_no" data-to="200" data-speed="2000">200</span><span>k+</span></h4>
								<h5>ACRES</h5>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-3">
						<div class="counter_block">
							<div class="counter_img">
								<span><img data-src="<?php echo base_url();?>assets/img/icons/certifications.png" alt="image" class="img-fluid" /></span>
							</div>
							<div class="counter_text">
								<h4 class="blue_color"><span class="count_no" data-to="10" data-speed="3000">10</span><span>+</span></h4>
								<h5>CERTIFICATION</h5>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	<!--Agriculture Testimonial-->
<!-- 	<div class="agri_testimonial_wrapper clv_section"> -->
<!-- 		<div class="container"> -->
<!-- 			<div class="row"> -->
<!-- 				<div class="col-md-4 col-lg-4"> -->
<!-- 					<div class="agri_testimonial_content"> 
						<h3>what client say <span><img src="<?php echo base_url();?>assets/public/images/agri_underline3.png" alt="image" /></span></h3>
						<p>Consectetur adipisicing elit sed do eiusmod tempor  dunt ut labore et dole magna aliqua. Ut enim ad minim veniam quis nostrud exercitation.</p> -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 				<div class="col-md-8 col-lg-8"> -->
<!-- 					<div class="agri_testimonial_slider"> -->
						<!-- Swiper -->
<!-- 						<div class="swiper-container"> -->
<!-- 							<div class="swiper-wrapper"> -->
<!-- 								<div class="swiper-slide"> -->
<!-- 									<div class="agri_testimonial_slide"> -->
<!-- 										<div class="agri_testimonial_image"> 
											<img src="<?php echo base_url();?>assets/public/images/agri_testimonial.png" alt="image" />
 										</div> -->
<!-- 										<div class="agri_testimonial_message"> -->
<!-- 											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempeior incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam quisesa fei nostrud exercitation ullamco laboris aliquip.</p> -->
<!-- 											<h4>Merica Ven <span>Garderner</span></h4> -->
<!-- 										</div> 
										<span class="agri_quate"><img src="<?php echo base_url();?>assets/public/images/Quote-Icon.png" alt="image" /></span>
 									</div> -->
<!-- 								</div> -->
<!-- 								<div class="swiper-slide"> -->
<!-- 									<div class="agri_testimonial_slide"> -->
<!-- 										<div class="agri_testimonial_image"> 
											<img src="<?php echo base_url();?>assets/public/images/agri_testimonial2.png" alt="image" />
 										</div> -->
<!-- 										<div class="agri_testimonial_message"> -->
<!-- 											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempeior incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam quisesa fei nostrud exercitation ullamco laboris aliquip.</p> -->
<!-- 											<h4>Merica Ven <span>Garderner</span></h4> -->
<!-- 										</div> 
										<span class="agri_quate"><img src="<?php echo base_url();?>assets/public/images/Quote-Icon.png" alt="image" /></span>
									</div> -->
<!-- 								</div> -->
<!-- 							</div> -->
<!-- 						</div> -->
						<!-- Add Arrows -->
<!-- 						<div class="agri_arrow agri_testimonial_left"> -->
<!-- 							<span> -->
<!-- 								<svg  -->
<!-- 								 xmlns="http://www.w3.org/2000/svg" -->
<!-- 								 xmlns:xlink="http://www.w3.org/1999/xlink" -->
<!-- 								 width="10px" height="15px"> -->
<!-- 								<path fill-rule="evenodd"  fill="rgb(222, 222, 222)" -->
<!-- 								 d="M0.324,8.222 L7.117,14.685 C7.549,15.097 8.249,15.097 8.681,14.685 C9.113,14.273 9.113,13.608 8.681,13.197 L2.670,7.478 L8.681,1.760 C9.113,1.348 9.113,0.682 8.681,0.270 C8.249,-0.139 7.548,-0.139 7.116,0.270 L0.323,6.735 C0.107,6.940 -0.000,7.209 -0.000,7.478 C-0.000,7.747 0.108,8.017 0.324,8.222 Z"/> -->
<!-- 								</svg> -->
<!-- 							</span> -->
<!-- 						</div> -->
<!-- 						<div class="agri_arrow agri_testimonial_right"> -->
<!-- 							<span> -->
<!-- 								<svg  -->
<!-- 								 xmlns="http://www.w3.org/2000/svg" -->
<!-- 								 xmlns:xlink="http://www.w3.org/1999/xlink" -->
<!-- 								 width="19px" height="25px"> -->
<!-- 								<path fill-rule="evenodd" fill="rgb(222, 222, 222)" -->
<!-- 								 d="M13.676,13.222 L6.883,19.685 C6.451,20.097 5.751,20.097 5.319,19.685 C4.887,19.273 4.887,18.608 5.319,18.197 L11.329,12.478 L5.319,6.760 C4.887,6.348 4.887,5.682 5.319,5.270 C5.751,4.861 6.451,4.861 6.884,5.270 L13.676,11.735 C13.892,11.940 14.000,12.209 14.000,12.478 C14.000,12.747 13.892,13.017 13.676,13.222 Z"/> -->
<!-- 								</svg> -->
<!-- 							</span> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 			</div> -->
<!-- 		</div> -->
<!-- 	</div> -->
	<!--Agriculture Blog-->
<!-- 	<div class="agri_blog_wrapper clv_section"> -->
<!-- 		<div class="container"> -->
<!-- 			<div class="row justify-content-center"> -->
<!-- 				<div class="col-lg-6 col-md-6"> -->
<!-- 					<div class="clv_heading"> -->
<!-- 						<h3>our recent post</h3> 
						<div class="clv_underline"><img src="<?php echo base_url();?>assets/public/images/agri_underline2.png" alt="image" /></div>
 						<p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dole magna aliqua. Ut enim ad minim veniam quis nostrud exercitation.</p> -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 			</div> -->
<!-- 			<div class="agri_blog_inner"> -->
<!-- 				<div class="row"> -->
<!-- 					<div class="col-lg-6 col-md-6"> -->
<!-- 						<div class="blog_section"> -->
<!-- 							<div class="agri_blog_image"> 
								<img src="<?php echo base_url();?>uploads/blog_image/blog_<?php echo $blogs[0]['id'];?>.jpg" alt="image" />
								<span class="agri_blog_date"><?php echo date('M d, Y', strtotime($blogs[0]['created_at']))?></span>
							</div> -->
<!-- 							<div class="agri_blog_content">
								<h3><a href="<?php echo base_url();?>blog/desc?id=<?php echo $blogs[0]['id'];?>"><?php echo $blogs[0]['title']?></a></h3>
 								<div class="blog_user"> -->
<!-- 									<div class="user_name">
										<img src="<?php echo base_url();?>assets/public/images/user.png" alt="image" />
										<a href="javascript:;"><span><?php echo $blogs[0]['author']?></span></a>
									</div> -->
<!-- 								</div>
								<p><?php if(strlen($blogs[0]['desc']) > 200){$pos=strpos($blogs[0]['desc'], ' ', 200);echo substr($blogs[0]['desc'],0,$pos ); }else{echo $blogs[0]['desc'];}?></p>
								<a href="<?php echo base_url();?>blog/desc?id=<?php echo $blogs[0]['id'];?>">read more <span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span></a>
							</div> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 					<div class="col-lg-6 col-md-6"> -->
<!-- 						<div class="right_blog_section"> -->
<!-- 							<div class="right_blog_block"> -->
<!-- 								<div class="right_blog_image"> 
								<img src="<?php echo base_url();?>uploads/blog_image/blog_<?php echo $blogs[1]['id'];?>.jpg" alt="image" />
 								</div> -->
<!-- 								<div class="right_blog_content">
									<span class="agri_blog_date"><?php echo date('M d, Y', strtotime($blogs[1]['created_at']))?></span>
									<h3><a href="<?php echo base_url();?>blog/desc?id=<?php echo $blogs[1]['id'];?>"><?php echo $blogs[1]['title']?></a></h3>
									<div class="blog_user"> -->
<!-- 										<div class="user_name">
											<img src="<?php echo base_url();?>assets/public/images/user.png" alt="image" />
											<a href="javascript:;"><span><?php //echo $blogs[1]['author']?></span></a>
 										</div> -->
<!-- 									</div> 
									<p><?php //if(strlen($blogs[1]['desc']) > 200){$pos=strpos($blogs[1]['desc'], ' ', 200);echo substr($blogs[1]['desc'],0,$pos ); }else{echo $blogs[1]['desc'];}?></p>
								<a href="<?php //echo base_url();?>blog/desc?id=<?php //echo $blogs[1]['id'];?>">read more <span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span></a>
 								</div> -->
<!-- 							</div> -->
<!-- 							</div> -->
<!-- 							<div class="right_blog_section"> -->
<!-- 							<div class="right_blog_block"> -->
<!-- 								<div class="right_blog_image"> 
								<img src="<?php //echo base_url();?>uploads/blog_image/blog_<?php //echo $blogs[2]['id'];?>.jpg?<?php //echo time();?>" alt="image" />
								</div> -->
<!-- 								<div class="right_blog_content"> 
									<span class="agri_blog_date"><?php //echo date('M d, Y', strtotime($blogs[2]['created_at']))?></span>
									<h3><a href="<?php //echo base_url();?>blog/desc?id=<?php //echo $blogs[2]['id'];?>"><?php //echo $blogs[2]['title']?></a></h3>
 									<div class="blog_user"> -->
<!-- 										<div class="user_name"> 
											<img src="<?php //echo base_url();?>assets/public/images/user.png" alt="image" />
											<a href="javascript:;"><span><?php //echo $blogs[2]['author']?></span></a>
										</div> -->
<!-- 									</div> 
									<p><?php //if(strlen($blogs[2]['desc']) > 200){$pos=strpos($blogs[2]['desc'], ' ', 200);echo substr($blogs[2]['desc'],0,$pos ); }else{echo $blogs[2]['desc'];}?></p>
									<a href="<?php //echo base_url();?>blog/desc?id=<?php //echo $blogs[2]['id'];?>">read more <span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span></a>
 								</div> -->
<!-- 							</div> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 			</div> -->
			
<!-- 		</div> -->
<!-- 	</div> -->
	<!--Team-->
	<!-- <div class="agri_team_wrapper clv_section">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-lg-3">
					<div class="agri_team_content">
						<h3>Our team</h3>
						<span><img data-src="<?php echo base_url();?>assets/public/images/agri_underline3.png" alt="image" /></span>
						<p>We are passionate with a combination of young and experienced team intends to works hard to improve upon every single day for the purpose we originate.</p>
					</div>
				</div>
				<div class="col-md-9 col-lg-9">
					<div class="agri_team_section">
						<div class="row ">
							<!-- <div class="col-md-4">
								<div class="agri_team_block">
									<div class="agri_team_image">
										<img data-src="<?php echo base_url();?>assets/img/team/team1.jpeg" width="250" alt="image" />
									</div>
									<div class="agri_team_overlay">
										<h4>Dr. NBT Raju</h4>
										<span>MSc(Agr), Phd In Entomology / Chairman Rtd Professor From Dr.NG Ranga Agriculture University, Hyderabad Served 35 Years Covering The Areas Of Teaching And Research.</span>
										<p>you can join us</p>
										<ul>
											<li><a href="https://www.facebook.com/Limpex-Global-100270095077286/?modal=admin_todo_tour"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a></li>
											<li><a href="https://twitter.com/home?lang=en"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a></li>
										</ul>
									</div>
								</div>
							</div>  -->
							<!-- <div class="col-md-4">
								<div class="agri_team_block">
									<div class="agri_team_image">
										<img data-src="<?php echo base_url();?>assets/img/team/team2.jpeg"  width="250"  alt="image" />
									</div>
									<div class="agri_team_overlay">
										<h4>D.Ravi Teja Varma</h4>
										<span>Executive Director B.Com, MBA Finance</span>
										<p>you can join us</p>
										<ul>
											<li><a href="https://www.facebook.com/Limpex-Global-100270095077286/?modal=admin_todo_tour"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a></li>
											<li><a href="https://twitter.com/home?lang=en"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> -->
	<!-- end Mixpanel -->
	<script type="text/javascript">
    	mixpanel.track("<?php echo $title;?>");
	</script>
<!-- end Mixpanel -->