<!--Breadcrumb-->
    <div class="breadcrumb_wrapper">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-4">
                    <div class="breadcrumb_inner">
                        <h3>contact us</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="breadcrumb_block">
            <ul>
                <li><a href="index.html">home</a></li>
                <li>contact us</li>
            </ul>
        </div>
    </div>
    <!--Contact Block-->
    <div class="contact_blocks_wrapper clv_section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <div class="contact_block">
                        <span></span>
                        <div class="contact_icon"><img data-src="<?php echo base_url()?>assets/img/icons/Contact.png" alt="image" style="height: 100px;"/></div>
                        <h4>contact us</h4>
                        <p>9422839891</p>
                        <p>9133057064</p>
                        <p>( +91 ) 4048-55-0189</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="contact_block">
                        <span></span>
                        <div class="contact_icon"><img data-src="<?php echo base_url()?>assets/img/icons/Email.png" alt="image" style="height: 100px;" /></div>
                        <h4>email</h4>
                        <p>admin@limpex.in</p>
                        <p>limpexglobal@gmail.com</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="contact_block">
                        <span></span>
                        <div class="contact_icon"><img data-src="<?php echo base_url()?>assets/img/icons/Address.png" alt="image" style="height: 100px;"/></div>
                        <h4>address</h4>
                        <p>Nagaratna Residency, </p>
                        <p>Aruna cooperative society,</p>
                        <p>Bhagyanagar Colony, Kukatpally,</p>
                        <p>Hyderabad Telangana 500072.</p>
                </div>
            </div>
        </div>
    </div>
    <!--Contact Form-->
    <div class="contact_form_wrapper clv_section">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8">
                    <div class="contact_form_section">
                        <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <h3>Contact Us</h3>
                                <?php if(!empty($this->session->flashdata('success'))){?>
                                    <div class="alert alert-success" id="success-alert">
                                      <button type="button" class="close" data-dismiss="alert">x</button>
                                      <strong>Success! </strong> Details submitted. Our team will get back to you.!
                                    </div>
                                <?php }elseif (!empty($this->session->flashdata('error'))){?>
                                	<div class="alert alert-danger" id="success-alert">
                                      <button type="button" class="close" data-dismiss="alert">x</button>
                                      <strong>Failed! </strong> Please try again....
                                    </div>
                                <?php }?>
                            </div>
							<form action="<?php echo base_url('contact');?>" method="post" enctype="multipart/form-data">
                            <div class="col-md-6 col-lg-6">
                                <div class="form_block">
                                    <input type="text" name="first_name" class="form_field" placeholder="First Name" value="<?php echo set_value('first_name')?>" >
                                    <?php echo form_error('first_name','<div style="color:red">','</div>');?>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="form_block">
                                    <input type="text" name="last_name" class="form_field " placeholder="Last Name" value="<?php echo set_value('last_name')?>"  >
                                    <?php echo form_error('last_name','<div style="color:red">','</div>');?>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="form_block">
                                    <input type="text" name="email" class="form_field " placeholder="Email" data-valid="email" value="<?php echo set_value('email')?>"  data-error="Email should be valid." >
                                    <?php echo form_error('email','<div style="color:red">','</div>');?>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="form_block">
                                    <input type="text" name="subject" class="form_field " placeholder="Subject" value="<?php echo set_value('subject')?>"  >
                                    <?php echo form_error('subject','<div style="color:red">','</div>');?>
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-12">
                                <div class="form_block">
                                    <textarea placeholder="Message" name="message" class="form_field " ><?php echo set_value('message')?> </textarea>
                                    <?php echo form_error('message','<div style="color:red">','</div>');?>
									<div class="response"></div>
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-12">
                                <div class="form_block">
                                   <button type="submit" class="clv_btn submitForm" data-type="contact" value="apply">send</button> 
                                </div>
                            </div>
							</form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="working_time_section">
                        <div class="timetable_block">
                            <h5>working hours</h5>
                            <ul>
                                <li>
                                    <p>monday</p>
                                    <p>9:30 am - 6:00 pm</p>
                                </li>
                                <li>
                                    <p>tuesday</p>
                                    <p>9:00 am - 6:00 pm</p>
                                </li>
                                <li>
                                    <p>wednesday</p>
                                    <p>9:45 am - 6:00 pm</p>
                                </li>
                                <li>
                                    <p>thursday</p>
                                    <p>10:30 am - 6:00 pm</p>
                                </li>
                                <li>
                                    <p>friday</p>
                                    <p>9:30 am - 6:00 pm</p>
                                </li>
                                <li>
                                    <p>saturday</p>
                                    <p>9:30 am - 6:00 pm</p>
                                </li>
                                <li>
                                    <p>sunday</p>
                                    <p>close</p>
                                </li>
                            </ul>
                        </div>
                        <div class="tollfree_block">
                            <h5>24*7 Available!</h5>
                            <h3>( +91 ) 4048-55-0189</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Contact Map-->
<!--     <div class="contact_map_wrapper"> -->
<!--         <div id="map"></div> -->
<!--     </div> -->
<!-- end Mixpanel -->
	<script type="text/javascript">
    	mixpanel.track("<?php echo $title;?>");
	</script>
<!-- end Mixpanel -->