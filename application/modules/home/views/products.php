<!--Breadcrumb-->
    <div class="breadcrumb_wrapper">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-4">
                    <div class="breadcrumb_inner">
                        <h3><?php echo $categories[array_search($_GET['cat_id'], array_column($categories, 'id'))]['name'];?></h3>
                    </div>
                </div>
            </div> 
        </div>
        <div class="breadcrumb_block">
            <ul>
                <li><a href="<?php echo base_url();?>">home</a></li>
                <li><?php echo $categories[array_search($_GET['cat_id'], array_column($categories, 'id'))]['name'];?></li>
            </ul>
        </div>
    </div>
    <!--Produst List-->
    <div class="products_wrapper clv_section">
        <div class="container-fluid">
        	<div class="row mb-3 top-cat-block">
        		<div class="col-12">
        			<?php if(! empty($sub_categories)){?>
                        <div class="sidebar_heading row">
                            <h3><?php echo $categories[array_search($_GET['cat_id'], array_column($categories, 'id'))]['name'];?> CATEGORIES</h3>
                            <img data-src="<?php echo base_url();?>assets/public/images/footer_underline.png" alt="image">
                        </div>
                        <div class="product_category row">
                        	<?php foreach ($sub_categories as $key =>$sub_cat):?>
                        		<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                        			<div class="card">
                            			<div class="card-body">
                            				<div class="sub-cat-image-block">
                            					<img data-src="<?php echo base_url()?>uploads/ecom_sub_category_image/ecom_sub_category_<?php echo $sub_cat['id'];?>.jpg" class="responsive" style="max-width: 100%; height: 180px">
                            				</div>
                            			</div>
                            			<div class="card-footer">
                            				<a class=" btn btn-info products_sidebar_link <?php echo ($sub_cat['id'] == $_GET['sub_cat_id'] || $key == $_GET['sub_cat_id'])? "active" : "";?>" href="<?php echo base_url()?>product/list?cat_id=<?php echo $_GET['cat_id'];?>&sub_cat_id=<?php echo $sub_cat['id'];?>" for="cat1"><?php echo $sub_cat['name']?><span>(<?php echo $sub_cat['count']?>)</span></a>
                            			</div>
                            		</div>
                        		</div>
                        	<?php endforeach;?>
                        </div>
                <?php }?>
        		</div>
        	</div>
            <div class="row">
                <div class="<?php echo (empty($sub_categories))? "col-lg-12 col-md-12" : "col-lg-12 col-md-12";?>">
                    <div class="product_list_section">
                    	<div class="card-header" style="text-align: center">
                    		<h1><?php echo (array_search($_GET['sub_cat_id'], array_column($sub_categories, 'id')) !== FALSE)? $sub_categories[array_search($_GET['sub_cat_id'], array_column($sub_categories, 'id'))]['name']: '';?></h1>
                    	</div>
                    	<div class="card-body">
                    		<div class="product_items_section">
                                <ul>
                                <?php if(! empty($products)){foreach ($products as $product):?>
                                    <li>
                                        <div class="product_item_block">
                                            <div class="org_product_block">
                                                <!-- <span class="product_label">0% off</span> -->
                                                <?php $images = $this->db->where('product_id', $product['id'])->get('product_images')->result_array(); ?>
                                                <div class="org_product_image">
                                                    <img data-src="<?php echo base_url();?>uploads/ecom_product_image/ecom_product_<?php echo (empty($images[0]['id']))? 0: $images[0]['id'] ?>.jpg" alt="image">
                                                </div>
                                                <h4><?php echo $product['name']?></h4>
    <!--                                             <h3><span><i class="fa fa-usd" aria-hidden="true"></i></span>25</h3> -->
                                                	<a href="<?php echo base_url()?>/product/desc?id=<?php echo $product['id']?>&product_name=<?php echo $product['name']?>">Read more</a>
                                            </div>
                                        </div>
                                    </li>
                               <?php endforeach;}else{echo "<h1 style='text-align:center'>No Product for ".$categories[array_search($_GET['cat_id'], array_column($categories, 'id'))]['name']." </h1>";}?>
                                </ul>
                            </div>
                    	</div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
<!-- end Mixpanel -->
	<script type="text/javascript">
    	mixpanel.track("<?php echo $title.'-'.$categories[array_search($_GET['cat_id'], array_column($categories, 'id'))]['name'];?>");
	</script>
<!-- end Mixpanel -->