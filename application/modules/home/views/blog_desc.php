        <!--Breadcrumb-->
		<div class="breadcrumb_wrapper">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-md-4">
                        <div class="breadcrumb_inner">
                            <h3>blog single</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="breadcrumb_block">
                <ul>
                    <li><a href="index.html">home</a></li>
                    <li>blog single</li>
                </ul>
            </div>
        </div>
        <!--Blog With Sidebar-->
        <div class="blog_sidebar_wrapper clv_section blog_single_wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="blog_left_section">
                            <div class="blog_section">
                                <div class="agri_blog_image">
                                    <!-- <img data-src="<?php echo base_url()?>assets/public/images/blog_main_big1.jpg" alt="image"> -->
                                    <img data-src="<?php echo base_url();?>uploads/blog_image/blog_<?php echo $blogs_desc['id'];?>.jpg" alt="image">
                                    <span class="agri_blog_date"><?php echo date('M d, Y', strtotime($blogs_desc['created_at']))?></span>
                                </div> <input type="hidden" name="id" value="<?php echo $blogs_desc['id'] ; ?>">
                                <div class="agri_blog_content">
                                    <h3><a href="blog_single.html" value="<?php echo $blogs_desc['title'];?>"><?php echo $blogs_desc['title'];?></a></h3>
                                    <div class="blog_user">
                                        <div class="user_name">
                                            <img data-src="<?php echo base_url()?>assets/public/images/user.png" alt="image">
                                            <a href="javascript:;"><span><?php echo $blogs_desc['author'];?></span></a>
                                        </div>
                                        <div class="comment_block">
                                            <span><i class="fa fa-comments-o" aria-hidden="true"></i></span>
                                            <a href="javascript:;">26 comments</a>
                                        </div>
                                    </div>
                                    <p><?php echo $blogs_desc['desc'];?></p>
                                    
                                    
                                    
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- end Mixpanel -->
	<script type="text/javascript">
    	mixpanel.track("<?php echo $blogs_desc['title'];?>");
	</script>
<!-- end Mixpanel -->