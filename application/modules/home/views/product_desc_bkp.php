<!--Breadcrumb-->
    <div class="breadcrumb_wrapper">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-4">
                    <div class="breadcrumb_inner">
                        <h3><?php echo $product['name']?></h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="breadcrumb_block">
            <ul>
                <li><a href="<?php echo base_url()?>">home</a></li>
                <li><?php echo $product['name']?></li>
            </ul>
        </div>
    </div>
    <!--Product Single-->
    <div class="product_single_wrapper clv_section">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-7">
                    <div class="product_single_slider">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="fd_pro_thumnail">
                                    <div class="swiper-container gallery-thumbs">
                                        <div class="swiper-wrapper">
                                        	<?php foreach ($images as $image):?>	
                                                <div class="swiper-slide">
                                                    <div class="fd_pro_img_thumnail">
                                                        <img src="<?php echo base_url();?>uploads/ecom_product_image/ecom_product_<?php echo $image['id']?>.jpg?<?php echo time();?>" alt="image">
                                                    </div>
                                                </div>
                                            <?php endforeach;?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9">
                                <div class="fd_product_img">
                                    <div class="swiper-container gallery-top">
                                        <div class="swiper-wrapper">
                                        	<?php foreach ($images as $image):?>
                                                <div class="swiper-slide">
                                                    <div class="fd_pro_img">
                                                        <img src="<?php echo base_url();?>uploads/ecom_product_image/ecom_product_<?php echo $image['id']?>.jpg?<?php echo time();?>" alt="image">
                                                    </div>
                                                </div>
                                            <?php endforeach;?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5">
                    <div class="product_single_details">
                        <div class="product_price_box"> 
                            <h3><?php echo $product['name']?></h3>  
                        </div>          
                        <!-- <p>PowerHouse & Pump</p>  -->
                        <!-- <div class="rating_section">
                            <span>4.1</span>
                            <ul>
                                <li><a class="active" href="javascript:;"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                <li><a class="active" href="javascript:;"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                <li><a class="active" href="javascript:;"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                <li><a href="javascript:;"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                <li><a href="javascript:;"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                            </ul>
                            <p>151 reviews</p>
                        </div> -->
                        <ul class="product_code">
                           <!--  <li>
                                <p>product code: 12948</p>
                            </li> -->
                            <li>
                                <p>availability: <span>in stock</span></p>
                            </li>
                        </ul>
                        <!-- <p>Consectetur adipisicing elit sed do eiusmod tempor incididunt utte labore et dolore magna aliqua Ut enim ad minim veniam quis strud exercitation ullamco laboris nisi ut aliquip ex ea commodo wis aute irure dolor in reprehenderit in voluptate</p> -->
                        <!-- <div class="product_prices">
                            <h2>$89.12</h2>
                            <h3>$98.12</h3>
                            <span class="product_discount">10% off</span>
                        </div>
                        <div class="product_delivery">
                            <p>
                                <span class="pro_icon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>  
                                <input type="text" placeholder="Enter your Delivery Pincode">
                                <a href="javascript:;" class="pin_check"> check</a>
                            </p>
                            <p class="pro_cod"><span class="pro_iocn"><i class="fa fa-check-circle" aria-hidden="true"></i></span> COD Available</p>
                        </div> -->
                        <div class="fd_pro_quantity">
                            <div class="quantity_wrapper">
                                <div class="input-group">
                                    <span class="quantity_minus"> - </span>
                                    <input type="text" class="quantity" value="2">
                                    <span class="quantity_plus"> + </span>
                                </div>
                            </div>
                        </div>
                        <div class="fd_pro_add_btn">
                            <a href="javascript:;" class="clv_btn">add to cart</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Products Details-->
    <div class="product_detail_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="product_detail_tab">
                        <ul class="nav nav-tabs">
                            <li><a data-toggle="tab" class="active" href="#description">Description</a></li>
                            <li><a data-toggle="tab" href="#info">Additional Information</a></li>
                            <li><a data-toggle="tab" href="#review">Review</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="description" class="tab-pane fade show active">
                                <?php echo $product['desc']?>
                            </div>
                            <div id="info" class="tab-pane fade">
                                <ul class="additional_info">
                                    <li>
                                        <span class="info_head">size -</span>
                                        4 cm x 20 cm
                                    </li>
                                    <li>
                                        <span class="info_head">system rating -</span>
                                        high
                                    </li>
                                    <li>
                                        <span class="info_head">color -</span>
                                        brown
                                    </li>
                                    <li>
                                        <span class="info_head">material -</span>
                                       pump
                                    </li>
                                    <li>
                                        <span class="info_head">model number -</span>
                                        GT 15014G
                                    </li>
                                    <li>
                                        <span class="info_head">generic name -</span>
                                       pump
                                    </li>
                                </ul>
                            </div>
                            <div id="review" class="tab-pane fade">
                                <div class="fd_review_wrapper">
                                    <h3 class="review_heading">there are no reviews yet.</h3>
                                    <h5>be the first to review "this product"</h5>
                                    <div class="row">
                                        <div class="col-lg-9">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form_block">
                                                        <input type="text" class="form_field" placeholder="First Name">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form_block">
                                                        <input type="text" class="form_field" placeholder="Email">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form_block">
                                                        <textarea placeholder="Your Review" class="form_field"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <button class="clv_btn">send</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>