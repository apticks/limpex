<!--Breadcrumb-->
<div class="breadcrumb_wrapper">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-md-4">
				<div class="breadcrumb_inner">
					<h3>our gallery</h3>
				</div>
			</div>
		</div>
	</div>
	<div class="breadcrumb_block">
		<ul>
			<li><a href="index.html">home</a></li>
			<li>gallery</li>
		</ul>
	</div>
</div>
<!--Gallery-->
<div class="clv_gallery_wrapper clv_section">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-lg-6 col-md-6">
				<div class="clv_heading">
					<h3>our gallery</h3>
					<div class="clv_underline">
						<img data-src="<?php echo base_url()?>assets/public/images/underline3.png" alt="image" />
					</div>
					<p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut
						labore et dole magna aliqua. Ut enim ad minim veniam quis nostrud
						exercitation.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="gallery_slide">
					<div class="gallery_grid">
						<div class="gallery_grid_item">
							<div class="gallery_image">
								<img data-src="<?php echo base_url()?>assets/public/images/gallery1.jpg" alt="image" />
								<div class="gallery_overlay">
									<a href="<?php echo base_url()?>assets/public/images/gallery1_big.jpg" class="view_image"><span><i
											class="fa fa-search" aria-hidden="true"></i></span></a>
								</div>
							</div>
						</div>
						<div class="gallery_grid_item">
							<div class="gallery_image">
								<img data-src="<?php echo base_url()?>assets/public/images/gallery2.jpg" alt="image" />
								<div class="gallery_overlay">
									<a href="<?php echo base_url()?>assets/public/images/gallery2_big.jpg" class="view_image"><span><i
											class="fa fa-search" aria-hidden="true"></i></span></a>
								</div>
							</div>
						</div>
						<div class="gallery_grid_item">
							<div class="gallery_image">
								<img data-src="<?php echo base_url()?>assets/public/images/gallery3.jpg" alt="image" />
								<div class="gallery_overlay">
									<a href="<?php echo base_url()?>assets/public/images/gallery3_big.jpg" class="view_image"><span><i
											class="fa fa-search" aria-hidden="true"></i></span></a>
								</div>
							</div>
						</div>
						<div class="gallery_grid_item">
							<div class="gallery_image">
								<img data-src="<?php echo base_url()?>assets/public/images/gallery4.jpg" alt="image" />
								<div class="gallery_overlay">
									<a href="<?php echo base_url()?>assets/public/images/gallery4_big.jpg" class="view_image"><span><i
											class="fa fa-search" aria-hidden="true"></i></span></a>
								</div>
							</div>
						</div>
						<div class="gallery_grid_item">
							<div class="gallery_image">
								<img data-src="<?php echo base_url()?>assets/public/images/gallery5.jpg" alt="image" />
								<div class="gallery_overlay">
									<a href="<?php echo base_url()?>assets/public/images/gallery5_big.jpg" class="view_image"><span><i
											class="fa fa-search" aria-hidden="true"></i></span></a>
								</div>
							</div>
						</div>
						<div class="gallery_grid_item">
							<div class="gallery_image">
								<img data-src="<?php echo base_url()?>assets/public/images/gallery7.jpg" alt="image" />
								<div class="gallery_overlay">
									<a href="<?php echo base_url()?>assets/public/images/gallery7_big.jpg" class="view_image"><span><i
											class="fa fa-search" aria-hidden="true"></i></span></a>
								</div>
							</div>
						</div>
						<div class="gallery_grid_item">
							<div class="gallery_image">
								<img data-src="<?php echo base_url()?>assets/public/images/gallery6.jpg" alt="image" />
								<div class="gallery_overlay">
									<a href="<?php echo base_url()?>assets/public/images/gallery6_big.jpg" class="view_image"><span><i
											class="fa fa-search" aria-hidden="true"></i></span></a>
								</div>
							</div>
						</div>
						<div class="gallery_grid_item">
							<div class="gallery_image">
								<img data-src="<?php echo base_url()?>assets/public/images/gallery8.jpg" alt="image" />
								<div class="gallery_overlay">
									<a href="<?php echo base_url()?>assets/public/images/gallery8_big.jpg" class="view_image"><span><i
											class="fa fa-search" aria-hidden="true"></i></span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="load_more_btn">
					<a href="javascript:;" class="clv_btn">view more</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end Mixpanel -->
	<script type="text/javascript">
    	mixpanel.track("<?php echo $title;?>");
	</script>
<!-- end Mixpanel -->