<!--Breadcrumb-->
    <div class="breadcrumb_wrapper">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-4">
                    <div class="breadcrumb_inner">
                        <h3><?php echo $product['name']?></h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="breadcrumb_block">
            <ul>
                <li><a href="<?php echo base_url()?>">home</a></li>
                <li><a href="<?php echo base_url()?>product/list?cat_id=<?php echo $product['cat_id']?>&sub_cat_id=<?php echo $product['sub_cat_id']?>"><?php echo $categories[array_search($product['cat_id'], array_column($categories, 'id'))]['name'];?></a></li>
                <li><?php echo $product['name']?></li>
            </ul>
        </div>
    </div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-6 col-lg-6 col-xs-12 mt-3 mb-3 product-image-part">
			<section id="default" class="padding-top0">
                <div class="row">
                  <div class="large-5 column">
                    <div class="xzoom-container">
                    	<?php if(! empty($images)):?>
                          <img class="xzoom" id="xzoom-default" data-src="<?php echo base_url();?>uploads/ecom_product_image/ecom_product_<?php echo $images[0]['id']?>.jpg" xoriginal="<?php echo base_url();?>uploads/ecom_product_image/ecom_product_<?php echo $images[0]['id']?>.jpg" />
                          <div class="xzoom-thumbs">
                              <?php foreach ($images as $image):?>	
                                <a href="<?php echo base_url();?>uploads/ecom_product_image/ecom_product_<?php echo $image['id']?>.jpg"><img class="xzoom-gallery" width="80" src="<?php echo base_url();?>uploads/ecom_product_image/ecom_product_<?php echo $image['id']?>.jpg"  xpreview="<?php echo base_url();?>uploads/ecom_product_image/ecom_product_<?php echo $image['id']?>.jpg" title="<?php echo $product['name'];?>"></a>
                              <?php endforeach;?>
                          </div>
                      <?php endif;?>
                    </div>        
                  </div>
                  <div class="large-7 column"></div>
                </div>
            </section>
		</div>
		<div class="col-md-6 col-lg-6 col-xs-12  mt-3 mb-3 product-title-part">
			<div class="product-title">
				<h1><?php echo $product['name']?></h1>
				<hr/>
			</div>
			<div class="product-action-status">
				<p>Availability: <span>In Stock</span></p>
                <a class="login-trigger" href="#" data-target="#login" data-toggle="modal">Enquiry</a>
				<!-- <button class="btn btn-success btn-lg">Enquiry</button> -->
			</div>
			<?php if(! empty($blog)){?>
<!-- 			<div class="clv_partner_wrapper clv_section mt-3"> -->
<!--         		<div class="container"> -->
<!--         			<div class="row"> -->
<!--         				<h5>Related Posts</h5> -->
<!--         				<hr> -->
<!--         				<div class="col-12"> -->
<!--         					<div class="product_blog_slider"> -->
<!--         						<div class="product_blog_swiper-container"> -->
<!--         							<div class="swiper-wrapper"> -->
        								<?php foreach ($blog as $b){?>
<!--         								<div class="swiper-slide"> -->
<!--         									<div class="partner_slide"> 
        										<a href="<?php echo (! empty($b['url']))? $b['url']: base_url().'blog/desc?id= '.$b['id']?>">
            										<div class="product_blog_image"> 
            											<img alt="<?php echo $product['name'].'-'.$b['title']?>" src="<?php echo base_url()?>uploads/blog_image/blog_<?php echo $b['id'].'.jpg?'.time();?>">
             										</div> 
            										<h5><?php echo $b['title']?></h5>
         										</a> -->
<!--         									</div> -->
<!--         								</div> -->
        								<?php }?>
<!--         							</div> -->
<!--         						</div> -->
        						<!-- Add Arrows -->
<!--         						<span class="slider_arrow product_blog_left left_arrow"> -->
<!--         							<svg  -->
<!--         							 xmlns="http://www.w3.org/2000/svg" -->
<!--         							 xmlns:xlink="http://www.w3.org/1999/xlink" -->
<!--         							 width="10px" height="15px"> -->
<!--         							<path fill-rule="evenodd"  fill="rgb(226, 226, 226)" -->
<!--         							 d="M0.324,8.222 L7.117,14.685 C7.549,15.097 8.249,15.097 8.681,14.685 C9.113,14.273 9.113,13.608 8.681,13.197 L2.670,7.478 L8.681,1.760 C9.113,1.348 9.113,0.682 8.681,0.270 C8.249,-0.139 7.548,-0.139 7.116,0.270 L0.323,6.735 C0.107,6.940 -0.000,7.209 -0.000,7.478 C-0.000,7.747 0.108,8.017 0.324,8.222 Z"/> -->
<!--         							</svg> -->
<!--         						</span> -->
<!--         						<span class="slider_arrow product_blog_right right_arrow"> -->
<!--         							<svg  -->
<!--         							 xmlns="http://www.w3.org/2000/svg" -->
<!--         							 xmlns:xlink="http://www.w3.org/1999/xlink" -->
<!--         							 width="19px" height="25px"> -->
<!--         							<path fill-rule="evenodd" fill="rgb(226, 226, 226)" -->
<!--         							 d="M13.676,13.222 L6.883,19.685 C6.451,20.097 5.751,20.097 5.319,19.685 C4.887,19.273 4.887,18.608 5.319,18.197 L11.329,12.478 L5.319,6.760 C4.887,6.348 4.887,5.682 5.319,5.270 C5.751,4.861 6.451,4.861 6.884,5.270 L13.676,11.735 C13.892,11.940 14.000,12.209 14.000,12.478 C14.000,12.747 13.892,13.017 13.676,13.222 Z"/> -->
<!--         							</svg> -->
<!--         						</span> -->
<!--         					</div>  -->
<!--         				</div> -->
<!--         			</div> -->
<!-- 				</div> -->
<!-- 				</div> -->
				<?php }?>
           <!--  <div class="contact-container">
                  <ul class="actions">
                    <li><a href="#" id="contact" class="button big">Enquiry</a></li>
                  </ul>
            </div> -->
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<!-- Tabs -->
            <section id="tabs">
            	<div class="container">
            		<h6 class="section-title h1"><?php echo $product['name'];?></h6>
            		<div class="row">
            			<div class="col-12 ">
            				<nav>
            					<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
            						<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Description</a>
            						<a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab" href="#nav-home1" role="tab" aria-controls="nav-home" aria-selected="true">Varieties</a>
                          <?php if($product['cat_id']!= 1):?>
            						<a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab" href="#nav-home2" role="tab" aria-controls="nav-home" aria-selected="true">Season</a>
                        <?php endif;?>
            						<a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab" href="#nav-home3" role="tab" aria-controls="nav-home" aria-selected="true">Management</a>
            						<a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab" href="#nav-home4" role="tab" aria-controls="nav-home" aria-selected="true">Irrigation/Cultivation</a>
            						<a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab" href="#nav-home5" role="tab" aria-controls="nav-home" aria-selected="true">Nutrition Values</a>
            					</div>
            				</nav>
            				<div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
            					<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
            						<div class="tab-content-product_des">
            							<?php echo $product['desc']?>
            						</div>
            					</div>
            					<div class="tab-pane fade" id="nav-home1" role="tabpanel" aria-labelledby="nav-home-tab">
            					<div class="tab-content-product_des">
            							<?php echo $product['varieties_desc']?>
            						</div>
            					</div>

            					<div class="tab-pane fade" id="nav-home2" role="tabpanel" aria-labelledby="nav-home-tab">
                       
            					<div class="tab-content-product_des">
                        
            							<?php echo $product['season_desc']?>
                          
            						</div>

            					</div>
                       
            					<div class="tab-pane fade" id="nav-home3" role="tabpanel" aria-labelledby="nav-home-tab">
            					<div class="tab-content-product_des">
            							<?php echo $product['management_desc']?>
            						</div>
            					</div>
            					<div class="tab-pane fade" id="nav-home4" role="tabpanel" aria-labelledby="nav-home-tab">
            						<div class="tab-content-product_des">
            							<?php echo $product['cultivation_desc']?>
            						</div>
            					</div>
            					<div class="tab-pane fade" id="nav-home5" role="tabpanel" aria-labelledby="nav-home-tab">
            						<div id="nutrition-img-block">
            							<img alt="nutrition-<?php echo $product['name']?>" src="<?php echo base_url();?>uploads/product_nutrition_image/product_nutrition_<?php echo $product['id'];?>.jpg">
                          
            						</div>
            					</div>
            				</div>
            			
            			</div>
            		</div>
            	</div>
            </section>
            <!-- ./Tabs -->
		</div>
	</div>
</div>
<div id="login" class="modal fade" role="dialog">
  <div class="modal-dialog">
    
    <div class="modal-content">
      <div class="modal-body">
        <button data-dismiss="modal" class="close">&times;</button>
        <h4>Enquiry Form</h4>
        <form action="<?php echo base_url('enquiry');?>" method="post" enctype="multipart/form-data">
             <input type="hidden" name="id" value="<?php echo $product['id'] ; ?>">
             
            <input type="text" name="1" disabled class="username form-control" placeholder="Product Name"  value="<?php echo $product['name'] ; ?>"  />
            
          <input type="text" name="name" class="username form-control" placeholder="Customer Name"/>
          
           <input type="text" name="qty" class="username form-control" placeholder="Quantity"/>
          
          <input type="email" name="email" class="username form-control" placeholder="Email"/>
          
          <input type="text" name="contact" class="username form-control" placeholder="Contact Number"/>
         
          <input type="text" name="address" class="username form-control" placeholder="Address"/>
          
          <input class="btn login" type="submit" value="Submit" />
        </form>
      </div>
    </div>
  </div>  
</div>
<style type="text/css">body{
  height: 100vh;
/*   text-align: center; */
}
  /*Trigger Button*/
.login-trigger {
  font-weight: bold;
  color: #fff;
  background: linear-gradient(to bottom right, #008c5c, #ffc025);
  padding: 15px 30px;
  border-radius: 30px;
  position: relative; 
  top: 50%;
}

/*Modal*/
h4 {
  font-weight: bold;
  color: green;
}
.close {
  color: #000000;
  /* transform: scale(1.2) */
}
.modal-content {
  font-weight: bold;
  /* background: linear-gradient(to bottom right,#008c5c,#ffc025); */
}
.form-control {
  margin: 1em 0;
}
.form-control:hover, .form-control:focus {
  box-shadow: none;  
  border-color: #fff;
  /* background: linear-gradient(to bottom right,#008c5c,#ffc025); */
}
.username, .password {
  border: none;
  border-radius: 0;
  box-shadow: none;
  border-bottom: 2px solid #eee;
  padding-left: 0;
  font-weight: normal;
  background: transparent;  
}
.form-control::-webkit-input-placeholder {
  /*color: transparent;  */
}
.form-control:focus::-webkit-input-placeholder {
  font-weight: bold;
  /*color: transparent;*/
}
.login {
  padding: 6px 20px;
  border-radius: 20px;
  background: none;
  border: 2px solid #FAB87F;
  color: #FAB87F;
  font-weight: bold;
  transition: all .5s;
  margin-top: 1em;
}
.login:hover {
  background: #FAB87F;
  color: #fff;
}</style>

<!-- end Mixpanel -->
	<script type="text/javascript">
    	mixpanel.track("<?php echo $title.'-'.$product['name'];?>");
	</script>
<!-- end Mixpanel -->