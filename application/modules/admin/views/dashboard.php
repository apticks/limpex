<div class="row">
	<div class="col-xl-3  col-md-4 col-sm-4">
		<a href="<?php echo base_url('ecom_product/r');?>">
			<div class="card">
				<div class="card-bg">
					<div class="p-t-20 d-flex justify-content-between">
						<div class="col">
							<h6 class="mb-0">Products</h6>
						</div>
						<!-- <i class="fas fa-address-card card-icon col-orange font-30 p-r-30"></i> -->
					</div>
					<!-- <canvas id="cardChart1" height="80"></canvas> -->
					<br>
					<div class="alert alert-sm alert-primary ">
						<center>
							<i class="fas fa-cubes card-icon font-20 p-r-30"><?php echo count($this->db->get_where('products', ['status' => 1])->result_array()); ?></i>
						</center>
					</div>
				</div>
			</div>
		</a>
	</div>
	<a href="<?php echo base_url('blog_admin/r');?>"> 
	<div class="col-xl-3  col-md-4 col-sm-4">
			<div class="card">
				<div class="card-bg">
					<div class="p-t-20 d-flex justify-content-between">
						<div class="col">
							<h6 class="mb-0">Blog Posts</h6>
							<span class="font-weight-bold mb-0 font-20"></span>
						</div>
					</div>
					<!-- <canvas id="cardChart4" height="80"></canvas> -->
					<br>
					<div class="alert alert-sm alert-primary ">
						<center>
							<i class="fas fa-newspaper card-icon font-20 p-r-30">
								<?php echo count($this->db->get_where('blog', ['status' => 1])->result_array()); ?></i>
						</center>
					</div>
				</div>
			</div>
		</a>
	</div>
	<div class="col-xl-3  col-md-4 col-sm-4">
		<a href="<?php echo base_url('subscribers');?>">
			<div class="card">
				<div class="card-bg">
					<div class="p-t-20 d-flex justify-content-between">
						<div class="col">
							<h6 class="mb-0">Subscribers</h6>
							<span class="font-weight-bold mb-0 font-20"></span>
						</div>
					</div>
					<!-- <canvas id="cardChart4" height="80"></canvas> -->
					<br>
					<div class="alert alert-sm alert-primary ">
						<center>
							<i class="fas fa-rss card-icon font-20 p-r-30"> <?php echo count($this->db->get_where('subscribe_details', ['status' => 1])->result_array()); ?></i>
						</center>
					</div>
				</div>
			</div>
		</a>
	</div>
	<div class="col-xl-12  col-md-12 col-sm-12">
		<a href="<?php echo base_url('subscribers');?>">
			<div class="card">
				<div class="card-bg">
					<div class="p-t-20 d-flex justify-content-between">
						<div class="col">
							<h6 class="mb-0">Visitors</h6>
							<span class="font-weight-bold mb-0 font-20"></span>
						</div>
					</div>
					<!-- <canvas id="cardChart4" height="80"></canvas> -->
					<br>
					<div class="alert alert-sm alert-primary ">
						<center>
							<img src="https://www.freevisitorcounters.com/en/counter/render/712935/t/0" border="0" class="counterimg">
						</center>
					</div>
				</div>
			</div>
		</a>
	</div>
</div>