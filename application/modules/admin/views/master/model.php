<!--Add Category And its list-->
<div class="row">
	<div class="col-12">
		<h4>Add Model</h4>
		<form class="needs-validation" novalidate="" action="<?php echo base_url('model/c');?>" method="post" enctype="multipart/form-data">
			<div class="card-header">
				<div class="form-row">
					<div class="form-group col-md-4">
						<label>Model Name</label> <input type="text" name="name"
							required="" value="<?php echo set_value('name')?>"
							class="form-control" placeholder="Model Name">
						<div class="invalid-feedback">Please Provide Model Number.!</div>
						<?php echo form_error('name', '<div style="color:red">', '</div>');?>
					</div>
					<div class="form-group col-md-4">
						<label>Brand</label>
						<!-- <input type="file" class="form-control" required="">-->
						<select class="form-control" name="brand_id" required="" >
								<option value="0" selected disabled>--select--</option>
    							<?php foreach ($brands as $brand):?>
    								<option value="<?php echo $brand['id'];?>"><?php echo $brand['name']?></option>
    							<?php endforeach;?>
						</select>
						<div class="invalid-feedback">Please select Brand.!</div>
						<?php echo form_error('cat_id','<div style="color:red>"','</div>');?>
					</div>
					<div class="form-group col-md-2">
						<button type="submit" name="upload" id="upload" value="Apply"
							class="btn btn-primary mt-27 ">Submit</button>
					</div>
				</div>
			</div>
		</form>

		<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of Models</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>Model Number</th>
									<th>Brand</th>
									<th>Actions</th>

								</tr>
							</thead>
							<tbody>
							<?php if(!empty($models)):?>
    							<?php  $sno = 1; foreach ($models as $model): ?>
    								<tr>
									<td><?php echo $sno++;?></td>
									<td><?php echo $model['name'];?></td>
									<td><?php foreach ($brands as $brand):?>
    										<?php echo ($brand['id'] == $model['brand_id'])? $brand['name']:'';?>
    									<?php endforeach;?></td>
									<td><a
										href="<?php echo base_url()?>model/edit?id=<?php echo $model['id']; ?>"
										class=" mr-2  " type="ecom_brands"> <i class="fas fa-pencil-alt"></i>
									</a> <a href="#" class="mr-2  text-danger "
										onClick="delete_record(<?php echo $model['id'] ?>, 'model/d')">
											<i class="far fa-trash-alt"></i>
									</a></td>

								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr>
									<th colspan='4'><h3>
											<center>No Models</center>
										</h3></th>
								</tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>
</div>
