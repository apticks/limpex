<!--Add Category And its list-->
<div class="row">
	<div class="col-12">
		<h4>Add Colour</h4>
		<form class="needs-validation" class="color-form" novalidate="" action="<?php echo base_url('colour/c');?>" method="post" enctype="multipart/form-data">
			<div class="card-header">
				<div class="form-row">
					<div class="form-group col-md-6">
						<label>Colour Name</label> <input type="text" name="name"
							required="" value="<?php echo set_value('name')?>"
							class="form-control" placeholder="Colour Name">
						<div class="invalid-feedback">Please provide colour name.!</div>
						<?php echo form_error('name', '<div style="color:red">', '</div>');?>
					</div>
					
					<div class="form-group col-md-6">
						<label>Colour code</label> <input type="text" name="color_code" id="picker"
							required="" value="<?php echo set_value('color_code')?>" placeholder="Colour code"
							class="form-control">
						<div class="invalid-feedback">Please provide colour code.!</div>
						<?php echo form_error('color_code', '<div style="color:red">', '</div>');?>
					</div>
					
					<div class="form-group col-md-2">
						<button type="submit" name="upload" id="upload" value="Apply"
							class="btn btn-primary mt-27 ">Submit</button>
					</div>
				</div>
			</div>
		</form>

		<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of Colours</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>Colour Name</th>
									<th>Colour Code</th>
									<th>Actions</th>

								</tr>
							</thead>
							<tbody>
							<?php if(!empty($colours)):?>
    							<?php  $sno = 1; foreach ($colours as $colour): ?>
    								<tr>
									<td><?php echo $sno++;?></td>
									<td><?php echo $colour['name'];?></td>
									<td><?php echo $colour['color_code'];?></td>
									<td><a
										href="<?php echo base_url()?>colour/edit?id=<?php echo $colour['id']; ?>"
										class=" mr-2  " type="ecom_brands"> <i class="fas fa-pencil-alt"></i>
									</a> <a href="#" class="mr-2  text-danger "
										onClick="delete_record(<?php echo $colour['id'] ?>, 'colour/d')">
											<i class="far fa-trash-alt"></i>
									</a></td>

								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr>
									<th colspan='4'><h3>
											<center>No Colours</center>
										</h3></th>
								</tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>
</div>
