<!--Add Category And its list-->
<div class="row">
	<div class="col-12">
		<h4>Add Size</h4>
		<form class="needs-validation" novalidate="" action="<?php echo base_url('size/c');?>" method="post" enctype="multipart/form-data">
			<div class="card-header">
				<div class="form-row">
					<div class="form-group col-md-4">
						<label>Size</label> <input type="text" name="size"
							required="" value="<?php echo set_value('size')?>"
							class="form-control" placeholder="Size">
						<div class="invalid-feedback">Please provide size.!</div>
						<?php echo form_error('size', '<div style="color:red">', '</div>');?>
					</div>
					
					<div class="form-group col-md-2">
						<button type="submit" name="upload" id="upload" value="Apply"
							class="btn btn-primary mt-27 ">Submit</button>
					</div>
				</div>
			</div>
		</form>

		<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of sizes</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>Size</th>
									<th>Actions</th>

								</tr>
							</thead>
							<tbody>
							<?php if(!empty($sizes)):?>
    							<?php  $sno = 1; foreach ($sizes as $size): ?>
    								<tr>
									<td><?php echo $sno++;?></td>
									<td><?php echo $size['size'];?></td>
									<td><a
										href="<?php echo base_url()?>size/edit?id=<?php echo $size['id']; ?>"
										class=" mr-2  " type="ecom_brands"> <i class="fas fa-pencil-alt"></i>
									</a> <a href="#" class="mr-2  text-danger "
										onClick="delete_record(<?php echo $size['id'] ?>, 'size/d')">
											<i class="far fa-trash-alt"></i>
									</a></td>

								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr>
									<th colspan='3'><h3>
											<center>No Sizes</center>
										</h3></th>
								</tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>
</div>
