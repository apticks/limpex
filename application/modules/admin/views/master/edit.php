
  <?php if ($type == 'state'){ ?>

                <!--Edit State -->
                <div class="row">
                    <div class="col-12">
                        <h4>Edit State</h4>
                        <form class="needs-validation" novalidate="" action="<?php echo base_url('state/u');?> " method="post" enctype="multipart/form-data">
                            <div class="card-header">
                                <div class="form-row">
                                    <div class="form-group col-md-6">

                                        <label>State Name</label>
                                        <input type="text" name="name" class="form-control" required="" value="<?php echo $state['name']; ?>">

                                        <div class="invalid-feedback">Enter Valid State Name?</div>
                                    </div>
                                    <input type="hidden" name="id" value="<?php echo $state['id'] ; ?>">
                                    </br>
                                    <div class="form-group col-md-6">
                                        <button class="btn btn-primary mt-27 ">Update</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <?php }elseif ($type == 'district'){?>

                    <!--Edit District-->
                    <div class="row">
                        <div class="col-12">
                            <h4>Edit District</h4>
                            <form class="needs-validation" novalidate="" action="<?php echo base_url('district/u');?>" method="post" enctype="multipart/form-data">
                                <div class="card-header">
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label>District Name</label>
                                            <input type="text" name="name" class="form-control" required="" value="<?php echo $district['name'];?>">
                                            <div class="invalid-feedback">Enter Valid District Name?</div>
                                        </div>
                                        <input type="hidden" name="id" value="<?php echo $district['id'] ; ?>">
                                        </br>
                                        <div class="form-group col-md-4">
                                            <label>State</label>

                                            <!-- 						<select class="form-control" name="state_id" required=""> -->
                                            <!-- 								<option value="">state1</option> -->
                                            <!-- 								<option value="" >state1</option> -->
                                            <!-- 								<option value="" selected >state1</option> -->
                                            <!-- 								<option value=""  >state1</option> -->

                                            <!-- 						</select> -->
                                            <select class="form-control" id='state' onchange="state_changed()" name="state_id" required="">
                                                <option value="0" selected disabled>--select--</option>
                                                <?php foreach ($states as $state):?>
                                                    <option value="<?php echo $state['id'];?>" <?php echo ($state['id'] == $district['state_id'])? 'selected': '';?>><?php echo $state['name']?></option>
														 <?php echo $state['name']?>
                                                    </option>
                                                    <?php endforeach;?>
                                            </select>
                                            <div class="invalid-feedback">Belongs to the state?</div>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <button class="btn btn-primary mt-27 ">Update</button>
                                        </div>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>

                    <?php }elseif ($type == 'model'){?>
                        <div class="row">
							<div class="col-12">
								<h4>Edit Model</h4>
								<form class="needs-validation" novalidate="" action="<?php echo base_url('model/u');?>" method="post" enctype="multipart/form-data">
									<div class="card-header">
										<div class="form-row">
											<div class="form-group col-md-4">
												<label>Model Number</label> <input type="text" name="name"
													required="" value="<?php echo $model['name']?>"
													class="form-control">
												<div class="invalid-feedback">Please Provide Model Number.!</div>
												<?php echo form_error('name', '<div style="color:red">', '</div>');?>
											</div>
											<input type="hidden" name="id" value="<?php echo $model['id'] ; ?>">
											<div class="form-group col-md-4">
												<label>Brand</label>
												<!-- <input type="file" class="form-control" required="">-->
												<select class="form-control" id='state'  name="brand_id" required="">
	                                                <option value="0" selected disabled>--select--</option>
	                                                <?php foreach ($brands as $brand):?>
	                                                    <option value="<?php echo $brand['id'];?>" <?php echo ($brand['id'] == $model['brand_id'])? 'selected': '';?>><?php echo $brand['name']?></option>
	                                                <?php endforeach;?>
	                                            </select>
												<div class="invalid-feedback">Please select Brand.!</div>
												<?php echo form_error('cat_id','<div style="color:red>"','</div>');?>
											</div>
											<div class="form-group col-md-2">
												<button type="submit" name="upload" id="upload" value="Apply"
													class="btn btn-primary mt-27 ">Submit</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					<?php }elseif ($type == 'warrenty'){?>
					<div class="row">
						<div class="col-12">
							<h4>Edit Warrenty</h4>
							<form class="needs-validation" novalidate="" action="<?php echo base_url('warrenty/u');?>" method="post" enctype="multipart/form-data">
								<div class="card-header">
									<div class="form-row">
										<div class="form-group col-md-6">
											<label>Warrenty duration</label> <input type="text" name="duration"
												required="" value="<?php echo $warrenty['time']?>"
												class="form-control">
											<div class="invalid-feedback">Please Provide duration.!</div>
											<?php echo form_error('duration', '<div style="color:red">', '</div>');?>
										</div>
										<input type="hidden" name="id" value="<?php echo $warrenty['id'] ; ?>">
										<div class="form-group col-md-2">
											<button type="submit" name="upload" id="upload" value="Apply"
												class="btn btn-primary mt-27 ">Submit</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<?php }elseif ( $type == 'ram'){?>
					<div class="row">
						<div class="col-12">
							<h4>Edit Ram</h4>
							<form class="needs-validation" novalidate="" action="<?php echo base_url('ram/u');?>" method="post" enctype="multipart/form-data">
								<div class="card-header">
									<div class="form-row">
										<div class="form-group col-md-6">
											<label>Ram Size</label> <input type="text" name="size"
												required="" value="<?php echo $ram['size']?>"
												class="form-control">
											<div class="invalid-feedback">Please provide size.!</div>
											<?php echo form_error('size', '<div style="color:red">', '</div>');?>
										</div>
										<input type="hidden" name="id" value="<?php echo $ram['id'] ; ?>">
										<div class="form-group col-md-2">
											<button type="submit" name="upload" id="upload" value="Apply"
												class="btn btn-primary mt-27 ">Submit</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<?php }elseif ($type == 'storage'){?>
					<div class="row">
						<div class="col-12">
							<h4>Edit Storage</h4>
							<form class="needs-validation" novalidate="" action="<?php echo base_url('storage/u');?>" method="post" enctype="multipart/form-data">
								<div class="card-header">
									<div class="form-row">
										<div class="form-group col-md-6">
											<label>Storage Size</label> <input type="text" name="size"
												required="" value="<?php echo $storage['size']?>"
												class="form-control">
											<div class="invalid-feedback">Please provide size.!</div>
											<?php echo form_error('size', '<div style="color:red">', '</div>');?>
										</div>
										<input type="hidden" name="id" value="<?php echo $storage['id'] ; ?>">
										<div class="form-group col-md-2">
											<button type="submit" name="upload" id="upload" value="Apply"
												class="btn btn-primary mt-27 ">Submit</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<?php }elseif ($type == 'batteries'){ ?>
					<div class="row">
						<div class="col-12">
							<h4>Edit Battery Capacity</h4>
							<form class="needs-validation" novalidate="" action="<?php echo base_url('batteries/u');?>" method="post" enctype="multipart/form-data">
								<div class="card-header">
									<div class="form-row">
										<div class="form-group col-md-6">
											<label>Battery Capacity</label> <input type="text" name="capacity"
												required="" value="<?php echo $battery['capacity']?>"
												class="form-control">
											<div class="invalid-feedback">Please provide capacity..!</div>
											<?php echo form_error('capacity', '<div style="color:red">', '</div>');?>
										</div>
										<input type="hidden" name="id" value="<?php echo $battery['id'] ; ?>">
										<div class="form-group col-md-2">
											<button type="submit" name="upload" id="upload" value="Apply"
												class="btn btn-primary mt-27 ">Submit</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<?php }elseif ($type == 'size'){?>
					<div class="row">
						<div class="col-12">
							<h4>Edit Size</h4>
							<form class="needs-validation" novalidate="" action="<?php echo base_url('size/u');?>" method="post" enctype="multipart/form-data">
								<div class="card-header">
									<div class="form-row">
										<div class="form-group col-md-6">
											<label>Size</label> <input type="text" name="size"
												required="" value="<?php echo $size['size']?>"
												class="form-control">
											<div class="invalid-feedback">Please provide size.!</div>
											<?php echo form_error('size', '<div style="color:red">', '</div>');?>
										</div>
										<input type="hidden" name="id" value="<?php echo $size['id'] ; ?>">
										<div class="form-group col-md-2">
											<button type="submit" name="upload" id="upload" value="Apply"
												class="btn btn-primary mt-27 ">Submit</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<?php }elseif ($type == 'colour'){?>
					<div class="row">
						<div class="col-12">
							<h4>Edit Colour</h4>
							<form class="needs-validation" novalidate="" action="<?php echo base_url('colour/u');?>" method="post" enctype="multipart/form-data">
								<div class="card-header">
									<div class="form-row">
										<div class="form-group col-md-6">
											<label>Colour Name</label> <input type="text" name="name"
												required="" value="<?php echo $colour['name']?>"
												class="form-control">
											<div class="invalid-feedback">Please provide colour name.!</div>
											<?php echo form_error('name', '<div style="color:red">', '</div>');?>
										</div>
										
										<div class="form-group col-md-6">
											<label>Colour code</label> <input type="text" id="picker" name="color_code"
												required="" value="<?php echo $colour['color_code'];?>"
												class="form-control">
											<div class="invalid-feedback">Please provide colour code.!</div>
											<?php echo form_error('color_code', '<div style="color:red">', '</div>');?>
										</div>
										<input type="hidden" name="id" value="<?php echo $colour['id'] ; ?>">
										<div class="form-group col-md-2">
											<button type="submit" name="upload" id="upload" value="Apply"
												class="btn btn-primary mt-27 ">Submit</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<?php }?>