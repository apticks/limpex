<!--Add Category And its list-->
<div class="row">
	<div class="col-12">
		<h4>Orders</h4>
		<form  novalidate="" action="<?php echo base_url('orders/tracking_info_update');?>" method="post" enctype="multipart/form-data">
			<div class="card-header" >
				<div class="row justify-content-center">
					<div class="col-6 admin-address">
                          <div class="container" style="border-bottom:1px solid black">
                            <h2><?php echo $order['address']['name'];?></h2>
                          </div>
                            <hr>
                          <ul class="container admin-address_details">
                            <li><p><span class="glyphicon glyphicon-earphone one" data-feather="smartphone" style="width:50px;">Mobile</span>+91 <?php echo $order['address']['phone'];?></p></li>
                            <li><p><span class="glyphicon glyphicon-envelope one" data-feather="mail" style="width:50px;">Email</span><?php echo $order['address']['email'];?></p></li>
                            <li><p><span class="glyphicon glyphicon-map-marker one" data-feather="home" style="width:50px;">Address</span><?php echo $order['address']['address'];?></p></li>
                            <li><p><span class="glyphicon glyphicon-new-window one" data-feather="map-pin" style="width:50px;">Pincode</span><?php echo $order['address']['pincode'];?></p>
                          </ul>
					</div>
				</div>
				<div class="form-row justify-content-center">
					<div class="form-group col-md-4">
						<label>Order tracking id</label>
						 <input type="text" name="track_id" required="" value="<?php echo $order['track_id'];?>" class="form-control" placeholder="Track id">
						 <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
					</div>
					<div class="form-group col-md-3">
						<label>Order status</label> 
						<select class="form-control" name="status">
							<option value="null" selected disabled>--select--</option>
							<option value="0" <?php echo ( $order['status'] == 0 )? 'selected': '';?>>Cancelled</option>
							<option value="1" <?php echo ( $order['status'] == 1 )? 'selected': '';?>>Placed</option>
							<option value="2" <?php echo ( $order['status'] == 2 )? 'selected': '';?>>Shipped</option>
							<option value="3" <?php echo ( $order['status'] == 3 )? 'selected': '';?>>Delivered</option>
						</select>
					</div>
					
					<div class="form-group col-md-1">
						<button type="submit" name="upload" id="upload" value="Apply"
							class="btn btn-primary mt-27 ">Submit</button>
					</div>
				</div>
			</div>
		</form>

		<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of Orders</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>Date</th>
									<th>Order item id</th>
									<th>Product Name</th>
									<th>MFG Brand</th>
									<th>Model</th>
									<th>Color</th>
									<th>Quantity</th>
									<th>Price</th>
									<th>Vendor</th>
									<th>Product Code</th>
									<th>SKU</th>
									<th>Status</th>
									<th>Actions</th>

								</tr>
							</thead>
							<tbody>
							<?php if(!empty($order_details)):?>
    							<?php  $sno = 1; foreach ($order_details as $ord): ?>
    								<tr>
									<td><?php echo $sno++;?></td>
									<td><?php echo date('d-m-Y', strtotime($ord['created_at']));?></td>
									<td><?php echo $ord['order_item_code'];?></td>
									<td><?php echo $ord['product']['name'];?></td>
									<td><?php echo $ord['product']['brand'];?></td>
									<td><?php echo $ord['variant_values']['model'];?></td>
									<td><?php echo $ord['variant_values']['color'];?></td>
									<td><?php echo $ord['qty'];?></td>
									<td><?php echo $ord['price'];?></td>
									<!-- <td>
										<ul>
											<li>Code: <b><?php //echo $ord['product']['product_code']?></b></li>
											<li>NAME: <b><?php //echo $ord['product']['name']?></b></li>
										</ul>
									</td> -->
									<td>
										<ul>
											<li>ID: <b><?php echo $ord['vendor']['unique_id']?></b></li>
											<li>NAME: <b><?php echo $ord['vendor']['first_name']?></b></li>
											<li>Mobile: <b><?php echo $ord['vendor']['phone']?></b></li>
											<li>Email: <b><?php echo $ord['vendor']['email']?></b></li>
										</ul>
									</td>
									<td><?php echo $ord['product']['product_code'];?></td>
									<td><?php echo $ord['variant']['sku'];?></td>
									<!--<td>
										<ul>
											<li>SKU: <b><?php //echo $ord['variant']['sku']?></b></li>
											<li>Price: <b><?php //echo $ord['variant']['price']?></b></li>
										</ul>
									</td>-->
									<td><b><?php 
										if($ord['status'] == 1){
											echo "Received";
										}elseif ($ord['status'] == 2){
											echo "Accepted";
										}elseif ($ord['status'] == 0){
											echo "Cancelled";
										}elseif ($ord['status'] == 3){
											echo "Arriced at hub";
										}
									?></b></td>
									<td>
										<input type="checkbox" <?php echo ($ord['status'] == 3) ? 'checked' : '';?> order_item_id="<?php echo $ord['id']?>" amount="<?php echo $ord['price']?>"  vendor_id="<?php echo $ord['vendor']['id']?>"  order_no="<?php echo $order['order_no']?>" order_item_code="<?php echo $ord['order_item_code'];?>" class="change_order_item_status" data-on="Arrivied At Hub" data-off="Not Yet Arrived" data-toggle="toggle" data-onstyle="success" data-offstyle="primary"> 
									</td>

								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr>
									<th colspan='8'><h3>
											<center>No Orders</center>
										</h3></th>
								</tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>
</div>
