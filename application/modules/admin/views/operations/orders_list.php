<!--Add Category And its list-->
<div class="row">
	<div class="col-12">
		<h4>Orders</h4>
		<form  action="<?php echo base_url('orders/r');?>" method="post" enctype="multipart/form-data">
			<div class="card-header">
				<div class="form-row">
					<div class="form-group col-md-4">
						<label>Order start date</label>
						 <input type="text" name="start_date" id="order-start-date" required="" value="<?php if(isset($_SESSION['order_filters']['start_date'])){echo $_SESSION['order_filters']['start_date'];}?>" class="form-control" placeholder="yyyy-mm-dd">
					</div>
					<div class="form-group col-md-4">
						<label>Order end date</label>
						 <input type="text" name="end_date" id="order-end-date" value="<?php if(isset($_SESSION['order_filters']['end_date'])){echo $_SESSION['order_filters']['end_date'];}?>" class="form-control" placeholder="yyyy-mm-dd">
					</div>
					<div class="form-group col-md-3">
						<label>Order status</label> 
						<select class="form-control" name="status">
							<option value="null" selected disabled>--select--</option>
							<option value="0" <?php if(isset($_SESSION['order_filters']['status']) && $_SESSION['order_filters']['status'] == 0){echo 'selected';}?>>Cancelled</option>
							<option value="1" <?php if(isset($_SESSION['order_filters']['status']) && $_SESSION['order_filters']['status'] == 1){echo 'selected';}?>>Placed</option>
							<option value="2" <?php if(isset($_SESSION['order_filters']['status']) && $_SESSION['order_filters']['status'] == 2){echo 'selected';}?>>Shipped</option>
							<option value="3" <?php if(isset($_SESSION['order_filters']['status']) && $_SESSION['order_filters']['status'] == 3){echo 'selected';}?>>Delivered</option>
						</select>
					</div>
					
					<div class="form-group col-md-1">
						<button type="submit" name="upload" id="upload" value="Apply"
							class="btn btn-primary mt-27 ">Submit</button>
					</div>
				</div>
			</div>
		</form>

		<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of Orders</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>Order no</th>
									<th>Referece</th>
									<th>Track id</th>
									<th>Discount</th>
									<th>Total</th>
									<th>Payemnt mode</th>
									<th>Date</th>
									<th>Status</th>
									<th>Actions</th>

								</tr>
							</thead>
							<tbody>
							<?php if(!empty($orders)):?>
    							<?php  $sno = 1; foreach ($orders as $order): ?>
    								<tr>
									<td><?php echo $sno++;?></td>
									<td><?php echo $order['order_no'];?></td>
									<td><?php if(! empty($order['coupon'])){?>
										<ul>
											<li>Title : <b><?php echo $order['coupon'][0]['title'];?></b></li>
											<li>Owner : <b><?php echo $order['coupon'][0]['owner'];?></b></li>
											<li>Code : <b><?php echo $order['coupon'][0]['code'];?></b></li>
											<li>Discount(in %) : <b><?php echo $order['coupon'][0]['per_cent'];?></b></li>
											<li>Discount(in RS) : <b><?php echo $order['coupon'][0]['amount'];?></b></li>
										</ul>
										<?php }?>
									</td>
									<td><?php echo $order['track_id'];?></td>
									<td><?php echo $order['discount'];?></td>
									<td><?php echo $order['total'];?></td>
									<td>
										<?php if($order['payment_method_id'] == 1){
											echo 'COD';
										}elseif ($order['payment_method_id'] == 2){
											echo 'PAYU MONEY';
										}else{
											echo 'INSTAMOJO';
										}?>
									</td>
									<td><?php echo $order['created_at'];?></td>
									<td><?php 
										if($order['status'] == 0){
											echo "Cancelled";
										}elseif ($order['status'] == 1){
											echo "Placed";
										}elseif ($order['status'] == 2){
											echo "Shipped";
										}elseif ($order['status'] == 3){
											echo "Delivered";
										}
									?></td>
									<td>
										<a href="<?php echo base_url()?>orders/info?id=<?php echo $order['id']; ?>" class=" mr-2  " type="ecom_brands"> 
											<i class="fas fa-eye"></i>
										</a> 
									</td>

								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr>
									<th colspan='10'><h3>
											<center>No Orders</center>
										</h3></th>
								</tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>
</div>
