<div class="container">
    <div class="row">
        <div class="col-md-12" style="">
            <form id="form_site_settings" action="<?php echo base_url('settings/site');?>" method="post" class="needs-validation reset" novalidate="" enctype="multipart/form-data">
                <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                        </div>
                        <h2 class="card-title">System Settings</h2>
                    </header>
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-3 ">System Name<span class="required">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" name="system_name" class="form-control" placeholder="System Name" required="" value="<?php echo $this->setting_model->where('key', 'system_name')->get()['value'];?>">
                            </div>
                            <div class="invalid-feedback">System Name?</div>
                            <?php echo form_error('system_name','<div style="color:red">','</div>');?>
                                <input type="hidden" name="id" value="">
                                <br>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 ">System Title <span class="required">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" name="system_title" class="form-control" placeholder="System Title " required="" value="<?php echo $this->setting_model->where('key','system_title')->get()['value'];?>">
                            </div>
                            <div class="invalid-feedback">System Title ?</div>
                            <?php echo form_error('system_title','<div style="color:red">','</div>');?>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 ">Mobile Number<span class="required">*</span></label>
                            <div class="col-sm-9">
                                <input type="number" name="mobile" class="form-control" placeholder="Mobile Number" required="" value="<?php echo $this->setting_model->where('key','mobile')->get()['value'];?>">
                            </div>
                            <div class="invalid-feedback">Mobile Number?</div>
                            <?php echo form_error('mobile','<div style="color:red" "margin_left=100px">','</div>');?>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 ">Address<span class="required">*</span></label>
                            <div class="col-sm-9 ">
                                <input type="text" class="form-control" style=" height: 70px " name="address" value=" <?php echo $this->setting_model->where('key','address')->get()['value'];?>" >
                               
                            </div>
                            <div class="invalid-feedback">Address?</div>
                            <?php echo form_error('address','<div style="color:red">','</div>');?>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 ">Facebook Link</label>
                            <div class="col-sm-9">
                                <input type="text" name="facebook" class="form-control" placeholder="Facebook Link" value="<?php echo $this->setting_model->where('key','facebook')->get()['value'];?>">
                            </div>
                            <div class="invalid-feedback">Facebook Link?</div>
                            <?php echo form_error('facebook','<div style="color:red ">','</div>');?>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 ">Twiter Link</label>
                            <div class="col-sm-9">
                                <input type="text" name="twiter" class="form-control" placeholder="Twiter Link" value="<?php echo $this->setting_model->where('key','twiter')->get()['value'];?>">
                            </div>
                            <div class="invalid-feedback">Twiter Link?</div>
                            <?php echo form_error('twiter','<div style="color:red">','</div>');?>
                        </div>
                        <!-- <div class="form-group row">
                            <label class="col-sm-3 ">Youtube Link</label>
                            <div class="col-sm-9">
                                <input type="text" name="youtube" class="form-control" placeholder="Youtube Link" value="<?php //echo $this->setting_model->where('key','youtube')->get()['value'];?>">
                            </div>
                            <div class="invalid-feedback">Youtube Link?</div>
                            <?php //echo form_error('youtube','<div style="color:red">','</div>');?>
                        </div> -->
                        <div class="form-group row">
                            <label class="col-sm-3 ">Linkdin Link</label>
                            <div class="col-sm-9">
                                <input type="text" name="linkdin" class="form-control" placeholder="Linkdin Link" value="<?php echo $this->setting_model->where('key','linkdin')->get()['value'];?>">
                            </div>
                            <div class="invalid-feedback">Linkdin Link?</div>
                            <?php echo form_error('linkdin','<div style="color:red">','</div>');?>
                        </div>
                        <div class="row justify-content-end">
                            <div class="col-sm-9">
                                <button class="btn btn-primary">Submit</button>
                                <input type="button" class="btn btn-default" onClick="clear_form('form_site_settings')" value="Reset" />
                            </div>
                        </div>

                    </div>
            </form>
            </section>
        </div>
        <div class="col-md-6">
            <form id="form_sms" action="<?php echo base_url('settings/sms');?>" class="needs-validation" novalidate="" method="post" enctype="multipart/form-data">
                <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                        </div>
                        <h2 class="card-title">SMS Settings</h2>
                    </header>
                    <br>
                    <div class="card-body">

                        <div class="form-group row">
                            <label class="col-sm-3 ">Username <span class="required">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" name="sms_username" class="form-control" placeholder="Username" required="" value="<?php echo $this->setting_model->where('key','sms_username')->get()['value'];?>">
                            </div>
                            <div class="invalid-feedback">sms_username?</div>
                            <?php echo form_error('sms_username','<div style="color:red">','</div>');?>
                        </div>
                        <br>
                        <div class="form-group row">
                            <label class="col-sm-3 ">Sender <span class="required">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" name="sms_sender" class="form-control" placeholder="Sender" required="" value="<?php echo $this->setting_model->where('key','sms_sender')->get()['value'];?>">
                            </div>
                            <div class="invalid-feedback">sms_sender?</div>
                            <?php echo form_error('sms_sender','<div style="color:red">','</div>');?>
                        </div>
                        <br>
                        <div class="form-group row">
                            <label class="col-sm-3 ">Hash Key <span class="required">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" name="sms_hash" class="form-control" placeholder="Hash Key" required="" value="<?php echo $this->setting_model->where('key','sms_hash')->get()['value'];?>">
                            </div>
                            <div class="invalid-feedback">Hash Key?</div>
                            <?php echo form_error('sms_hash','<div style="color:red">','</div>');?>
                        </div>

                        <div class="row justify-content-end">
                            <div class="col-sm-9">
                                <button class="btn btn-primary">Submit</button>
                                <input type="button" class="btn btn-default" onClick="clear_form('form_sms')" value="Reset" />
                            </div>
                        </div>
                    </div>
            </form>
            </section>
        </div>

        <div class="col-md-6">
            <form id="form-smtp" action="<?php echo base_url('settings/smtp');?>" class="needs-validation form" novalidate="" method="post" enctype="multipart/form-data">
                <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                        </div>
                        <h2 class="card-title">SMTP Settings</h2>
                    </header>
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-3 ">SMTP Port <span class="required">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" name="smtp_port" class="form-control" placeholder="SMTP Port" required="" value="<?php echo $this->setting_model->where('key','smtp_port')->get()['value']?>">
                            </div>
                            <div class="invalid-feedback">smtp_port?</div>
                            <?php echo form_error('smtp_port','<div style="color:red">','</div>');?>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 ">SMTP Host<span class="required">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" name="smtp_host" class="form-control" placeholder="SMTP Host" required="" value="<?php echo $this->setting_model->where('key','smtp_host')->get()['value']?>">
                            </div>
                            <div class="invalid-feedback">smtp_host?</div>
                            <?php echo form_error('smtp_host','<div style="color:red">','</div>');?>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 ">SMTP Username<span class="required">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" name="smtp_username" class="form-control" placeholder="SMTP Username" required="" value="<?php echo $this->setting_model->where('key','smtp_username')->get()['value']?>">
                            </div>
                            <div class="invalid-feedback">smtp_username?</div>
                            <?php echo form_error('smtp_username','<div style="color:red">','</div>');?>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 ">SMTP Password<span class="required">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" name="smtp_password" class="form-control" placeholder="SMTP Password" required="" value="<?php echo $this->setting_model->where('key','smtp_password')->get()['value']?>">
                            </div>
                            <div class="invalid-feedback">smtp_password?</div>
                            <?php echo form_error('smtp_password','<div style="color:red">','</div>');?>
                        </div>

                        <div class="row justify-content-end">
                            <div class="col-sm-9">
                                <button class="btn btn-primary">Submit</button>
                                <input type="button" class="btn btn-default" onClick="clear_form('form-smtp')" value="Reset" />
                            </div>
                        </div>
                    </div>
            </form>
            </section>
        </div>
    </div>
    <style>#editor{
  padding: 0.4em 0.4em 0.4em 0;

}</style>