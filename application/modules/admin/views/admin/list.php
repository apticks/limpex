<?php if($plan_type == 1){ ?>
<div class="row">
<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of Daily Members</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Id</th>
									<th>Name</th>
									<th>Aadhar</th>
									<th>Mobile</th>
									<th>Plan</th>
									<th>Amount</th>
									<th>Actions</th>

								</tr>
							</thead>
							<tbody>
								<?php if(!empty($members)):?>
    							<?php $sno = 1; foreach ($members as $member):?>
    								<tr>
    									<td><?php echo $sno++;?></td>
    									<td><?php echo $member['name'];?></td>
    									<td><?php echo $member['aadhar_no'];?></td>
    									<td><?php echo $member['mobile'];?></td>
    									<td><?php echo 'Daily';?></td>
    									<td><?php echo $member['payment'];?></td>
    									<td><a href="<?php echo base_url()?>members_edit/1?id=<?php echo $member['id'] ?>" class=" mr-2  " plan_type="1" > <i class="fas fa-pencil-alt"></i>
    									</a> <a href="#" class="mr-2  text-danger " onClick="delete_record(<?php echo $member['id'] ?>, 'members_delete')"> <i
    											class="far fa-trash-alt"></i>
    									</a></td>
    
    								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr ><th colspan='7'><h3><center> No Daily Members </center></h3></th></tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>
</div>
<?php }elseif ($plan_type == 2){?>
<div class="row">
<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of Weekly Members</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Id</th>
									<th>Name</th>
									<th>Aadhar</th>
									<th>Mobile</th>
									<th>Plan</th>
									<th>Amount</th>
									<th>Actions</th>

								</tr>
							</thead>
							<tbody>
								<?php if(!empty($members)):?>
    							<?php $sno = 1; foreach ($members as $member):?>
    								<tr>
    									<td><?php echo $sno++;?></td>
    									<td><?php echo $member['name'];?></td>
    									<td><?php echo $member['aadhar_no'];?></td>
    									<td><?php echo $member['mobile'];?></td>
    									<td><?php echo 'Weekly';?></td>
    									<td><?php echo $member['payment'];?></td>
    									<td><a href="<?php echo base_url()?>members_edit/2?id=<?php echo $member['id'] ?>" class=" mr-2  " plan_type="2" > <i class="fas fa-pencil-alt"></i>
    									</a> <a href="#" class="mr-2  text-danger " onClick="delete_record(<?php echo $member['id'] ?>, 'members_delete')"> <i
    											class="far fa-trash-alt"></i>
    									</a></td>
    
    								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr ><th colspan='7'><h3><center>No Weekly Members</center></h3></th></tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>
	</div>
</div>
<?php }elseif ($plan_type == 3){?>
<div class="row">
<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of Monthly Members</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Id</th>
									<th>Name</th>
									<th>Aadhar</th>
									<th>Mobile</th>
									<th>Plan</th>
									<th>Amount</th>
									<th>Actions</th>

								</tr>
							</thead>
							<tbody>
								<?php if(!empty($members)):?>
    							<?php $sno = 1; foreach ($members as $member):?>
    								<tr>
    									<td><?php echo $sno++;?></td>
    									<td><?php echo $member['name'];?></td>
    									<td><?php echo $member['aadhar_no'];?></td>
    									<td><?php echo $member['mobile'];?></td>
    									<td><?php echo 'Monthly';?></td>
    									<td><?php echo $member['payment'];?></td>
    									<td><a href="<?php echo base_url()?>members_edit/3?id=<?php echo $member['id'] ?>" class=" mr-2  " type="category" > <i class="fas fa-pencil-alt"></i>
    									</a> <a href="#" class="mr-2  text-danger " onClick="delete_record(<?php echo $member['id'] ?>, 'members_delete')"> <i
    											class="far fa-trash-alt"></i>
    									</a></td>
    
    								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr ><th colspan='7'><h3><center>No Monthly Members</center></h3></th></tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>
</div>
<?php }else{?>
<div class="row">
<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of Mahila Members</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Id</th>
									<th>Name</th>
									<th>Aadhar</th>
									<th>Mobile</th>
									<th>Plan</th>
									<th>Amount</th>
									<th>Actions</th>

								</tr>
							</thead>
							<tbody>
								<?php if(!empty($members)):?>
    							<?php $sno = 1; foreach ($members as $member):?>
    								<tr>
    									<td><?php echo $sno++;?></td>
    									<td><?php echo $member['name'];?></td>
    									<td><?php echo $member['aadhar_no'];?></td>
    									<td><?php echo $member['mobile'];?></td>
    									<td><?php echo 'Mahila';?></td>
    									<td><?php echo $member['payment'];?></td>
    									<td><a href="<?php echo base_url()?>members_edit/1?id=<?php echo $member['id'] ?>" class=" mr-2  " type="category" > <i class="fas fa-pencil-alt"></i>
    									</a> <a href="#" class="mr-2  text-danger " onClick="delete_record(<?php echo $member['id'] ?>, 'members_delete')"> <i
    											class="far fa-trash-alt"></i>
    									</a></td>
    
    								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr ><th colspan='7'><h3><center>No Mahila Members</center></h3></th></tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>
</div>
<?php }?>