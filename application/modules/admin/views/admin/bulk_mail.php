<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of Mails</h4>
					<a class="btn btn-outline-dark btn-lg col-2" href="<?php echo base_url('bulk_email/send_mail')?>"><i class="fa fa-plus" aria-hidden="true"></i> Send Email</a>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>To Sucscribers</th>
									<th>To Email</th>
									<th>Subject</th>
									<th>Message</th>
									<th>Attachment</th>
								</tr>
							</thead>
							<tbody>
							<?php if(!empty($email_details)):?>
    							<?php  $sno = 1; foreach ($email_details as $details): ?>
    								<tr>
									<td><?php echo $sno++;?></td>
									<td><?php $subscribers = explode(",", $details['emails']);
									if(is_array($subscribers)){
									    echo "<ul>";
									    foreach ($subscribers as $s){
									    echo "<li>$s</li>";
									}
									   echo "</ul>";
									}else{
									    echo "NA";
									}
									   
									?></td>
									<td><?php echo $details['manual_email'];?></td>
									<td><?php echo $details['subject'];?></td>
									<td><?php echo $details['desc'];?></td>
									<td><?php if($details['attachment'] == 1) {?>
									<img
										src="<?php echo base_url();?>uploads/email_attachment_image/email_attachment_<?php echo $details['id'];?>.jpg?<?php echo time();?>"
										class="img-thumb" style="width: 100px;height: 63px;">
										<?php }else { echo "No Attachment"; }?>
									</td>
									</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr>
									<th colspan='6'><h3>
											<center>No Mails</center>
										</h3></th>
								</tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>
</div>
