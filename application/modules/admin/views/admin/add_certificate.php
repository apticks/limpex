<!--Add Sub_Category And its list-->
<div class="row">
  <div class="col-12">
    <h4 class="ven">Add Certificate</h4>
    <form class="needs-validation" novalidate=""
      action="<?php echo base_url('certificates/c');?>" method="post"
      enctype="multipart/form-data">
      <div class="card-header">

        <div class="form-row">
         <div class="form-group mb-0 col-md-12">
            <label>Title</label> <input type="text" class="form-control"
              name="title" required="" placeholder="Title" <?php echo set_value('title')?>>
            <div class="invalid-feedback">Give Title</div>
             <?php echo form_error('title','<div style="color:red">','</div>');?>
          </div>

          <div class="form-group col-md-4">
              <label>Upload Image</label> <input type="file" name="file"
               value="<?php echo set_value('file')?>"
              class="form-control" onchange="readURL(this);" > <br> <img id="blah"
              src="#" alt="" style="width: 200px">
            <div class="invalid-feedback">Upload Image?</div>
            <?php echo form_error('file', '<div style="color:red">', '</div>');?>
          </div>
          <div class="form-group col-md-12">

            <button class="btn btn-primary mt-27 ">Submit</button>
          </div>
</div>
</div>
    </form>
</div>
</div>
