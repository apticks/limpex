<!--Add Sub_Category And its list-->
<div class="row">
	<div class="col-12">
		<h4>Subscribe</h4>
		<form class="needs-validation" novalidate=""
			action="<?php echo base_url('bulk_email/send_mail');?>" method="post"
			enctype="multipart/form-data">
			<div class="card-header">
				<div class="form-row">
				<div class="form-group col-md-6">
						<label>Subscribers Email List</label>
						<!-- <input type="file" class="form-control" required="">-->
						<select class="form-control" id="subscribe_multiselect" name="emails[]" multiple="multiple">
							<?php foreach ($email_details as $email):?>
								<option value="<?php echo $email['email'];?>"><?php echo $email['email']?></option>
							<?php endforeach;?>
						</select>
						<div class="invalid-feedback">New Category Name?</div>
						<?php echo form_error('emails[]','<div style="color:red>"','</div>');?>
				</div>

				<div class="form-group col-md-6">
						<label>To Mail</label> <input type="text"
							class="form-control" name="manual_email" value="<?php echo set_value('manual_email')?>" placeholder="Mail">
						<div class="invalid-feedback">New Sub_Category Name?</div>
						<?php echo form_error('manual_email','<div style="color:red">','</div>')?>
					</div>

					<div class="form-group col-md-12">
						<label>Subject</label> <input type="text"
							class="form-control" name="subject" required="" value="<?php echo set_value('subject')?>" placeholder="Subject">
						<div class="invalid-feedback">New Sub_Category Name?</div>
						<?php echo form_error('subject','<div style="color:red">','</div>')?>
					</div>
					<div class="form-group  col-md-12" >
					<label>Attachment</label> 
          				<input type="file" class="form-control" name="attachment"   placeholder="Attachment">
					</div>
					
					<div class="form-group  col-md-12" >
					<label>Message</label> 
          				<textarea id="sub_cat" name="desc" class="ckeditor" rows="10" data-sample-short></textarea>
          					<?php echo form_error('desc', '<div style="color:red">', '</div>');?>
        			</div>
					<div class="form-group col-md-2">

						<button class="btn btn-primary mt-27 ">Submit</button>
					</div>


				</div>


			</div>
		</form>
	</div>
</div>

