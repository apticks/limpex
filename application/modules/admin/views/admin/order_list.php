<div class="row">
	<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of  Order</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>Product Name</th>
									<th>Product Image</th>
									<th>Customer Name</th>
									<th>Quantity</th>
									<th>Email</th>
									<th>Contact</th>
									<th>Address</th>
									<th>Date</th>
									

								</tr>
							</thead>
							
							<tbody>
								<?php if(!empty($orders)):?>
    							<?php $sno = 1; foreach ($orders as $con):?>
    								<tr>
    									<td><?php echo $sno++;?></td>
    									<td><?php echo $con['product']['name'];?></td>
    									<td><img
										src="<?php echo base_url();?>uploads/ecom_product_image/ecom_product_<?php echo $con['product']['id'];?>.jpg?<?php echo time();?>" style="width: 100px;height: 63px;" class="img-thumb"></td>
    									<td><?php echo $con['name'];?></td>
    									<td><?php echo $con['qty'];?></td>
    									<td><?php echo $con['email'];?></td>
    									<td><?php echo $con['contact'];?></td>
    									<td><?php echo $con['address'];?></td>
    									<td><?php echo date('Y-M-d H:i', strtotime($con['product']['created_at']))?></td>
    								
    								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr ><th colspan='8'><h3><center>No Enquiries</center></h3></th></tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>
	</div>