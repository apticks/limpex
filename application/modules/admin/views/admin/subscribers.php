<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of Subscribers</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>Email</th>
								</tr>
							</thead>
							<tbody>
							<?php if(!empty($subscribers)):?>
    							<?php  $sno = 1; foreach ($subscribers as $subscriber): ?>
    								<tr>
									<td><?php echo $sno++;?></td>
									<td><?php echo $subscriber['email'];?></td>
								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr>
									<th colspan='4'><h3>
											<center>No Subscribers</center>
										</h3></th>
								</tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>
</div>
