<div class="row">
	<div class="col-12">
		<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>All Users</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>Unique id</th>
									<th>Name</th>
									<th>Mobile</th>
									<th>Email</th>

								</tr>
							</thead>
							<tbody>
							<?php if(!empty($users)):?>
    							<?php  $sno = 1; foreach ($users as $user): ?>
    								<tr>
									<td><?php echo $sno++;?></td>
									<td><?php echo $user['unique_id'];?></td>
									<td><?php echo $user['first_name'].' '.$user['last_name'];?></td>
									<td><?php echo $user['phone'];?></td>
									<td><?php echo $user['email'];?></td>
								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr>
									<th colspan='6'><h3>
											<center>No Sellers</center>
										</h3></th>
								</tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>
		
