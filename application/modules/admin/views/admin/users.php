<div class="row">
	<div class="col-12">
		<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>Pending Sellers</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>Unique id</th>
									<th>Name</th>
									<th>Mobile</th>
									<th>Email</th>
									<th>Actions</th>

								</tr>
							</thead>
							<tbody>
							<?php if(!empty($users)):?>
    							<?php  $sno = 1; foreach ($users as $user): ?>
    								<tr>
									<td><?php echo $sno++;?></td>
									<td><?php echo $user['unique_id'];?></td>
									<td><?php echo $user['first_name'].' '.$user['last_name'];?></td>
									<td><?php echo $user['phone'];?></td>
									<td><?php echo $user['email'];?></td>
									<td>
										<select class="form-control " onchange="change_status(<?php echo $user['id']?>)" id="change_status<?php echo $user['id']?>">
											<option value="2" selected >Disapprove</option>
											<option value="1" >Approve</option>
										</select>
									</td>

								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr>
									<th colspan='6'><h3>
											<center>No Sellers</center>
										</h3></th>
								</tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>
		
		<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>Approved Sellers</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport1"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>Unique id</th>
									<th>Name</th>
									<th>Mobile</th>
									<th>Email</th>
									<th>Actions</th>

								</tr>
							</thead>
							<tbody>
							<?php if(!empty($approved_users)):?>
    							<?php  $sno = 1; foreach ($approved_users as $user): ?>
    								<tr>
									<td><?php echo $sno++;?></td>
									<td><?php echo $user['unique_id'];?></td>
									<td><?php echo $user['first_name'].' '.$user['last_name'];?></td>
									<td><?php echo $user['phone'];?></td>
									<td><?php echo $user['email'];?></td>
									<td>
										<select class="form-control " onchange="change_status(<?php echo $user['id']?>)" id="change_status<?php echo $user['id']?>">
											<option value="2" >Disapprove</option>
											<option value="1" selected>Approve</option>
										</select>
									</td>
								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr>
									<th colspan='6'><h3>
											<center>No Sellers</center>
										</h3></th>
								</tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>
	</div>
</div>