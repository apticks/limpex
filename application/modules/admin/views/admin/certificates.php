<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4 class="col-10 ven1">List of Certificates</h4>
					<a class="btn btn-outline-dark btn-lg col-2" href="<?php echo base_url('certificates/c')?>"><i class="fa fa-plus" aria-hidden="true"></i> Add Certificate</a>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>Title</th>
									<th>Image</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
							<?php if(!empty($certificates)):?>
    							<?php  $sno = 1; foreach ($certificates as $faq_obj): ?>
    								<tr>
									<td><?php echo $sno++;?></td>
									 <td><?php echo $faq_obj['title'];?></td>
    								<td><img
										src="<?php echo base_url();?>uploads/certificates_image/certificates_<?php echo $faq_obj['id'];?>.jpg?<?php echo time();?>" style="width: 100px;height: 63px;" class="img-thumb"></td>
									<td><a
										href="<?php echo base_url()?>certificates/edit?id=<?php echo $faq_obj['id']; ?>"
										class=" mr-2  " type="certificates"> <i class="fas fa-pencil-alt"></i>
									</a> <a href="#" class="mr-2  text-danger "
										onClick="delete_record(<?php echo $faq_obj['id'] ?>, 'certificates')">
											<i class="far fa-trash-alt"></i>
									</a></td>

								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr>
									<th colspan='5'><h3>
											<center>Sorry!! No Certificate's!!!</center>
										</h3></th>
								</tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>