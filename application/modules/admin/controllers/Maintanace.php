<?php
(defined('BASEPATH')) or exit('No direct script access allowed');

/**
 *
 * @author Mehar
 *         Admin module
 */
class Maintanace extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->template = 'template/admin/main';
        if (! $this->ion_auth->logged_in()) // || ! $this->ion_auth->is_admin()
            redirect('auth/login');

    }

    public function db_backup()
    {
    	// Load the DB utility class
    	$this->load->dbutil();
    	
    	$prefs = array(
    			'tables'        => array('products', 'product_variants', 'product_variant_values'),   // Array of tables to backup.
    			'ignore'        => array(),                     // List of tables to omit from the backup
    			'format'        => 'txt',                       // gzip, zip, txt
    			'filename'      => 'mybackup.sql',              // File name - NEEDED ONLY WITH ZIP FILES
    			'add_drop'      => TRUE,                        // Whether to add DROP TABLE statements to backup file
    			'add_insert'    => TRUE,                        // Whether to add INSERT data to backup file
    			'newline'       => "\n"                         // Newline character used in backup file
    	);
    	// Backup your entire database and assign it to a variable
    	$backup = $this->dbutil->backup($prefs);
    	
    	// Load the file helper and write the file to your server
    	/* $this->load->helper('file');
    	write_file('/path/to/mybackup.gz', $backup); */
    	
    	// Load the download helper and send the file to your desktop
    	$this->load->helper('download');
    	force_download('mybackup.sql', $backup);
    }
    
}