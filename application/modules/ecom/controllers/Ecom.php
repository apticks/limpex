<?php
class Ecom extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->template = 'template/admin/main';
        if (! $this->ion_auth->logged_in()) // || ! $this->ion_auth->is_admin()
            redirect('auth/login');
            $this->load->model('category_model');
            $this->load->model('sub_category_model');
            //$this->load->model('brand_model');
            $this->load->model('product_model');
            $this->load->model('product_variant_model');
           // $this->load->model('size_model');
            //$this->load->model('colour_model');
           // $this->load->model('model_model');
    }
    
    
   
    /**
     * @author Trupti
     * @desc To Manage Ecommerce Categories
     * @param string $type
     */
    public function ecom_category($type = 'r'){
        /* if (! $this->ion_auth_acl->has_permission('ecom'))
            redirect('admin'); */
            
            if ($type == 'c') {
                $this->form_validation->set_rules($this->category_model->rules);
                /* if (empty($_FILES['file']['name'])) {
                    $this->form_validation->set_rules('file', 'Ecommerce Category Image', 'required');
                } */
                if ($this->form_validation->run() == FALSE) {
                    $this->ecom_category('r');
                } else {
                    $id = $this->category_model->insert([
                        'name' => $this->input->post('name'),
                        'desc' => $this->input->post('desc'),
                    ]);
                    
                    $path = $_FILES['file']['name'];
                    $ext = pathinfo($path, PATHINFO_EXTENSION);
                    $this->file_up("file", "ecom_category", $id, '', 'no');
                    redirect('ecom_category/r', 'refresh');
                } 
            } elseif ($type == 'r') {
                $this->data['title'] = 'E-Commerce Category';
                $this->data['content'] = 'ecom/ecom/category';
              	$this->data['ecom_categories'] = $this->category_model->order_by('id', 'ASCE')->get_all();
                $this->_render_page($this->template, $this->data);
            }  elseif ($type == 'u') {
                $this->form_validation->set_rules($this->category_model->rules);
                if ($this->form_validation->run() == FALSE) {
                    echo validation_errors();
                } else {
                    $this->category_model->update([
                        'id' => $this->input->post('id'),
                        'name' => $this->input->post('name'),
                        'desc' => $this->input->post('desc'),
                    ], 'id');
                    if ($_FILES['file']['name'] !== '') {
                        $path = $_FILES['file']['name'];
                        $ext = pathinfo($path, PATHINFO_EXTENSION);
                        $this->file_up("file", "ecom_category", $this->input->post('id'), '', 'no');
                    }
                    redirect('ecom_category/r', 'refresh');
                }
            }elseif ($type == 'd') {
                echo $this->category_model->delete(['id' => $this->input->post('id')]);
            }elseif($type == 'edit'){
                $this->data['title'] = 'Edit E-Commerce Category';
                $this->data['content'] = 'ecom/ecom/edit';
                $this->data['type'] = 'ecom_category';
                $this->data['category'] = $this->category_model->where('id',$this->input->get('id'))->get();
                $this->data['i'] = $this->category_model->where('file',$this->input->get('file'))->get();
                
                
                $this->_render_page($this->template, $this->data);
            }
    }
    
    /**
     * E-Commerce Sub_Category crud
     *
     * @author Trupti
     * @desc To Manage Ecommerce Sub Categories
     * @param string $type
     */
    public function ecom_sub_category($type = 'r')
    {
        
        /* if (! $this->ion_auth_acl->has_permission('ecom'))
            redirect('admin'); */
            
            if ($type == 'c') {
                
                $this->form_validation->set_rules($this->sub_category_model->rules);
                
                /* if (empty($_FILES['file']['name'])) {
                    $this->form_validation->set_rules('file', 'Ecom sub_category Image', 'required');
                } */
                if ($this->form_validation->run() == FALSE) {
                    $this->ecom_sub_category('r');
                } else {
                    $id = $this->sub_category_model->insert([
                        'cat_id' => $this->input->post('cat_id'),
                        'name' => $this->input->post('name'),
                        'desc' => $this->input->post('desc'),
                    ]);
                    $path = $_FILES['file']['name'];
                    $ext = pathinfo($path, PATHINFO_EXTENSION);
                    $this->file_up("file", "ecom_sub_category", $id, '', 'no');
                    redirect('ecom_sub_category/r', 'refresh');
                }
            } elseif ($type == 'r') {
                $this->data['title'] = 'Ecommerce Sub_Category';
                $this->data['content'] = 'ecom/ecom/subcategory';
                $this->data['ecom_categories'] = $this->category_model->get_all();
                $this->data['ecom_sub_categories'] = $this->sub_category_model->get_all();
                //print_r($this->data['brands']);exit();
                $this->_render_page($this->template, $this->data);
                //echo json_encode($this->data);
            } elseif ($type == 'u') {
                $this->form_validation->set_rules($this->sub_category_model->rules);
                if ($this->form_validation->run() == FALSE) {
                    echo validation_errors();
                } else {
                    $this->sub_category_model->update([
                        'cat_id' => $this->input->post('cat_id'),
                        'name' => $this->input->post('name'),
                        'desc' => $this->input->post('desc'),
                    ], $this->input->post('id'));
                    if ($_FILES['file']['name'] !== '') {
                        $path = $_FILES['file']['name'];
                        $ext = pathinfo($path, PATHINFO_EXTENSION);
                        $this->file_up("file", "ecom_sub_category", $this->input->post('id'), '', 'no');
                    }
                    redirect('ecom_sub_category/r', 'refresh');
                }
            } elseif ($type == 'd') {
                $this->sub_category_model->delete(['id' => $this->input->post('id')]);
            }elseif($type == 'edit'){
                $this->data['title'] = 'Edit E-commerce sub_category';
                $this->data['content'] = 'ecom/ecom/edit';
                $this->data['type'] = 'ecom_sub_category';
                $this->data['sub_categories']=$this->sub_category_model->order_by('id', 'DESC')->where('id', $this->input->get('id'))->get();
                $this->data['categories'] = $this->category_model->order_by('id', 'DESC')->get_all();
                $this->_render_page($this->template, $this->data);
            }elseif($type == 'list'){
                $data = $this->sub_category_model->fields('id, name, desc')->where(['cat_id' =>$this->input->post('cat_id')])->get_all();
                echo json_encode($data);
            }
    }
    /**
     * E-Commerce Sub Sub_Category crud
     *
     * @author Trupti
     * @desc To Manage Ecommerce Sub Categories
     * @param string $type
     */
    public function ecom_sub_sub_category($type = 'r')
    {
        
        /* if (! $this->ion_auth_acl->has_permission('ecom'))
            redirect('admin'); */
            
            if ($type == 'c') {
                
                $this->form_validation->set_rules($this->ecom_sub_sub_category_model->rules);
                
                if (empty($_FILES['file']['name'])) {
                    $this->form_validation->set_rules('file', 'Ecom sub_sub_category Image', 'required');
                }
                if ($this->form_validation->run() == FALSE) {
                    $this->ecom_sub_category('r');
                } else {
                    $id = $this->ecom_sub_sub_category_model->insert([
                        'cat_id' => $this->input->post('cat_id'),
                        'sub_cat_id' => $this->input->post('sub_cat_id'),
                        'name' => $this->input->post('name'),
                        'desc' => $this->input->post('desc'),
                    ]);
                  
                    $path = $_FILES['file']['name'];
                    $ext = pathinfo($path, PATHINFO_EXTENSION);
                    $this->file_up("file", "ecom_sub_sub_category", $id, '', 'no');
                    redirect('ecom_sub_sub_category/r', 'refresh');
                }
            } elseif ($type == 'r') {
                $this->data['title'] = 'Ecommerce Sub Sub_Category';
                $this->data['content'] = 'ecom/ecom/sub_subcategory';
                $this->data['ecom_categories'] = $this->category_model->get_all();
                $this->data['ecom_sub_categories'] = $this->sub_category_model->get_all();
                $this->data['ecom_sub_sub_categories'] = $this->ecom_sub_sub_category_model->get_all();
                $this->_render_page($this->template, $this->data);
                //echo json_encode($this->data);
            } elseif ($type == 'u') {
                $this->form_validation->set_rules($this->ecom_sub_sub_category_model->rules);
                if ($this->form_validation->run() == FALSE) {
                    echo validation_errors();
                } else {
                    $this->ecom_sub_sub_category_model->update([
                        'cat_id' => $this->input->post('cat_id'),
                        'name' => $this->input->post('name'),
                        'desc' => $this->input->post('desc'),
                    ], $this->input->post('id'));
                   /*  foreach ($this->input->post('brand_id') as $sid){
                        if(! $this->db->get_where('ecom_subcategories_brands', ['sub_cat_id' => $this->input->post('id'), 'brand_id' => $sid])->row())
                            $this->db->insert('ecom_subcategories_brands', ['sub_cat_id' => $this->input->post('id'), 'brand_id' => $sid]);
                            else
                                $this->db->update('ecom_subcategories_brands', ['sub_cat_id' => $this->input->post('id'), 'brand_id' => $sid]);
                    } */
                    if ($_FILES['file']['name'] !== '') {
                        $path = $_FILES['file']['name'];
                        $ext = pathinfo($path, PATHINFO_EXTENSION);
                        $this->file_up("file", "ecom_sub_sub_category", $this->input->post('id'), '', 'no');
                    }
                    redirect('ecom_sub_sub_category/r', 'refresh');
                }
            } elseif ($type == 'd') {
                $this->ecom_sub_sub_category_model->delete(['id' => $this->input->post('id')]);
            }elseif($type == 'edit'){
                $this->data['title'] = 'Edit E-commerce sub sub_category';
                $this->data['content'] = 'ecom/ecom/edit';
                $this->data['type'] = 'ecom_sub_sub_category';
                $this->data['sub_categories']=$this->sub_category_model->order_by('id', 'DESC')->get_all();
                $this->data['categories'] = $this->category_model->order_by('id', 'DESC')->get_all();
                $this->data['ecom_sub_sub_categories'] = $this->ecom_sub_sub_category_model->with_category('fields: id, name')->with_sub_category('fields: id, name')->where('id',$this->input->get('id'))->get();
                $this->_render_page($this->template, $this->data);
            }elseif($type == 'list'){
                $data = $this->sub_category_model->fields('id, name, desc')->where(['cat_id' =>$this->input->post('cat_id')])->get_all();
                echo json_encode($data);
            }
    }
     
    /**
     * E-Commerce Product crud
     *
     * @author Trupti
     * @desc To Manage Ecommerce Sub Categories
     * @param string $type
     */
    public function ecom_product($type = 'r')
    {
        
        /* if (! $this->ion_auth_acl->has_permission('ecom'))
            redirect('admin'); */
            
            if ($type == 'c') {
                
                $this->form_validation->set_rules($this->sub_category_model->rules);
                
                /* if (empty($_FILES['file']['name'])) {
                    $this->form_validation->set_rules('file', 'Ecom sub_category Image', 'required');
                } */
                if ($this->form_validation->run() == FALSE) {
                    $this->ecom_product('r');
                } else {
                    $id = $this->product_model->insert([
                        'cat_id' => $this->input->post('cat_id'),
                        'sub_cat_id' => $this->input->post('sub_cat_id'),
                        'name' => $this->input->post('name'),
                        'desc' => $this->input->post('desc'),
                        'varieties_desc' => $this->input->post('varieties_desc'),
                        'season_desc' => $this->input->post('season_desc'),
                        'management_desc' => $this->input->post('management_desc'),
                        'cultivation_desc' => $this->input->post('cultivation_desc'),
                    ]);
                    
                    //print_array($_FILES["files"]["tmp_name"]);
                    if (!file_exists('./uploads/ecom_product_image/')) {
                        mkdir('./uploads/ecom_product_image/', 0777, true);
                    }
                    foreach($_FILES["files"]["tmp_name"] as $key=>$tmp_name) {
                        $this->db->insert('product_images', [
                            'product_id' => $id
                        ]);
                        $insert_id = $this->db->insert_id();
                        //print_r(  $insert_id);exit();
                        move_uploaded_file($_FILES['files']['tmp_name'][$key], './uploads/ecom_product_image/ecom_product_' . $insert_id . '.jpg');
                    }

                    $path = $_FILES['file']['name'];
                    $ext = pathinfo($path, PATHINFO_EXTENSION);
                    $this->file_up("file", "product_nutrition", $id, '', 'no');
                    redirect('ecom_product/r', 'refresh');
                }
            } elseif ($type == 'r') {
                $this->data['title'] = 'Ecommerce Product';
                $this->data['content'] = 'ecom/ecom/product';
                $this->data['ecom_categories'] = $this->category_model->get_all();
                $this->data['ecom_sub_categories'] = $this->sub_category_model->get_all();
                $this->data['ecom_product'] = $this->product_model->get_all();
                //print_r($this->data['brands']);exit();
                $this->_render_page($this->template, $this->data);
                //echo json_encode($this->data);
            } elseif ($type == 'u') {
                $this->form_validation->set_rules($this->product_model->rules);
                if ($this->form_validation->run() == FALSE) {
                    echo validation_errors();
                } else {
                    $this->product_model->update([
                        'cat_id' => $this->input->post('cat_id'),
                        'sub_cat_id'=>$this->input->post('sub_cat_id'),
                        'name' => $this->input->post('name'),
                        'desc' => $this->input->post('desc'),
                        'varieties_desc' => $this->input->post('varieties_desc'),
                        'season_desc' => $this->input->post('season_desc'),
                        'management_desc' => $this->input->post('management_desc'),
                        'cultivation_desc' => $this->input->post('cultivation_desc')
                    ], $this->input->post('id'));
                    if (!file_exists('./uploads/ecom_product_image/')) {
                        mkdir('./uploads/ecom_product_image/', 0777, true);
                    }
                    foreach($_FILES["files"]["tmp_name"] as $key=>$tmp_name) {
                        $this->db->where('id', $this->input->post('id'));
                        $this->db->update('product_images', [
                            'product_id' => $this->input->post('id')
                        ]);
                        move_uploaded_file($_FILES['files']['tmp_name'][$key], './uploads/ecom_product_image/ecom_product_' . $this->input->post('id') . '.jpg');
                    }
                    if ($_FILES['file']['name'] !== '') {
                        $path = $_FILES['file']['name'];
                        $ext = pathinfo($path, PATHINFO_EXTENSION);
                        $this->file_up("file", "product_nutrition", $this->input->post('id'), '', 'no');
                    }
                    redirect('ecom_product/r', 'refresh');
                }  
            } elseif ($type == 'd') {
                $this->product_model->delete(['id' => $this->input->post('id')]);
            }elseif($type == 'edit'){
                $this->data['title'] = 'Edit E-commerce product';
                $this->data['content'] = 'ecom/ecom/edit';
                $this->data['type'] = 'ecom_product';
                $this->data['images'] = $this->db->where('product_id', $_GET['id'])->get('product_images')->result_array();
                // print_r($this->data['images']);
                $this->data['sub_categories']=$this->sub_category_model->order_by('id', 'DESC')->where('id', $this->input->get('id'))->get();
                $this->data['categories'] = $this->category_model->order_by('id', 'DESC')->get_all();
                $this->data['ecom_product'] = $this->product_model->where('id',$this->input->get('id'))->get();
               $this->_render_page($this->template, $this->data);
               //   if ($_FILES['files']['temp_name'] !== '') {
               //          $path = $_FILES['files']['temp_name'];
               //          $ext = pathinfo($path, PATHINFO_EXTENSION);
               //          $this->file_up("file", "ecom_product", $this->input->post('id'), '', 'no');
               //      }
               // if ($_FILES['file']['name'] !== '') {
               //          $path = $_FILES['file']['name'];
               //          $ext = pathinfo($path, PATHINFO_EXTENSION);
               //          $this->file_up("file", "product_nutrition", $this->input->post('id'), '', 'no');
               //      }
            }elseif($type == 'list'){
                $data = $this->sub_category_model->fields('id, name, desc')->where(['cat_id' =>$this->input->post('cat_id')])->get_all();
                echo json_encode($data);
            }
    }
    
    
}