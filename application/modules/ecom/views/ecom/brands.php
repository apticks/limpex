<!--Add Category And its list-->
<div class="row">
	<div class="col-12">
		<h4>Add Brands</h4>
		<form class="needs-validation" novalidate="" action="<?php echo base_url('ecom_brands/c');?>" method="post" enctype="multipart/form-data">
			<div class="card-header">
				<div class="form-row">
					<div class="form-group col-md-6">
						<label>Brand Name</label> <input type="text" name="name"
							required="" value="<?php echo set_value('name')?>"
							class="form-control" placeholder="Brand Name">
						<div class="invalid-feedback">New Category Name?</div>
						<?php echo form_error('name', '<div style="color:red">', '</div>');?>
					</div>
					<div class="form-group mb-0 col-md-6">
						<label>Description</label> <input type="text" name="desc"
							required="" value="<?php echo set_value('desc')?>"
							class="form-control" placeholder="Description">
						<div class="invalid-feedback">Give some Description</div>
						<?php echo form_error('desc', '<div style="color:red">', '</div>');?>
					</div>
					<div class="form-group col-md-6">
						<label>Upload Image</label> <input type="file" name="file"
							required="" value="<?php echo set_value('file')?>"
							class="form-control" onchange="readURL(this);"> <br> <img id="blah"
							src="#" alt="" style="width: 200px">
						<div class="invalid-feedback">Upload Image?</div>
						<?php echo form_error('file', '<div style="color:red">', '</div>');?>
					</div>
					<div class="form-group col-md-2">
						<button type="submit" name="upload" id="upload" value="Apply"
							class="btn btn-primary mt-27 ">Submit</button>
					</div>
				</div>
			</div>
		</form>

		<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of Brands</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>Brand Name</th>
									<th>Description</th>
									<th>Image</th>
									<th>Actions</th>

								</tr>
							</thead>
							<tbody>
							<?php if(!empty($ecom_brands)):?>
    							<?php  $sno = 1; foreach ($ecom_brands as $ecom_brands): ?>
    								<tr>
									<td><?php echo $sno++;?></td>
									<td><?php echo $ecom_brands['name'];?></td>
									<td><?php echo $ecom_brands['desc'];?></td>
									<td><img
										src="<?php echo base_url();?>uploads/ecom_brands_image/ecom_brands_<?php echo $ecom_brands['id'];?>.jpg?<?php echo time();?>"
										width="50px"></td>
									<td><a
										href="<?php echo base_url()?>ecom_brands/edit?id=<?php echo $ecom_brands['id']; ?>"
										class=" mr-2  " type="ecom_brands"> <i class="fas fa-pencil-alt"></i>
									</a> <a href="#" class="mr-2  text-danger "
										onClick="delete_record(<?php echo $ecom_brands['id'] ?>, 'ecom_brands/d')">
											<i class="far fa-trash-alt"></i>
									</a></td>

								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr>
									<th colspan='5'><h3>
											<center>No Brands</center>
										</h3></th>
								</tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>
</div>
