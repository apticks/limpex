<?php if($type == 'ecom_category'){?>


    <!--Edit Category -->
    <div class="row">
        <div class="col-12">
            <h4>Edit Category</h4>
            <form class="needs-validation" novalidate=""  action="<?php echo base_url('ecom_category/u');?>" method="post" enctype="multipart/form-data">
           <div class="card-header">
           <div class="form-row">
            <div class="form-group col-md-6">
                            <label>Category Name</label>
                            <input type="text" name="name" class="form-control" required="" value="<?php echo $category['name'];?>">
                            <div class="invalid-feedback">Enter Valid Category Name?</div>
                        </div>
                         <input type="hidden" name="id" value="<?php echo $category['id'] ; ?>">
                      <!-- 
                        <div class="form-group col-md-6">
                       <input type='file' name="file" onchange="readURL(this);" id="upload_form"/>
                       <?php //echo form_error('file', '<div style="color:red">', '</div>');?>
                          <img id="blah" src="<?php //echo base_url(); ?>uploads/ecom_category_image/ecom_category_<?php //echo $category['id']; ?>.jpg?<?php echo time();?>" width="180" height="180" alt="your image" />
                      </div> -->
                      <div class="form-group col-md-6">
                        <label>Upload Image</label>
                        <input type="file"  id="upload_form" name="file" class="form-control" onchange="readURL(this);" value="<?php echo base_url(); ?>uploads/ecom_category_image/ecom_category_<?php echo $category['id']; ?>.jpg?<?php echo time();?>" width="180" height="180" >
                        <br>
                        <img id="blah" src="<?php echo base_url(); ?>uploads/ecom_category_image/ecom_category_<?php echo $category['id']; ?>.jpg?<?php echo time();?>"<?php echo time();?> style="width: 200px;" />

                        <div class="invalid-feedback">Upload Image?</div>
                    </div>
                      <div class="col col-sm col-md" >
                      <label>Description</label> 
                      <textarea id="cat" name="desc" class="ckeditor" rows="10" data-sample-short reuired="" ><?php echo $category['desc'] ; ?></textarea>
                        <?php echo form_error('desc', '<div style="color:red">', '</div>');?>
                      </div>
                         <div class="form-group col-md-12">
                         <button type="submit" name="upload" id="upload" value="Apply" class="btn btn-primary mt-27 ">Update</button> 
<!--                              <button class="btn btn-primary mt-27 ">Update</button> -->
                           
                        </div>
           
            </div>
            </div>
            </form>

        </div>
    </div>
    <?php }elseif ($type == 'ecom_sub_category'){?>
      <!--sub_category Edit-->
        <div class="row">
            <div class="col-12">
                <h4>Edit sub category</h4>
                <form class="needs-validation" novalidate="" action="<?php echo base_url('ecom_sub_category/u');?>" method="post" enctype="multipart/form-data">
                    <div class="card-header">

                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label>sub_categories</label>
                                <input type="text" class="form-control" name="name" required="" value="<?php echo $sub_categories['name'];?>">
                                <div class="invalid-feedback">Enter valid  Name?</div>
                            </div>
                            <input type="hidden" name="id" value="<?php echo $sub_categories['id'] ; ?>">
                            </br>
                            <div class="form-group col-md-4">
                                <label>Category</label>
                                <!-- <input type="file" class="form-control" required="">-->
                                <select class="form-control" name="cat_id" required="">
                                    <option value="0" selected disabled>select</option>
                                    <?php foreach ($categories as $category):?>
                                      <option value="<?php echo $category['id'];?>" <?php echo ($category['id'] == $sub_categories['cat_id'])? 'selected': '';?>><?php echo $category['name']?></option>
                                        <?php endforeach;?>
                                </select>
                                <div class="invalid-feedback">Select Category Name?</div>
                            </div>
							
                            <div class="form-group col-md-4">
                                <label>Upload Image</label>
                                <input type="file" name="file" class="form-control" onchange="readURL(this);" value="<?php echo base_url(); ?>uploads/ecom_sub_category_image/ecom_sub_category_<?php echo $sub_categories['id']; ?>.jpg?<?php echo time();?>">
                            <br><img src="<?php echo base_url(); ?>uploads/ecom_sub_category_image/ecom_sub_category_<?php echo $sub_categories['id']; ?>.jpg?<?php echo time();?>" style="width: 200px;" />
                                
<!--                                 <input type="file" class="form-control" name="file"> -->
                                <div class="invalid-feedback">Upload Image?</div>
                            </div>

<!--                             <div class="form-group col-md-6"> -->
<!--                                 <img src="" width="80px"> -->
<!--                             </div> -->
                      <div class="col col-sm col-md" >
                      <label>Description</label> 
                      <textarea id="cat" name="desc" class="ckeditor" rows="10" data-sample-short reuired="" ><?php echo $sub_categories['desc'] ; ?></textarea>
                        <?php echo form_error('desc', '<div style="color:red">', '</div>');?>
                      </div>
                            <div class="form-group col-md-12">
                                <button class="btn btn-primary mt-27 ">Update</button>
                            </div>

                        </div>

                    </div>
                </form>

            </div>
        </div>
    <?php }elseif ($type == 'ecom_brands'){?>
      
     <?php }elseif ($type == 'product'){?>
     <div class="row">
	<div class="col-12">
		<h4>Product Page</h4>
		<form class="needs-validation" novalidate="" enctype="multipart/form-data"
			action="<?php echo base_url('product/u'); ?>" method="post" >
			<div class="card-header">
				<div class="form-row">
					<div class="form-group col-md-6">
						<label>Product Name</label> <input type="text" name="name"
							class="form-control" placeholder="Product Name" required="" value="<?php echo $product['name'];?>">
						<?php echo form_error('name','<div style="color:red">','</div>')?>
					</div>
 <input type="hidden" name="id" value="<?php echo $product['id'] ; ?>">
					
                    
                    <div class="form-group col-md-6">
						<label>Upload Image</label><input type="file" name="file" class="form-control" onchange="readURL(this);" value="<?php echo base_url(); ?>uploads/ecom_product_image/ecom_product_<?php echo $product['id']; ?>.jpg?<?php echo time();?>">
                            <br><img src="<?php echo base_url(); ?>uploads/ecom_product_image/ecom_product_<?php echo $product['id']; ?>.jpg?<?php echo time();?>"  style="width: 200px;"/>

                             <img id="blah" src="#" alt=""  style="width: 200px;"/>
						<div class="invalid-feedback">Upload Image?</div>
						<?php echo form_error('file', '<div style="color:red">', '</div>');?>
					</div>
                    
					<div class="form-group col-md-6">
                      <label class="control-label" for="product-category">Product Category</label>
                      <div class="">

                        <select class="form-control" name="cat_id" required="" id='category' onchange="category_changed()">
								<option value="0" selected disabled>--select--</option>
    							<?php foreach ($ecom_categories as $category):?>
    								<option value="<?php echo $category['id'];?>" <?php echo ($category['id'] == $product['category']['id'])? 'selected' : '';?>><?php echo $category['name']?></option>
    							<?php endforeach;?>
						</select>
                      </div>
                    </div>
                    
                    <div class="form-group col-md-6">
                      <label class="col-md-6 control-label" id="product_desc" for="textarea">Product Details</label>
                     
                      <?php echo form_error('desc','<div style="color:red">','</div>')?>
                    </div>
                    
                    <div class="form-group col-md-6">
                      <label class="control-label" for="product-sub-category">Product Sub Category</label>
                      <div class="">

                          <select class="form-control" name="sub_cat_id" id="sub_cat_id" onchange="sub_category_changed()">
								
                                 <option value="0" selected disabled>--select--</option>
    							<?php foreach ($ecom_sub_categories as $subcat):?>
    								<option value="<?php echo $subcat['id'];?>" <?php echo ($subcat['id'] == $product['sub_category']['id'])? 'selected' : '';?>><?php echo $subcat['name']?></option>
    							<?php endforeach;?>  

                        </select>
						
                      </div>
                    </div>
   
                    
                    
					<div class="form-group col-md-6">
                      <label class="control-label" for="product-brand">Product Brand</label>
                      <div class="">
                         <select class="form-control" name="brand_id"  id="brand_id" onchange="sub_category_changed()"> 
							<option value="0" selected disabled>--select--</option> 
    							<?php foreach ($ecom_brands as $brand):?>
    								<option value="<?php echo $brand['id'];?>" <?php echo ($brand['id'] == $product['brand']['id'])? 'selected' : '';?>><?php echo $brand['name']?></option>
    							<?php endforeach;?>
				</select> 
                      </div>
                    </div>
					
                    <!-- Text input-->
                    <div class="form-group col-md-2">
                      <label class="control-label" for="mrp-price">Qty</label>  
                      <div class="">
                      <input id="mrp-price" name="qty" type="text" placeholder="Quantity" class="form-control input-md" value="<?php echo $product['qty'];?>">
                   
                      </div>
                    </div>
                    
                    <!-- Text input-->
                    <div class="form-group col-md-2">
                      <label class="control-label" for="mrp-price">Units</label>  
                      <div class="">
                      <input id="mrp-price" name="units" type="text" placeholder="Units" class="form-control input-md" value="<?php echo $product['units'];?>">
                   
                      </div>
                    </div>
                    
                    <div class="form-group col-md-6">
                      <label class="col-md-6 control-label" for="marp-price">MRP Price</label>  
                      <div class="">
                      <input id="marp-price" name="mrp" type="text" placeholder="Mrp Price" class="form-control input-md"  value="<?php echo $product['mrp'];?>">
                    
                      </div>
                    </div>
                    
                    <div class="form-group col-md-6">
                      <label class="col-md-6 control-label" for="offered-price">Offer Price</label>  
                      <div class="">
                      <input id="offered-price" name="offer_price" type="text" placeholder="Offer Price" class="form-control input-md"  value="<?php echo $product['offer_price'];?>">
                    
                      </div>
                    </div>
                    
                    
                    <div class="form-group col-md-6 ">
                      <label class="control-label" for="product-gst-tax">Product GST Tax</label>  
                      <div class="">
                      <input id="product-gst-tax" name="gst" type="text" placeholder="0" class="form-control input-md"  value="<?php echo $product['gst'];?>">
                      </div>
                    </div>
                    
					<div class="form-group col-md-12">
                        <button type="submit" name="upload" id="upload" value="Apply" class="btn btn-success mt-27">Update</button>

        
					</div>
				</div>
			</div>
		</form>
    </div>
</div>
 <?php }elseif ($type == 'ecom_sub_sub_category'){?>

     <div class="row">
	<div class="col-12">
		<h4>Add E-Commerce Sub Sub_Category</h4>
		<form class="needs-validation" novalidate=""
			action="<?php echo base_url('ecom_sub_sub_category/u');?>" method="post"
			enctype="multipart/form-data">
			<div class="card-header">

				<div class="form-row">
					<div class="form-group col-md-4">
						<label>Sub Sub_Category Name</label> <input type="text"
							class="form-control" name="name" required="" value="<?php echo $ecom_sub_sub_categories['name'];?>">
						<div class="invalid-feedback">New Sub_Category Name?</div>
						<?php echo form_error('name','<div style="color:red">','</div>')?>
					</div>
					 <input type="hidden" name="id" value="<?php echo $ecom_sub_sub_categories['id'] ; ?>">
					<div class="form-group col-md-2">
						<label>Category</label>
						<!-- <input type="file" class="form-control" required="">-->
						<select class="form-control" name="cat_id" required="" id="category" onchange="category_changedsub()">
								 <option value="0" selected disabled>--select--</option>

                                                    <?php foreach ($categories as $cat):?>
                                                        <option value="<?php echo $cat['id'];?>" <?php echo ($cat['id'] == $ecom_sub_sub_categories['cat_id'])? 'selected': '';?>><?php echo $cat['name']?></option>
                                                        <?php echo $cat['name']?>
                                                        </option>
                                                    <?php endforeach;?>
                            
						</select>
						<div class="invalid-feedback">New Category Name?</div>
						<?php echo form_error('cat_id','<div style="color:red>"','</div>');?>
					</div>
 	<div class="form-group col-md-2">
						<label>Sub Category</label>
						<!-- <input type="file" class="form-control" required="">-->
						<select class="form-control" name="sub_cat_id" required="" id="subcat">
								<option value="0" selected disabled>--select--</option>
    							 <?php foreach ($sub_categories as $cat):?>
                                                        <option value="<?php echo $cat['id'];?>" <?php echo ($cat['id'] == $ecom_sub_sub_categories['sub_cat_id'])? 'selected': '';?>><?php echo $cat['name']?></option>
                                                        <?php echo $cat['name']?>
                                                        </option>
                                                    <?php endforeach;?>
						</select>
						<div class="invalid-feedback">New Category Name?</div>
						<?php echo form_error('sub_cat_id','<div style="color:red>"','</div>');?>
					</div>

					<div class="form-group mb-0 col-md-4">
						<label>Description</label> <textarea type="text" class="form-control"
							name="desc" id="product_desc" data-sample-short><?php echo $ecom_sub_sub_categories['desc'];?></textarea>
						<div class="invalid-feedback">Give some Description</div>
						
					</div>
						
					<div class="form-group col-md-2">
						<label>Upload Image</label> 
						<input type="file" name="file" class="form-control" onchange="readURL(this);" value="<?php echo base_url(); ?>uploads/ecom_sub_sub_category_image/ecom_sub_sub_category_<?php echo $ecom_sub_sub_categories['id']; ?>.jpg?<?php echo time();?>">
                            <br><img src="<?php echo base_url(); ?>uploads/ecom_sub_sub_category_image/ecom_sub_sub_category_<?php echo $ecom_sub_sub_categories['id']; ?>.jpg?<?php echo time();?>"  style="width: 200px;"/>

                             <img id="blah" src="#" alt="" style="width: 200px;"/>
						<div class="invalid-feedback">Upload Image?</div>
						<?php echo form_error('file', '<div style="color:red">', '</div>');?>
					</div>

					<div class="form-group col-md-12">
                        <button type="submit" name="upload" id="upload" value="Apply" class="btn btn-success mt-27">Update</button>
					</div>


				</div>


			</div>
		</form>
     
  <?php } elseif ($type == 'ecom_product'){?>
  <style>
  th, td {
  padding: 15px;
}
  </style>
  <div class="row">
            <div class="col-12">
                <h4>Edit product</h4>
                <form class="needs-validation" novalidate="" action="<?php echo base_url('ecom_product/u');?>" method="post" enctype="multipart/form-data">
                    <div class="card-header">

                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label>Product</label>
                                <input type="text" class="form-control" name="name" required="" value="<?php echo $ecom_product['name'];?>">
                                <div class="invalid-feedback">Enter valid  Name?</div>
                            </div>
                            <input type="hidden" name="id" value="<?php echo $ecom_product['id'] ; ?>">
                            </br>
                            <div class="form-group col-md-4">
                                <label>Category</label>
                                <!-- <input type="file" class="form-control" required="">-->
                                <select class="form-control" name="cat_id" required="" id="pcategory" onchange="category_changedp()">
                                    <option value="0" selected disabled>select</option>
                                    <?php foreach ($categories as $category):?>
                                      <option value="<?php echo $category['id'];?>" <?php echo ($category['id'] == $ecom_product['cat_id'])? 'selected': '';?>><?php echo $category['name']?></option>
                                        <?php endforeach;?>
                                </select>
                                <div class="invalid-feedback">Select Category Name?</div>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Sub Category</label>
                                <!-- <input type="file" class="form-control" required="">-->
                                <!-- <select class="form-control" name="cat_id" required="">
                                    <option value="0" selected disabled>select</option>
                                    <?php foreach ($categories as $category):?>
                                      <option value="<?php echo $category['id'];?>" <?php echo ($category['id'] == $sub_categories['cat_id'])? 'selected': '';?>><?php echo $category['name']?></option>
                                        <?php endforeach;?>
                                </select> -->
                                <select class="form-control" name="sub_cat_id" required="" id="psub_cat_id" onchange="product_changed()">
                                    <option value="0" selected disabled>select</option>
                                    <?php foreach ($sub_categories as $category):?>
                                      <option value="<?php echo $category['id'];?>" <?php echo ($category['id'] == $ecom_product['sub_cat_id'])? 'selected': '';?>><?php echo $category['name']?></option>
                                        <?php endforeach;?>
                                </select>
                                <div class="invalid-feedback">Select Category Name?</div>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Upload Image</label>
                                <input type="file" name="files[]" class="form-control" multiple onchange="readURL(this);" value="<?php echo base_url(); ?>uploads/ecom_product_image/ecom_product_<?php echo $ecom_product['id']; ?>.jpg?<?php echo time();?>">
                            <br><img src="<?php echo base_url(); ?>uploads/ecom_product_image/ecom_product_<?php echo $ecom_product['id']; ?>.jpg?<?php echo time();?>" style="width: 200px;" />
                                
                             <!-- <input type="file" class="form-control" name="file"> -->
                                <div class="invalid-feedback">Upload Image?</div>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Nutrition Component Image</label>
                                <input type="file" name="file" class="form-control" onchange="readURL(this);" value="<?php echo base_url(); ?>uploads/product_nutrition_image/product_nutrition_<?php echo $ecom_product['id']; ?>.jpg?<?php echo time();?>">
                            <br><img src="<?php echo base_url(); ?>uploads/product_nutrition_image/product_nutrition_<?php echo $ecom_product['id']; ?>.jpg?<?php echo time();?>" style="width: 200px;" />
                                
                             <!-- <input type="file" class="form-control" name="file"> -->
                                <div class="invalid-feedback">Upload Image?</div>
                            </div>

<!--                             <div class="form-group col-md-6"> -->
<!--                                 <img src="" width="80px"> -->
<!--                             </div> -->
                      
                      <div class="form-group  col-md-12" >
          <label>Description</label> 
                  <textarea id="product_desc" name="desc" class="ckeditor" rows="10" data-sample-short><?php echo $ecom_product['desc'] ; ?></textarea>
                    <?php echo form_error('desc', '<div style="color:red">', '</div>');?>
              </div>
              <div class="form-group  col-md-12" >
          <label>Varieties</label> 
                  <textarea id="varieties_desc" name="varieties_desc" class="ckeditor" rows="10" data-sample-short><?php echo $ecom_product['varieties_desc'] ; ?></textarea>
                    <?php echo form_error('varieties_desc', '<div style="color:red">', '</div>');?>
              </div>
              <div class="form-group  col-md-12" >
          <label>Season</label> 
                  <textarea id="season_desc" name="season_desc" class="ckeditor" rows="10" data-sample-short><?php echo $ecom_product['season_desc'] ; ?></textarea>
                    <?php echo form_error('desc', '<div style="color:red">', '</div>');?>
              </div>
              <div class="form-group  col-md-12" >
          <label>Management</label> 
                  <textarea id="management_desc" name="management_desc" class="ckeditor" rows="10" data-sample-short><?php echo $ecom_product['management_desc'] ; ?></textarea>
                    <?php echo form_error('desc', '<div style="color:red">', '</div>');?>
              </div>
              <div class="form-group  col-md-12" >
          <label>Irrigation/Cultivation</label> 
                  <textarea id="cultivation_desc" name="cultivation_desc" class="ckeditor" rows="10" data-sample-short><?php echo $ecom_product['cultivation_desc'] ; ?></textarea>
                    <?php echo form_error('desc', '<div style="color:red">', '</div>');?>
              </div>
                            <div class="form-group col-md-12">
                                <button class="btn btn-primary mt-27 ">Update</button>
                            </div>

                        </div>

                    </div>
                </form>

            </div>
            <div class="container">
                <fieldset class="form-group">
                    <a href="javascript:void(0)" onclick="$('#pro-image').click()">Upload Image</a>
                    <input type="file" id="pro-image" name="pro-image" style="display: none;" class="form-control" multiple>
                </fieldset>
                <div class="preview-images-zone">
                	<?php foreach($images as $image):?>
                    <div class="preview-image preview-show-<?php echo $image['id'];?>">
                        <div class="image-cancel" data-no="<?php echo $image['id'];?>">x</div>
                        <div class="image-zone"><img id="pro-img-<?php echo $image['id'];?>" src="<?php echo base_url();?>/uploads/ecom_product_image/ecom_product_<?php echo $image['id'];?>.jpg"></div>
                        <div class="tools-edit-image"><a href="javascript:void(0)" data-no="<?php echo $image['id'];?>" class="btn btn-light btn-edit-image">edit</a></div>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
  <?php }?>
  
                        
                    