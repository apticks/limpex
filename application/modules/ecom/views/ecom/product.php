<!--Add Product And its list-->
<div class="row">
	<div class="col-12">
		<h4>Add  Product</h4>
		<form class="needs-validation" novalidate=""
			action="<?php echo base_url('ecom_product/c');?>" method="post"
			enctype="multipart/form-data">
			<div class="card-header">

				<div class="form-row">
					<div class="form-group col-md-4">
						<label>Product Name</label>
						<input type="text" class="form-control" name="name" required="" value="<?php echo set_value('name')?>" placeholder="Product Name">
						<div class="invalid-feedback">New Product Name?</div>
						<?php echo form_error('name','<div style="color:red">','</div>')?>
					</div>

					<div class="form-group col-md-3">
						<label>Category</label>
						<!-- <input type="file" class="form-control" required="">-->
						<select class="form-control" name="cat_id" required="" id="pcategory" onchange="category_changedp()"> 
								<option value="0" selected disabled>--select--</option>
    							<?php foreach ($ecom_categories as $category):?>
    								<option value="<?php echo $category['id'];?>"><?php echo $category['name']?></option>
    							<?php endforeach;?>
						</select>
						<div class="invalid-feedback">New Category Name?</div>
						<?php echo form_error('cat_id','<div style="color:red>"','</div>');?>
					</div>
					<div class="form-group col-md-3">
						<label>Sub Category</label>
						<!-- <input type="file" class="form-control" required="">-->
						<select class="form-control" name="sub_cat_id" id="psub_cat_id" >
								<option value="0" selected disabled>--select--</option>
    							<?php foreach ($ecom_sub_categories as $sub_category):?>
    								<option value="<?php echo $sub_category['id'];?>"><?php echo $sub_category['name']?></option>
    							<?php endforeach;?>
						</select>
						<div class="invalid-feedback">New Sub Category Name?</div>
						<?php echo form_error('cat_id','<div style="color:red>"','</div>');?>
					</div>
 

					<div class="form-group col-md-3">
						<label>Upload Image</label> 
						<input type="file" name="files[]"  value="<?php echo set_value('file')?>"
							class="form-control" multiple onchange="readURL(this);">
						<img id="blah" src="#" alt="" style="width: 200px"> 
						<div class="invalid-feedback">Upload Image?</div>
						<?php echo form_error('file', '<div style="color:red">', '</div>');?>
					</div>
					<div class="form-group col-md-3">
						<label>Upload Nutritional Component Image</label> <input type="file" name="file"
							 value="<?php echo set_value('file')?>"
							class="form-control" onchange="readURL(this);"> <br> <img id="blah"
							src="#" alt="" style="width: 200px">
						<div class="invalid-feedback">Upload Image?</div>
						<?php echo form_error('file', '<div style="color:red">', '</div>');?>
					</div>
					<div class="form-group  col-md-12" >
					<label>Description</label> 
          				<textarea id="product_desc" name="desc" class="ckeditor" rows="10" data-sample-short></textarea>
          					<?php echo form_error('desc', '<div style="color:red">', '</div>');?>
        			</div>
        			<div class="form-group  col-md-12" >
					<label>Varieties</label> 
          				<textarea id="varieties_desc" name="varieties_desc" class="ckeditor" rows="10" data-sample-short></textarea>
          					<?php echo form_error('varieties_desc', '<div style="color:red">', '</div>');?>
        			</div>
        			<div class="form-group  col-md-12" >
					<label>Season</label> 
          				<textarea id="season_desc" name="season_desc" class="ckeditor" rows="10" data-sample-short></textarea>
          					<?php echo form_error('season_desc', '<div style="color:red">', '</div>');?>
        			</div>
        			<div class="form-group  col-md-12" >
					<label>Management</label> 
          				<textarea id="management_desc" name="management_desc" class="ckeditor" rows="10" data-sample-short></textarea>
          					<?php echo form_error('management_desc', '<div style="color:red">', '</div>');?>
        			</div>
        			<div class="form-group  col-md-12" >
					<label>Irrigation/Cultivation</label> 
          				<textarea id="cultivation_desc" name="cultivation_desc" class="ckeditor" rows="10" data-sample-short></textarea>
          					<?php echo form_error('cultivation_desc', '<div style="color:red">', '</div>');?>
        			</div>
					<div class="form-group col-md-2">

						<button class="btn btn-primary mt-27 ">Submit</button>
					</div>


				</div>


			</div>
		</form>

		<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of Products</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Id</th>
									<th>Name</th>
									<th>Category</th>
									<th>Sub Category</th>
									<th>Image</th>
									<th>Nutritional Component Image</th>
									<th>Actions</th>

								</tr>
							</thead>
							<tbody>
								<?php if(!empty($ecom_product)):?> 
    							<?php $sno = 1; foreach ($ecom_product as $product):?>
    								<tr>
    									<td><?php echo $sno++;?></td>
    									<td><?php echo $product['name'];?></td>
    									<td><?php foreach ($ecom_categories as $category):?>
    										<?php echo ($category['id'] == $product['cat_id'])? $category['name']:'';?>
    									<?php endforeach;?></td>
    									<td><?php foreach ($ecom_sub_categories as $category):?>
    										<?php echo ($category['id'] == $product['sub_cat_id'])? $category['name']:'';?>
    									<?php endforeach;?></td>
    									<td><?php $images = $this->db->where('product_id', $product['id'])->get('product_images')->result_array();
    									foreach ($images as $image){
    									?>
        									<img
        										src="<?php echo base_url();?>uploads/ecom_product_image/ecom_product_<?php echo $image['id'];?>.jpg?<?php echo time();?>"
        										width="50px">
    									<?php }?>
    									</td>
    										<td><img
										src="<?php echo base_url();?>uploads/product_nutrition_image/product_nutrition_<?php echo $product['id'];?>.jpg?<?php echo time();?>"
										class="img-thumb" style="width: 100px;height: 63px;"></td>
    									<td><a href="<?php echo base_url()?>ecom_product/edit?id=<?php echo $product['id'];?>" class=" mr-2  "  > <i class="fas fa-pencil-alt"></i>
    									</a> <a href="#" class="mr-2  text-danger " onClick="delete_record(<?php echo $product['id'] ?>, 'ecom_product/d')"> <i
    											class="far fa-trash-alt"></i>
    									</a></td>
    
    								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr ><th colspan='6'><h3><center>No Sub_Category</center></h3></th></tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>
</div>

