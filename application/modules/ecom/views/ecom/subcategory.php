<!--Add Sub_Category And its list-->
<div class="row">
	<div class="col-12">
		<h4>Add  Sub_Category</h4>
		<form class="needs-validation" novalidate=""
			action="<?php echo base_url('ecom_sub_category/c');?>" method="post"
			enctype="multipart/form-data">
			<div class="card-header">

				<div class="form-row">
					<div class="form-group col-md-4">
						<label>Sub_Category Name</label> <input type="text"
							class="form-control" name="name" required="" value="<?php echo set_value('name')?>" placeholder="Sub Category Name">
						<div class="invalid-feedback">New Sub_Category Name?</div>
						<?php echo form_error('name','<div style="color:red">','</div>')?>
					</div>

					<div class="form-group col-md-3">
						<label>Category</label>
						<!-- <input type="file" class="form-control" required="">-->
						<select class="form-control" name="cat_id" required="" >
								<option value="0" selected disabled>--select--</option>
    							<?php foreach ($ecom_categories as $category):?>
    								<option value="<?php echo $category['id'];?>"><?php echo $category['name']?></option>
    							<?php endforeach;?>
						</select>
						<div class="invalid-feedback">New Category Name?</div>
						<?php echo form_error('cat_id','<div style="color:red>"','</div>');?>
					</div>


					<div class="form-group col-md-3">
						<label>Upload Image</label> 
						
						<input type="file" name="file"  value="<?php echo set_value('file')?>"
							class="form-control" onchange="readURL(this);">
						<img id="blah" src="#" alt="" style="width: 200px"> 
						<div class="invalid-feedback">Upload Image?</div>
						<?php echo form_error('file', '<div style="color:red">', '</div>');?>
					</div>
					<div class="form-group  col-md-12" >
					<label>Description</label> 
          				<textarea id="sub_cat" name="desc" class="ckeditor" rows="10" data-sample-short></textarea>
          					<?php echo form_error('desc', '<div style="color:red">', '</div>');?>
        			</div>
					<div class="form-group col-md-2">

						<button class="btn btn-primary mt-27 ">Submit</button>
					</div>


				</div>


			</div>
		</form>

		<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of Sub_Categories</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Id</th>
									<th>Sub_Category Name</th>
									<th>Category</th>
									<th>Desc</th>
									<th>Image</th>
									<th>Actions</th>

								</tr>
							</thead>
							<tbody>
								<?php if(!empty($ecom_sub_categories)):?> 
    							<?php $sno = 1; foreach ($ecom_sub_categories as $ecom_sub_cat):?>
    								<tr>
    									<td><?php echo $sno++;?></td>
    									<td><?php echo $ecom_sub_cat['name'];?></td>
    									<td><?php foreach ($ecom_categories as $category):?>
    										<?php echo ($category['id'] == $ecom_sub_cat['cat_id'])? $category['name']:'';?>
    									<?php endforeach;?></td>
    									<td><?php echo $ecom_sub_cat['desc'];?></td>
    									<td><img
    										src="<?php echo base_url();?>uploads/ecom_sub_category_image/ecom_sub_category_<?php echo $ecom_sub_cat['id'];?>.jpg?<?php echo time();?>"
    										width="50px"></td>
    									<td><a href="<?php echo base_url()?>ecom_sub_category/edit?id=<?php echo $ecom_sub_cat['id'];?>" class=" mr-2  "  > <i class="fas fa-pencil-alt"></i>
    									</a> <a href="#" class="mr-2  text-danger " onClick="delete_record(<?php echo $ecom_sub_cat['id'] ?>, 'ecom_sub_category/d')"> <i
    											class="far fa-trash-alt"></i>
    									</a></td>
    
    								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr ><th colspan='6'><h3><center>No Sub_Category</center></h3></th></tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>
</div>

