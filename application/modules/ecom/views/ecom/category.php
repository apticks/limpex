<!--Add Category And its list-->
<div class="row">
	<div class="col-12">
		<h4>Add Category</h4>
		<form class="needs-validation" novalidate="" action="<?php echo base_url('ecom_category/c');?>" method="post" enctype="multipart/form-data">
			<div class="card-header">
				<div class="form-row">
					<div class="form-group col-md-6">
						<label>Category Name</label> <input type="text" name="name"
							required="" value="<?php echo set_value('name')?>"
							class="form-control" placeholder="Category Name">
						<div class="invalid-feedback">New Category Name?</div>
						<?php echo form_error('name', '<div style="color:red">', '</div>');?>
					</div>
					<div class="form-group col-md-6">
						<label>Upload Image</label> <input type="file" name="file"
							 value="<?php echo set_value('file')?>"
							class="form-control" onchange="readURL(this);" > <br> <img id="blah"
							src="#" alt="" style="width: 200px">
						<div class="invalid-feedback">Upload Image?</div>
						<?php echo form_error('file', '<div style="color:red">', '</div>');?>
					</div>
					<div class="col col-sm col-md" >
					<label>Description</label> 
          	<textarea id="cat" name="desc" class="ckeditor" rows="10" data-sample-short></textarea>
          	<?php echo form_error('desc', '<div style="color:red">', '</div>');?>
        </div>
					
					<div class="form-group col-md-2">
						<button type="submit" name="upload" id="upload" value="Apply" 
							class="btn btn-primary mt-27 ">Submit</button>
					</div>
				</div>
			</div>
		</form>

		<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of  Categories</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>Category Name</th>
									<th>Description</th>
									<th>Image</th>
									<th>Actions</th>

								</tr>
							</thead>
							<!-- <tbody>
							<?php if(!empty($ecom_categories_brands)):?>
    							<?php  $sno = 1; foreach ($ecom_categories_brands as $ecom_category): ?>
    								<tr>
									<td><?php echo $sno++;?></td>
									<td><?php echo $ecom_category['name'];?></td>
									<td>
										<ul>
										<?php if(isset($ecom_category['brands'])){ foreach ($ecom_category['brands'] as $brands):?>
											<li><?php echo $brands['name'];?></li>
										<?php endforeach;}?>
										</ul>
									</td> -
									<td><?php echo $ecom_category['name'];?></td>
									<td><img
										src="<?php echo base_url();?>uploads/ecom_category_image/ecom_category_<?php echo $ecom_category['id'];?>.jpg"
										width="50px"></td>
									<td><a
										href="<?php echo base_url()?>ecom_category/edit?id=<?php echo $ecom_category['id']; ?>"
										class=" mr-2  " type="ecom_category">  <i class="fas fa-pencil-alt"></i>
 									</a><!--  <a href="#" class="mr-2  text-danger " 
										onClick="delete_record(<?php //echo $ecom_category['id'] ?>, 'ecom_category/d')">
											<i class="far fa-trash-alt"></i> 
									</a></td>

								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr>
									<th colspan='5'><h3>
											<center>No Categories</center>
										</h3></th>
								</tr>
							<?php endif;?>
							</tbody> -->
							<tbody>
								<?php if(!empty($ecom_categories)):?>
    							<?php $sno = 1; foreach ($ecom_categories as $category):?>
    								<tr>
    									<td><?php echo $sno++;?></td>
    									<td><?php echo $category['name'];?></td>
    									<td><?php echo $category['desc'];?></td>
    									<td><img
										src="<?php echo base_url();?>uploads/ecom_category_image/ecom_category_<?php echo $category['id'];?>.jpg?<?php echo time();?>"
										class="img-thumb" style="
    width: 100px;
    height: 63px;
"></td>
    									<td><a href="<?php echo base_url()?>ecom_category/edit?id=<?php echo $category['id'];?>" class=" mr-2  " type="category" > <i class="fas fa-pencil-alt" ></i>
    									</a> <a href="#" class="mr-2  text-danger " onClick="delete_record(<?php echo $category['id'] ?>, 'ecom_category/d')"> <i
    											class="far fa-trash-alt"></i>
    									</a></td>
    								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr ><th colspan='5'><h3><center>No Districts</center></h3></th></tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>
</div>
