<div class="row">
	<div class="col-12">
		<h4>Product Page</h4>
		<form class="needs-validation" novalidate=""
			action="<?php echo base_url('product/c'); ?>" method="post"
			enctype="multipart/form-data">
			<div class="card-header">
				<div class="form-row">
					<div class="form-group col-md-6">
						<label>Product Name</label> <input type="text" name="name"
							class="form-control" placeholder="Product Name" required="">
						<?php echo form_error('name','<div style="color:red">','</div>')?>
						
					</div>

					<div class="form-group col-md-6">
						<label>Upload Image</label> <input type="file" name="files[]"
							required="" value=""
							class="form-control" onchange="readURL(this);" multiple>
						<div class="invalid-feedback">Upload Image?</div>
						<?php echo form_error('file', '<div style="color:red">', '</div>');?>
					</div>

					<div class="form-group col-md-6">
						<label class="control-label" for="product-category">Product
							Category</label>
						<div class="">
							<select class="form-control" name="cat_id" required=""
								id='category' onchange="category_changed()">
								<option value="0" selected disabled>--select--</option>
    							<?php foreach ($ecom_categories as $category):?>
    								<option value="<?php echo $category['id'];?>"><?php echo $category['name']?></option>
    							<?php endforeach;?>
						</select>
						</div>
					</div>

					<div class="form-group col-md-6">
						<label class="col-md-6 control-label" for="textarea">Product
							Details</label>
						<div class="">
							<textarea class="form-control" id="product_desc" name="desc"
								data-sample-short>Product Details</textarea>
						</div>

					</div>

					<div class="form-group col-md-3">
						<label class="control-label" for="product-sub-category">Product
							Sub Category</label>
						<div class="">

							<select class="form-control" name="sub_cat_id" id="sub_cat_id"
								onchange="sub_category_changed()" required="">
								<option value="0" selected disabled>--select--</option>
                               <?php foreach ($ecom_sub_categories as $subcat):?>
                                   <option
									value="<?php echo $subcat['cat_id'];?>"><?php echo $subcat['name']?></option>
                               <?php endforeach;?>
                        </select>

						</div>
					</div>



					<div class="form-group col-md-3">
						<label class="control-label" for="product-brand">Product Sub Sub
							Category</label>
						<div class="">
							<select class="form-control" name="sub_id" id="sub_id"
								required="">
								<option value="0" selected disabled>--select--</option>
    							<?php foreach ($brands as $brand):?>
    								<option value="<?php echo $brand['id'];?>"><?php echo $brand['name']?></option>
    							<?php endforeach;?>
				</select>
						</div>
					</div>
					<div class="form-group col-md-3">
						<label class="control-label" for="product-brand">Product Brand</label>
						<div class="">
							<select class="form-control" name="brand_id" id="brand_id"
								required="">
								<option value="0" selected disabled>--select--</option>
    							<?php foreach ($brands as $brand):?>
    								<option value="<?php echo $brand['id'];?>"><?php echo $brand['name']?></option>
    							<?php endforeach;?>
				</select>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group col-md-2">
						<label class="control-label" for="mrp-price">Qty</label>
						<div class="">
							<input id="mrp-price" name="qty" type="text"
								placeholder="Quantity" class="form-control input-md">

						</div>
					</div>

					<!-- Text input-->
					<div class="form-group col-md-2">
						<label class="control-label" for="mrp-price">Units</label>
						<div class="">
							<input id="mrp-price" name="units" type="text"
								placeholder="Units" class="form-control input-md">

						</div>
					</div>
					<div class="form-group col-md-6">
						<label class="col-md-6 control-label" for="marp-price">MRP Price</label>
						<div class="">
							<input id="marp-price" name="mrp" type="text"
								placeholder="Mrp Price" class="form-control input-md">

						</div>
					</div>

					<div class="form-group col-md-6">
						<label class="col-md-6 control-label" for="offered-price">Offer
							Price</label>
						<div class="">
							<input id="offered-price" name="offer_price" type="text"
								placeholder="Offer Price" class="form-control input-md">

						</div>
					</div>
					<div class="form-group col-md-6 ">
						<label class="control-label" for="product-gst-tax">Product GST Tax</label>
						<div class="">
							<input id="product-gst-tax" name="gst" type="text"
								placeholder="0" class="form-control input-md">
						</div>
					</div>

					<div class="form-group col-md-12">
						<button id="product-save" name="product-save"
							class="btn btn-success mt-27">Sumbit</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>


