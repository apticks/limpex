<!--Add Category And its list-->
<div class="row">
	<div class="col-12">
		<h4>Add Blog</h4>
		<form class="needs-validation" novalidate="" action="<?php echo base_url('blog/u');?>" method="post" enctype="multipart/form-data">
			<div class="card-header">
				<div class="form-row"><input type="hidden" name="id" value="<?php echo $blog['id'] ; ?>">
                            </br>
					<div class="form-group col-md-4">
						<label>Category</label>
						<!-- <input type="file" class="form-control" required="">-->
						<select class="form-control" name="cat_id" required="" id="pcategory" onchange="category_changedp()">
                                    <option value="0" selected disabled>select</option>
                                    <?php foreach ($categories as $category):?>
                                      <option value="<?php echo $category['id'];?>" <?php echo ($category['id'] == $blog['cat_id'])? 'selected': '';?>><?php echo $category['name']?></option>
                                        <?php endforeach;?>
                                </select>
						<div class="invalid-feedback">New Category Name?</div>
						<?php echo form_error('cat_id','<div style="color:red>"','</div>');?>
					</div>
                    
                            
					<div class="form-group col-md-4">
						<label>Sub Category</label>
						<!-- <input type="file" class="form-control" required="">-->
						<select class="form-control" name="sub_cat_id" required="" id="psub_cat_id" onchange="product_changed()">
                                    <option value="0" selected disabled>select</option>
                                    <?php foreach ($sub_categories as $category):?>
                                      <option value="<?php echo $category['id'];?>" <?php echo ($category['id'] == $blog['sub_cat_id'])? 'selected': '';?>><?php echo $category['name']?></option>
                                        <?php endforeach;?>
                                </select>
						<div class="invalid-feedback">New Category Name?</div>
						<?php echo form_error('cat_id','<div style="color:red>"','</div>');?>
					</div>
					<div class="form-group col-md-4">
						<label>Product</label>
						<!-- <input type="file" class="form-control" required="">-->
						<select class="form-control" name="product_id" required="" id="product_id" >
                                    <option value="0" selected disabled>select</option>
                                    <?php foreach ($product as $category):?>
                                      <option value="<?php echo $category['id'];?>" <?php echo ($category['id'] == $blog['product_id'])? 'selected': '';?>><?php echo $category['name']?></option>
                                        <?php endforeach;?>
                                </select>
						<div class="invalid-feedback">New Category Name?</div>
						<?php echo form_error('cat_id','<div style="color:red>"','</div>');?>
					</div>
					<div class="form-group col-md-6">
						<label>Title</label> <input type="text"
							class="form-control" name="title" required="" value="<?php echo $blog['title'];?>" placeholder="Title">
						<div class="invalid-feedback">New Sub_Category Name?</div>
						<?php echo form_error('name','<div style="color:red">','</div>')?>
					</div>
					<div class="form-group col-md-6">
						<label>Author</label> <input type="text"
							class="form-control" name="author" required="" value="<?php echo $blog['author'];?>" placeholder="Author">
						<div class="invalid-feedback">New Sub_Category Name?</div>
						<?php echo form_error('name','<div style="color:red">','</div>')?>
					</div>
					<div class="form-group col-md-12">
						<label>URL</label> <input type="text"
							class="form-control" name="url"value="<?php echo $blog['url'];?>" placeholder="URL">
						<div class="invalid-feedback">New Sub_Category Name?</div>
						<?php echo form_error('url','<div style="color:red">','</div>')?>
					</div>
					
						 <div class="form-group col-md-12">
                                <label>Upload Image</label>
                                <input type="file" name="file" class="form-control" onchange="readURL(this);" value="<?php echo base_url(); ?>uploads/blog_image/blog_<?php echo $blog['id']; ?>.jpg?<?php echo time();?>">
                            <br><img src="<?php echo base_url(); ?>uploads/blog_image/blog_<?php echo $blog['id']; ?>.jpg?<?php echo time();?>" style="width: 200px;" />
                                
<!--                                 <input type="file" class="form-control" name="file"> -->
                                <div class="invalid-feedback">Upload Image?</div>
                            </div>
					
					<div class="col col-sm col-md" >
					<label>Description</label> 
          	<textarea id="cat" name="desc" class="ckeditor" rows="10" data-sample-short><?php echo $blog['desc'] ; ?></textarea>
          	<?php echo form_error('desc', '<div style="color:red">', '</div>');?>
        </div>
					
					<div class="form-group col-md-2">
						<button type="submit" name="upload" id="upload" value="Apply" 
							class="btn btn-primary mt-27 ">Submit</button>
					</div>
				</div>
			</div>
		</form>

		

	</div>
</div>
