<!--Add Category And its list-->
<div class="row">
	<div class="col-12">
		<h4>Add Blog</h4>
		<form class="needs-validation" novalidate="" action="<?php echo base_url('blog_admin/c');?>" method="post" enctype="multipart/form-data">
			<div class="card-header">
				<div class="form-row">
					<div class="form-group col-md-4">
						<label>Category</label>
						<!-- <input type="file" class="form-control" required="">-->
						<select class="form-control" name="cat_id" required="" id="pcategory" onchange="category_changedp()">
								<option value="0" selected disabled>--select--</option>
    							<?php foreach ($ecom_categories as $category):?>
    								<option value="<?php echo $category['id'];?>"><?php echo $category['name']?></option>
    							<?php endforeach;?>
						</select>
						<div class="invalid-feedback">New Category Name?</div>
						<?php echo form_error('cat_id','<div style="color:red>"','</div>');?>
					</div>
					<div class="form-group col-md-4">
						<label>Sub Category</label>
						<!-- <input type="file" class="form-control" required="">-->
						<select class="form-control" name="sub_cat_id" required="" id="psub_cat_id" onchange="product_changed()">
								<option value="0" selected disabled>--select--</option>
    							<?php foreach ($ecom_sub_categories as $category):?>
    								<option value="<?php echo $category['id'];?>"><?php echo $category['name']?></option>
    							<?php endforeach;?>
						</select>
						<div class="invalid-feedback">New Category Name?</div>
						<?php echo form_error('cat_id','<div style="color:red>"','</div>');?>
					</div>
					<div class="form-group col-md-4">
						<label>Product</label>
						<!-- <input type="file" class="form-control" required="">-->
						<select class="form-control" name="product_id" required="" id="product_id" >
								<option value="0" selected disabled>--select--</option>
    							<?php foreach ($ecom_product as $category):?>
    								<option value="<?php echo $category['id'];?>"><?php echo $category['name']?></option>
    							<?php endforeach;?>
						</select>
						<div class="invalid-feedback">New Category Name?</div>
						<?php echo form_error('cat_id','<div style="color:red>"','</div>');?>
					</div>
					<div class="form-group col-md-6">
						<label>Title</label> <input type="text"
							class="form-control" name="title" required="" value="<?php echo set_value('title')?>" placeholder="Title">
						<div class="invalid-feedback">New Sub_Category Name?</div>
						<?php echo form_error('name','<div style="color:red">','</div>')?>
					</div>
					<div class="form-group col-md-6">
						<label>Author</label> <input type="text"
							class="form-control" name="author" required="" value="<?php echo set_value('author')?>" placeholder="Author">
						<div class="invalid-feedback">New Sub_Category Name?</div>
						<?php echo form_error('name','<div style="color:red">','</div>')?>
					</div>
					<div class="form-group col-md-12">
						<label>Url</label> <input type="text"
							class="form-control" name="url" value="<?php echo set_value('url')?>" placeholder="URL">
						<div class="invalid-feedback">New Sub_Category Name?</div>
						<?php echo form_error('name','<div style="color:red">','</div>')?>
					</div>
					<div class="form-group col-md-12">
						<label>Upload Image</label> <input type="file" name="file"
							 value="<?php echo set_value('file')?>"
							class="form-control" onchange="readURL(this);" > <br> <img id="blah"
							src="#" alt="" style="width: 200px">
						<div class="invalid-feedback">Upload Image?</div>
						<?php echo form_error('file', '<div style="color:red">', '</div>');?>
					</div>
					<div class="col col-sm col-md" >
					<label>Description</label> 
          	<textarea id="cat" name="desc" class="ckeditor" rows="10" data-sample-short></textarea>
          	<?php echo form_error('desc', '<div style="color:red">', '</div>');?>
        </div>
					
					<div class="form-group col-md-2">
						<button type="submit" name="upload" id="upload" value="Apply" 
							class="btn btn-primary mt-27 ">Submit</button>
					</div>
				</div>
			</div>
		</form>

		<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of  Categories</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>Title</th>
									<th>Author</th>
									<th>Category</th>
									<th>Sub_Category</th>
									<th>Product</th>
									<th>Description</th>
									<th>Image</th>
                                    <th>Actions</th>
								</tr>
							</thead>
							
							<tbody>
								<?php if(!empty($blogs)):?>
    							<?php $sno = 1; foreach ($blogs as $blog):?>
    								<tr>
    									<td><?php echo $sno++;?></td>
    									<td><?php echo $blog['title'];?></td>
    									<td><?php echo $blog['author'];?></td>
    									
    									<td><?php foreach ($ecom_categories as $category):?>
    										<?php echo ($category['id'] == $blog['cat_id'])? $category['name']:'';?>
    									<?php endforeach;?></td>
    									<td><?php foreach ($ecom_sub_categories as $sub_category):?>
    										<?php echo ($sub_category['id'] == $blog['sub_cat_id'])? $sub_category['name']:'';?>
    									<?php endforeach;?></td>
    									<td><?php foreach ($ecom_product as $product):?>
    										<?php echo ($product['id'] == $blog['product_id'])? $product['name']:'';?>
    									<?php endforeach;?></td>
    									<td><?php echo $blog['desc'];?></td>
    									<td><img
										src="<?php echo base_url();?>uploads/blog_image/blog_<?php echo $blog['id'];?>.jpg?<?php echo time();?>"
										class="img-thumb" style="width: 100px;height: 63px;"></td>
    									<td><a href="<?php echo base_url()?>blog_admin/edit?id=<?php echo $blog['id'];?>" class=" mr-2  " type="blog" > <i class="fas fa-pencil-alt" ></i>
    									</a> <a href="#" class="mr-2  text-danger " onClick="delete_record(<?php echo $blog['id'] ?>, 'blog_admin/d')"> <i
    											class="far fa-trash-alt"></i>
    									</a></td>
    								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr ><th colspan='5'><h3><center>No Blogs</center></h3></th></tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
 

		</div>

	</div>
</div>
