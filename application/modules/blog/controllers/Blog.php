<?php
class Blog extends MY_Controller
{

     function __construct()
    {
        parent::__construct();
        $this->template = 'template/admin/main';
        if (! $this->ion_auth->logged_in()) // || ! $this->ion_auth->is_admin()
            redirect('auth/login');
            $this->load->model('blog_model');
            $this->load->model('category_model');
            $this->load->model('sub_category_model');
            $this->load->model('product_model');
    }

    /**
     * Blog  crud
     *
     * @author Trupti
     * @desc To Manage  Blog
     * @param string $type
     */
    public function blog_admin($type = 'r')
    {
        
         if ($type == 'c') {
                
                $this->form_validation->set_rules($this->blog_model->rules);
                if ($this->form_validation->run() == FALSE) {
                    $this->blog_admin('r');
                } else {
                    $id = $this->blog_model->insert([
                        'cat_id' => $this->input->post('cat_id'),
                        'sub_cat_id' => $this->input->post('sub_cat_id'),
                        'product_id' => $this->input->post('product_id'),
                        'title' => $this->input->post('title'),
                        'author' => $this->input->post('author'),
                        'desc' => $this->input->post('desc'),
                        'url' => $this->input->post('url')
                    ]);
                    $path = $_FILES['file']['name'];
                    $ext = pathinfo($path, PATHINFO_EXTENSION);
                    $this->file_up("file", "blog", $id, '', 'no');
                    redirect('blog_admin/r', 'refresh');
                }
            } elseif ($type == 'r') { 
                $this->data['title'] = 'Blog';
                $this->data['content'] = 'blog/blog';
                $this->data['ecom_categories'] = $this->category_model->get_all();
                $this->data['ecom_sub_categories'] = $this->sub_category_model->get_all();
                $this->data['ecom_product'] = $this->product_model->get_all();
                $this->data['blogs'] = $this->blog_model->order_by('id', 'ASCE')->get_all();
                $this->_render_page($this->template, $this->data);
            } elseif ($type == 'u') {
                $this->form_validation->set_rules($this->blog_model->rules);
                if ($this->form_validation->run() == FALSE) {
                    echo validation_errors();
                } else {
                    $this->blog_model->update([
                        'cat_id' => $this->input->post('cat_id'),
                        'sub_cat_id' => $this->input->post('sub_cat_id'),
                        'product_id' => $this->input->post('product_id'),
                        'title' => $this->input->post('title'),
                        'author' => $this->input->post('author'),
                        'desc' => $this->input->post('desc'),
                    ], $this->input->post('id'));
                    if ($_FILES['file']['name'] !== '') {
                        $path = $_FILES['file']['name'];
                        $ext = pathinfo($path, PATHINFO_EXTENSION);
                        $this->file_up("file", "blog", $this->input->post('id'), '', 'no');
                    }
                    redirect('blog_admin/r', 'refresh');
                }
            } elseif ($type == 'd') {
                $this->blog_model->delete(['id' => $this->input->post('id')]);
            }elseif($type == 'edit'){
                $this->data['title'] = 'Edit Blog';
                $this->data['content'] = 'blog/edit';
                $this->data['type'] = 'ecom_product';
                $this->data['sub_categories']=$this->sub_category_model->order_by('id', 'DESC')->get_all();
                $this->data['categories'] = $this->category_model->order_by('id', 'DESC')->get_all();
                $this->data['product'] = $this->product_model->order_by('id', 'DESC')->get_all();
                $this->data['blog'] = $this->blog_model->where('id',$this->input->get('id'))->get();
              
               $this->_render_page($this->template, $this->data);
            }elseif($type == 'list'){
                $data = $this->sub_category_model->fields('id, name, desc')->where(['cat_id' =>$this->input->post('cat_id')])->get_all();
                echo json_encode($data);
            }elseif($type == 'product_list'){
                $data = $this->product_model->fields('id, name, desc')->where(['sub_cat_id' =>$this->input->post('sub_cat_id')])->get_all();
                echo json_encode($data);
            }
    }
    
    
}