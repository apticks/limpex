<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/*
 *Admin 
 */
$route['dashboard'] = 'admin/dashboard';
$route['sliders/(:any)'] = 'admin/sliders/$1';
$route['settings/(:any)'] = 'admin/settings/$1';
$route['profile/(:any)'] = 'admin/profile/$1';
$route['contact/(:any)'] = 'admin/contact/$1';
$route['bulk_email/(:any)'] = 'admin/bulk_email/$1';
$route['subscribers'] = 'admin/subscribers';
$route['orders'] = 'admin/orders';
$route['certificates/(:any)'] = 'admin/certificates/$1';

/*Catalogue*/
$route['ecom_category/(:any)'] = 'ecom/ecom_category/$1';
$route['ecom_sub_category/(:any)'] = 'ecom/ecom_sub_category/$1';
$route['ecom_orders/(:any)'] = 'ecom/ecom_orders/$1';
$route['ecom_product/(:any)'] = 'ecom/ecom_product/$1';

/*Blog*/
$route['blog_admin/(:any)'] = 'blog/blog/blog_admin/$1';


/*Home*/
$route['about'] = 'home/about';
$route['contact'] = 'home/contact';

$route['our_team'] = 'home/our_team';
$route['medical_properties'] = 'home/medical_properties';
$route['spice_growing'] = 'home/spice_growing';
$route['gallery'] = 'home/gallery';
$route['blog/(:any)'] = 'home/blog/$1';
$route['product/(:any)'] = 'home/products/$1';
$route['subscribe'] = 'home/subscribe';
$route['enquiry'] = 'home/enquiry';
$route['certificate_list'] = 'home/certificate_list';
/*Employees*/
$route['employee/(:any)'] = 'admin/employee/$1';
$route['role/(:any)'] = 'admin/role/$1';
$route['emp_list/(:any)'] = 'admin/emp_list/$1';
$route['sellers/(:any)'] = 'admin/sellers/$1';
$route['all_users/(:any)'] = 'admin/all_users/$1';
