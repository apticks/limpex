<?php

class Order_model extends MY_Model
{
    public $rules;
    public $foreign_key;
    public function __construct()
    {
        parent::__construct();
        $this->table = 'orders';
        $this->primary_key = 'id';
        $this->foreign_key = 'order_id';
        
       $this->_config();
       $this->_form();
       $this->_relations();
    }
    public function _config() {
        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        $this->delete_cache_on_save = TRUE;
    }
    
    public function _relations(){
        $this->has_one['address'] = array('Users_address_model', 'id', 'address_id');
        $this->has_one['user'] = array('User_model', 'id', 'user_id');
        $this->has_many['order_details'] = array(
            'foreign_model' => 'Order_details_model',
            'foreign_table' => 'order_details',
            'local_key' => 'id',
            'foreign_key' => $this->foreign_key,
            'get_relate' => FALSE
        );
    }
    
   
    
    public function _form(){
        $this->rules = array(
            array(
                'field' => 'discount',
                'lable' => 'discount',
                'rules' => 'required',
            ),
            array(
                'field' => 'tax',
                'lable' => 'tax',
                'rules' => 'required',
            ),
            array(
                'field' => 'total',
                'lable' => 'total',
                'rules' => 'required',
            ),
            array(
                'field' => 'payment_method_id',
                'lable' => 'payment method id',
                'rules' => 'required',
            ),
        );
    }
    
    public function all($start_date = NULL, $end_date = NULL, $status = NULL )
    {
    		$this->_query_all($start_date, $end_date, $status);
    		$this->db->order_by('id', 'DESC');
    		$rs     = $this->db->get($this->table);
    		$result = $rs->result_array();
    	return  $result;
    }
    
    private function _query_all($start_date = NULL, $end_date = NULL, $status = NULL)
    {
    	
    	$this->load->model(array('sub_category_model', 'user_model', 'category_model'));
    	
    	$primary_key = '`' . $this->primary_key . '`';
    	$table       = '`' . $this->table . '`';
    	
    	$str_select_product = '';
    	foreach (array( 'id', 'user_id', 'order_no', 'track_id', 'discount','total', 'created_at', 'status') as $v)
    	{
    		$str_select_product .= "$table.`$v`,";
    	}
    	
    	$this->db->select($str_select_product);
    	
    	if (! empty($start_date) && ! empty($end_date)){
    		$this->db->or_where('date(`orders`.`created_at`) BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($end_date)).'"');
    	}elseif (! empty($start_date) &&  empty($end_date)){
    		$this->db->or_where("date($table.`created_at`)=",  date('Y-m-d', strtotime($start_date)));
    	}
    	
    	if(! empty($status)){
    		$this->db->where("$table.`status`=", $status);
    	}
    	
    	return $this;
    }
    
}
