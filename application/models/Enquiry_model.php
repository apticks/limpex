<?php

class Enquiry_model extends MY_Model
{
    public $rules;
    public $foreign_key;
    public function __construct()
    {
        parent::__construct();
       
            $this->table="orders";
            $this->primary_key="id";
           
            $this->config();
            $this->forms();
            $this->relations();
    }
    
    
    public function config(){
        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        $this->delete_cache_on_save = TRUE;
        
    }
    public function relations() {
        $this->has_one['product'] = array('Product_model', 'id', 'product_id');
    }
    public function forms(){
        $this->rules = array(
            array(
                'field' => 'first_name',
                'label' => 'Name',
                'rules' => 'trim|required',
                'errors' =>  array(
                    'required' => 'You must provide a %s.',
                    'min_length' => 'you need to give minimum 5 characters'
                )
            )
        );
    }
}

