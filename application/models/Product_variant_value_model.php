<?php
class Product_variant_value_model extends MY_Model {
	public $rules;
	public function __construct() {
		parent::__construct ();
		$this->table = 'product_variant_values';
		$this->primary_key = 'id';

		$this->_config ();
		$this->_form ();
		$this->_relations ();
	}
	private function _config() {
		$this->timestamps = TRUE;
		$this->soft_deletes = TRUE;
		$this->delete_cache_on_save = TRUE;
	}
	private function _relations() {
		$this->has_one['product_variant'] = array('Product_variant_model', 'id', 'variant_id');
		$this->has_one['product'] = array('Product_model', 'id', 'product_id');
		$this->has_one['color'] = array('Colour_model', 'id', 'color');
		$this->has_one['model'] = array('Model_model', 'id', 'model');
		$this->has_one['size'] = array('Size_model', 'id', 'size');
	}
	private function _form() {
		$tables = $this->config->item('tables','ion_auth');
		$this->rules = array (
				array (
						'lable' => 'First Name',
						'field' => 'first_name',
						'rules' => 'trim|required|min_length[5]',
				        'errors'=>array(
				            'required'=>'Please give at least 5 characters'
				        )
				    
				),
		);
		
		
	}
}

