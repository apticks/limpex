<?php

class Blog_model extends MY_Model
{
    public $rules;
    public $foreign_key;
    public function __construct()
    {
        parent::__construct();
       
            $this->table="blog";
            $this->primary_key="id";
            $this->foreign_key = 'cat_id';
           
            $this->config();
            $this->forms();
            $this->relations();
    }
   
    
    public function config(){
        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        $this->delete_cache_on_save = TRUE;
        
    }
    public function relations() {
        
        $this->has_many['ecom_sub_categories'] = array(
            'foreign_model' => 'Sub_category_model',
            'foreign_table' => 'sub_categories',
            'local_key' => 'id',
            'foreign_key' => 'cat_id',
            'get_relate' => FALSE
        );
        $this->has_one['category'] = array('Category_model', 'id', 'cat_id');
    }
    public function forms(){
        $this->rules = array(
            array(
                'field' => 'title',
                'label' => 'title',
                'rules' => 'trim|required',
                'errors' =>  array(
                    'required' => 'You must provide a %s.',
                    'min_length' => 'you need to give minimum 5 characters'
                )
            )
        );
    }
}

