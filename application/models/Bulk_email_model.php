
<?php

class Bulk_email_model extends MY_Model
{
    public $rules;
    public $foreign_key;
    public function __construct()
    {
        parent::__construct();
       
            $this->table="bulk_email";
            $this->primary_key="id";
           
            $this->config();
            $this->forms();
            $this->relations();
    }
   
    
    public function config(){
        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        $this->delete_cache_on_save = TRUE;
        
    }
    public function relations() {
       
    }
    public function forms(){
        $this->rules = array(
            array(
                'field' => 'emails[]',
                'label' => 'Subscribers Emails',
                'rules' => 'trim',
                'errors' =>  array(
                    'required' => 'You must provide a %s.',
                )
            ),
            array(
                'field' => 'manual_email',
                'label' => 'Email',
                'rules' => 'trim',
                'errors' =>  array(
                    'required' => 'You must provide a %s.',
                )
            ),
            array(
                'field' => 'subject',
                'label' => 'Subject',
                'rules' => 'trim|required',
                'errors' =>  array(
                    'required' => 'You must provide a %s.',
                )
            ),
            array(
                'field' => 'desc',
                'label' => 'Mail Content',
                'rules' => 'trim|required',
                'errors' =>  array(
                    'required' => 'You must provide a %s.',
                )
            )
        );
    }
}

