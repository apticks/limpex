<?php

class Contact_model extends MY_Model
{
    public $rules;
    public $foreign_key;
    public function __construct()
    {
        parent::__construct();
       
            $this->table="contact_details";
            $this->primary_key="id";
           
            $this->config();
            $this->forms();
            $this->relations();
    }
    // protected function _add_created_by($data)
    // {
    //     $data['created_user_id'] = $this->ion_auth->get_user_id(); //add user_id
    //     return $data;
    // }
    // protected function _add_updated_by($data)
    // {
    //     $data['updated_user_id'] = $this->ion_auth->get_user_id(); //add user_id
    //     return $data;
    // } 
    
    public function config(){
        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        $this->delete_cache_on_save = TRUE;
        
    }
    public function relations() {
       
    }
    public function forms(){
        $this->rules = array(
            array(
                'field' => 'first_name',
                'label' => 'First Name',
                'rules' => 'trim|required',
                'errors' =>  array(
                    'required' => 'You must provide a %s.',
                )
            ),
            array(
                'field' => 'last_name',
                'label' => 'Last Name',
                'rules' => 'trim|required',
                'errors' =>  array(
                    'required' => 'You must provide a %s.',
                )
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required',
                'errors' =>  array(
                    'required' => 'You must provide a %s.',
                )
            ),
            array(
                'field' => 'subject',
                'label' => 'Subject',
                'rules' => 'trim|required',
                'errors' =>  array(
                    'required' => 'You must provide a %s.',
                )
            ),
            array(
                'field' => 'message',
                'label' => 'Message',
                'rules' => 'trim|required',
                'errors' =>  array(
                    'required' => 'You must provide a %s.',
                )
            )
        );
    }
}

