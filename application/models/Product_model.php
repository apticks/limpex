<?php
class Product_model extends MY_Model {
	public $rules;
	public function __construct() {
		parent::__construct ();
		$this->table = 'products';
		$this->primary_key = 'id';
		$this->foreign_key = 'product_id';
		$this->_config ();
		$this->_form ();
		$this->_relations ();
	}
	private function _config() {
		$this->timestamps = TRUE;
		$this->soft_deletes = TRUE;
		$this->delete_cache_on_save = TRUE;
	}
	private function _relations() {
		$this->has_one['user'] = array('User_model', 'id', 'user_id');
		$this->has_one['category'] = array('Category_model', 'id', 'cat_id');
		$this->has_one['sub_category'] = array('Sub_category_model', 'id', 'sub_cat_id');
	}
	private function _form() {
		$this->rules = array (
				 array (
						'lable' => 'Product Name',
						'field' => 'name',
						'rules' => 'required',
				), 
		);
		
	}
	
	
	public function all($cat_id = NULL, $sub_cat_id = NULL, $brand_id = NULL, $search = NULL, $user_id = NULL)
	{
	   /*  $cache_name = $cat_id . $sub_cat_id . $search ;
	    $this->set_cache($cache_name); //just to set cache_name using MY_model
	    $result     = $this->_get_from_cache(); //MY_model */
	    
	   /*  if ( ! (isset($result) && $result !== FALSE))
	    { */
		$this->_query_all($cat_id, $sub_cat_id, $brand_id, $search, $user_id);
	        $this->db->order_by('id', 'DESC');
	        /* if ( $limit != NULL && $offset != NULL)
	        {
	            $this->db->limit($limit, $offset);
	        } */
	        $rs     = $this->db->get($this->table);
	        $result = $rs->result_array();
	        /* $this->_write_to_cache($result);  *///MY_model
	    /* } */
	    //print_array($this->db->last_query());
	    
	    $this->db->reset_query();
	    
	    $this->_query_all($cat_id, $sub_cat_id, $brand_id, $search, $user_id);
	    $count = $this->db->count_all_results($this->table);
	    
	    return  array(
	        'result' => $result,
	        'count'  => $count
	    );
	}
	
	private function _query_all($cat_id = NULL, $sub_cat_id = NULL, $brand_id = NULL, $search = NULL, $user_id = NULL)
	{
	    
	    $this->load->model(array('sub_category_model', 'user_model', 'category_model', 'brand_model'));
	    
	    $ecom_sub_category_table       = '`' . $this->sub_category_model->table . '`';
	    $ecom_sub_category_primary_key = '`' . $this->sub_category_model->primary_key . '`';
	    $ecom_sub_category_foreign_key = '`' . $this->sub_category_model->foreign_key . '`';
	    
	    $brand_table       = '`' . $this->brand_model->table . '`';
	    $brand_primary_key = '`' . $this->brand_model->primary_key . '`';
	    $brand_foreign_key = '`' . $this->brand_model->foreign_key . '`';
	    
	    $category_table       = '`' . $this->category_model->table . '`';
	    $category_primary_key = '`' . $this->category_model->primary_key . '`';
	    $category_foreign_key = '`' . $this->category_model->foreign_key . '`';
	    
	    $user_table       = '`' . $this->user_model->table . '`';
	    $user_primary_key = '`' . $this->user_model->primary_key . '`';
	    $user_foreign_key = '`' . $this->user_model->foreign_key . '`';
	    
	    $primary_key = '`' . $this->primary_key . '`';
	    $foreign_key = '`' . $this->foreign_key . '`';
	    $table       = '`' . $this->table . '`';
	    
	    $str_select_product = '';
	    foreach (array( 'id', 'user_id', 'name', 'product_code', 'cat_id', 'sub_cat_id', 'brand_id', 'status') as $v)
	    {
	        $str_select_product .= "$table.`$v`,";
	    }
	    
	    $this->db->select($str_select_product);
	    $this->db->join($ecom_sub_category_table, "$ecom_sub_category_table.$primary_key=$table.$ecom_sub_category_foreign_key", 'left');
	    $this->db->join($category_table, "$category_table.$primary_key=$table.$category_foreign_key", 'left');
	    $this->db->join($brand_table, "$brand_table.$brand_primary_key=$table.$brand_foreign_key", 'left');
	    $this->db->join($user_table, "$user_table.$user_primary_key=$table.$user_foreign_key", 'left');
	    
	    if ($cat_id)
	    {
	        $this->db->where("$table.$category_foreign_key", $cat_id);
	    }
	    
	    if ($brand_id)
	    {
	        $this->db->where("$table.$brand_foreign_key", $brand_id);
	    }
	    
	    if ($user_id)
	    {
	    	$this->db->where("$table.$user_foreign_key", $user_id);
	    }
	    
	    if ($sub_cat_id)
	    {
	        $this->db->where("$table.$ecom_sub_category_foreign_key", $sub_cat_id);
	    }
	    
	    if ( ! is_null($search))
	    {
	        
	        $this->db->or_like($table . '.`sounds_like`', metaphone($search));
	    }
	    $this->db->where("$table.status", 1);
	    $this->db->where("$user_table.status", 1);
	    return $this;
	}
	
}

class Products_list_row
{
    public $id;
    public $name;
    public $product_code;
    public $cat_id;
    public $sub_cat_id;
    public $brand_id;
}
