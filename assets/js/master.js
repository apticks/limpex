function delete_record(id, uri){
	if(confirm('Do you want to delete..?')){
		$.ajax({
			url: base_url+uri,
			type: 'post',
			data: {id : id},
			success: function(data){
				window.location.reload();
			}
		});
	}
}

function deleteAllCookies() {
    var cookies = document.cookie.split(";");

    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
    return true;
}

$(() => {
    $('.approve_toggle').change(function() {
    	if(confirm('Do You Want To Change Approve Status..?')){
    		let vendor_id = $(this).attr('vendor_id');
    		let user_id = $(this).attr('user_id');
    		let is_checked = $(this).is(':checked');
    		$.ajax({
    			url: base_url+'vendors/change_status',
    			type: 'post',
    			dataType: 'json',
    			data: {vendor_id : vendor_id, user_id : user_id, is_checked : is_checked},
    			success: function(data){
    				console.log(data);
    			}
    		});
    	}
    });
    
    $('.change_order_item_status').change(function() {
    	if(confirm('Is product arrived..?')){
    		let order_item_id = $(this).attr('order_item_id');
    		let order_item_code = $(this).attr('order_item_code');
    		let amount = $(this).attr('amount');
    		let vendor_id = $(this).attr('vendor_id');
    		let order_no = $(this).attr('order_no');
    		let is_checked = $(this).is(':checked');
    		$.ajax({
    			url: base_url+'orders/change_order_item_status',
    			type: 'post',
    			dataType: 'json',
    			data: {order_item_id : order_item_id, vendor_id: vendor_id, order_item_code : order_item_code, order_no : order_no, amount : amount, is_checked : is_checked},
    			success: function(data){
    				console.log(data);
    				location.reload();
    			}
    		});
    	}
    });
    
    $('.pay_status').change(function() {
    	 	let txn_id = prompt("Please enter transaction id", "Transaction id");
    		let amount = $(this).attr('amount');
    		let id = $(this).attr('id');
    		$.ajax({
    			url: base_url+'wallet_transactions/change_status',
    			type: 'post',
    			dataType: 'json',
    			data: {amount : amount, txn_id : txn_id, id : id},
    			success: function(data){
    				console.log(data);
    				location.reload();
    			}
    		});
    	});
  });

function state_changed(){
	var state_id = document.getElementById("state").value;
	var token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEiLCJ1c2VyZGV0YWlsIjp7InVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsIjoiYWRtaW5AYWRtaW4uY29tIiwicGhvbmUiOiIwIn0sInRpbWUiOjE1Njk1MDY5Njd9.-5N8CdYYitPW_eGE-U9FyZHSliaXspErZvb1wUhHWpY';
	$.ajax({
		url: base_url+'general/api/master/states/'+state_id,
		type: 'get',
		beforeSend: function(xhr){xhr.setRequestHeader('X_AUTH_TOKEN', token);},
		success: function(data){
			var options = '';
			for(var i = 0; i < data.data.districts.length; i++){
				options += '<option value="'+data.data.districts[i].id+'">'+data.data.districts[i].name+'</option>'
			}
			document.getElementById("district").innerHTML = options;
		}
	});
}

function category_changed(){
    var cat_id = document.getElementById("category").value;
    $.ajax({
        url: base_url+'ecom_sub_category/list',
        type: 'post',
        data: {cat_id : cat_id},
        dataType: 'json',
        success: function(data){
        	//console.log(data.length);
            var options = '<option value="0" selected disabled>--select--</option>';
            for(var i = 0; i < data.length; i++){
                options += '<option value="'+data[i].id+'">'+data[i].name+'</option>'
            }
            document.getElementById("sub_cat_id").innerHTML = options;
        }
    });
}

function category_changedp(){
    var cat_id = document.getElementById("pcategory").value;
    $.ajax({
        url: base_url+'ecom_product/list',
        type: 'post',
        data: {cat_id : cat_id},
        dataType: 'json',
        success: function(data){
            //console.log(data.length);
            var options = '<option value="0" selected disabled>--select--</option>';
            for(var i = 0; i < data.length; i++){
                options += '<option value="'+data[i].id+'">'+data[i].name+'</option>'
            }
            document.getElementById("psub_cat_id").innerHTML = options;
        }
    });
}
function product_changed(){
    var sub_cat_id = document.getElementById("psub_cat_id").value;
    $.ajax({
        url: base_url+'blog/product_list',
        type: 'post',
        data: {sub_cat_id : sub_cat_id},
        dataType: 'json',
        success: function(data){
            //console.log(data.length);
            var options = '<option value="0" selected disabled>--select--</option>';
            for(var i = 0; i < data.length; i++){
                options += '<option value="'+data[i].id+'">'+data[i].name+'</option>'
            }
            document.getElementById("product_id").innerHTML = options;
        }
    });
}

function sub_category_changed(){
    var sub_cat_id = document.getElementById("sub_cat_id").value;
    $.ajax({
        url: base_url+'ecom_brands/list',
        type: 'post',
        data: {sub_cat_id : sub_cat_id},
        dataType: 'json',
        success: function(data){
        	console.log(data[0]);
        	let options = '<option value="0" selected disabled>--select--</option>';
        	$.each(data[0].brands, function(index, element) {
        		options += '<option value="'+element.id+'">'+element.name+'</option>';
        	});
            document.getElementById("brand_id").innerHTML = options;
            
            var options2 = '<option value="0" selected disabled>--select--</option>';
            for(var i = 0; i < data[0].ecom_sub_sub_categories.length; i++){
                options2 += '<option value="'+data[0].ecom_sub_sub_categories[i].id+'">'+data[0].ecom_sub_sub_categories[i].name+'</option>'
            }
            document.getElementById("sub_id").innerHTML = options2;
          
        }
    });
}


function category_changedsub(){
    var cat_id = document.getElementById("category").value;
    $.ajax({
        url: base_url+'ecom_sub_category/list',
        type: 'post',
        data: {cat_id : cat_id},
        dataType: 'json',
        success: function(data){
            var options = '<option value="0" selected disabled>--select--</option>';
            for(var i = 0; i < data.length; i++){
                options += '<option value="'+data[i].id+'">'+data[i].name+'</option>'
            }
            document.getElementById("subcat").innerHTML = options;
        }
    });
}

function clear_form(id) {
	$('#'+id).find('input:text, input:password, input:file, select, textarea').val('');
	$("#"+id).find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
}

$(function () {
    $("#btnSubmit").click(function () {
        var password = $("#Password").val();
        var confirmPassword = $("#ConfirmPassword").val();
        if (password != confirmPassword) {
            alert("Passwords do not match.");
            return false;
        }
        return true;
    });
});

function readURL(input, width, height) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#blah')
                .attr('src', e.target.result)
                .width(width)
                .height(height);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

$(document).ready(function() {
	deleteAllCookies();
    $('#upload_form').on('submit', function(e) {
        e.preventDefault();
        if ($('#userfile').val() == '') {
            alert("Please Select the File");
        } else {
            $.ajax({
                url: "<?php echo base_url(); ?>master",
                method: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                dataType: 'json',
                success: function(data) {
                    if (data.success == true) {
                        $('#result').find('img').attr('src', data.file);
                    } else {
                        alert(data.msg);
                    }
                }
            });
        }
    });
    
    /*Mobile Number validation*/
    $("#mobile").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
           return false;
       }
	});
    
    /*news module post type*/
    if(url == '' || url == undefined || url == null){
    	$(".link").hide();
    }else{
    	$("#link").val(url);
    	$(".link").show();
    }
    $("#type").change(function(){
    	let type = $(this).val();
    	if(type ==  2){
    		if(url == '' || url == undefined || url == null){
    			$("#link").val('');
    	    }else{
    	    	$("#link").val(url);
    	    }
    		$(".link").show();
    	}else{
    		$("#link").val('');
    		$(".link").hide();
    	}
    });

});

function change_status(id){
	var is_confirmed = confirm('Do you want to Change status..?');
	var e = document.getElementById("change_status"+id);
	var status = e.options[e.selectedIndex].value;
	if(is_confirmed){
		$.ajax({
            url: base_url+"sellers/change_status",
            method: "POST",
            data: {id :id, status: status},
            success: function(data) {
                window.location.reload();
            }
        });
	}
}
